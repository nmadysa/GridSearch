
"""Module that exports a few handy exception classes."""

from __future__ import print_function
from __future__ import absolute_import
from __future__ import division


class NProngError(ValueError):

    """Class of exceptions raised by an invalid value to `nprong`."""
