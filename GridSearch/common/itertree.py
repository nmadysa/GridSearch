#! /usr/bin/env python2

"""Contains the function itertree."""

from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import sys
import numbers

def itertree(tree, n_feedback=20000, stdout=sys.stdout):
    """Print current progress while iterating over a ROOT.TTree.

    This is a convenience wrapper around `ROOT.TTree.__iter__`.

    tree       : ROOT.TTree object.
    n_feedback : Print progress every so many events.
                 Set to 0 to disable feedback.
    stdout     : file-like object that feedback messages are printed to.
    """
    # Dispatch on n_feedback. Yield the non-trivial iterator only
    # if n_feedback is an integer greater than zero.
    if not isinstance(n_feedback, numbers.Integral):
        raise ValueError("Not an integer: {}".format(n_feedback))
    if n_feedback < 0:
        raise ValueError("Must be non-negative: {}".format(n_feedback))
    elif n_feedback == 0:
        return iter(tree)
    else:
        return _itertree(tree, n_feedback, stdout)


def _itertree(tree, n_feedback, stdout):
    """Implementation of `itertree`."""
    n_entries = tree.GetEntries()
    template, deleter = _get_feedback_template(n_entries)
    # Dummy line deleted immediately by the first loop iteration.
    print(template.format(0, n_entries), end="", file=stdout)
    # Iterate over tree.
    for i, event in enumerate(tree):
        yield event
        if i % n_feedback == 0:
            print(deleter, template.format(i, n_entries),
                  sep="", end="", file=stdout)
            stdout.flush()
    # Print final line "all / all".
    print(deleter, template.format(n_entries, n_entries),
          sep="", end="\n", file=stdout)


def _get_feedback_template(number):
    """Return a format string wide enough to print the passed int.

    Returns: Tuple (template, deleter)
    template : A `format` template string that can be used to show a
               message "{number} / {number}".
    deleter  : A string as long as the formatted messages, but consisting
               only of backspace characters.
    """
    if number < 0 or not isinstance(number, numbers.Integral):
        raise ValueError("Not a positive integer: {}".format(number))
    n_digits = len(str(number))
    n_thousand_separators = (n_digits-1) // 3
    n_characters = n_digits + n_thousand_separators
    template = "{{:{n},d}} / {{:{n},d}}".format(n=n_characters)
    deleter = "\b" * (3 + 2*n_characters)
    return template, deleter
