#! /usr/bin/env python2

"""Centrally define two named tuples which are used throughout the package."""

from collections import namedtuple

SigBkgPair = namedtuple("SigBkgPair", ["sig", "bkg"])
ProngPair = namedtuple("ProngPair", ["sp", "mp"])
