
"""Module that exports a few handy context managers."""

from __future__ import print_function
from __future__ import absolute_import
from __future__ import division

import os
import sys
import contextlib

@contextlib.contextmanager
def preserve_cwd(newdir):
    """Context manager that resets the CWD and environment on exit.

    `newdir` is the directory to change into when entering the context.
    This also creates a local copy of `os.environ` with its "PWD" set
    accordingly.
    When exiting the context, both the working directory and `os.environ`
    are reset.
    """
    oldenv = os.environ.copy()
    os.environ["PWD"] = os.path.normpath(os.path.abspath(newdir))
    olddir = os.getcwd()
    os.chdir(newdir)
    try:
        yield
    finally:
        os.chdir(olddir)
        os.environ = oldenv


class redirect_stdout(object):

    """Context manager for temporarily redirecting stdout to another file

    This is a backport of Python3's `contextlib.redirect_stdout`.

        # How to send help() to stderr
        with redirect_stdout(sys.stderr):
            help(dir)

        # How to write help() to a file
        with open('help.txt', 'w') as f:
            with redirect_stdout(f):
                help(pow)
    """

    # pylint: disable=invalid-name, too-few-public-methods

    def __init__(self, new_target):
        self._new_target = new_target
        # We use a list of old targets to make this CM re-entrant
        self._old_targets = []

    def __enter__(self):
        self._old_targets.append(sys.stdout)
        sys.stdout = self._new_target
        return self._new_target

    def __exit__(self, exctype, excinst, exctb):
        sys.stdout = self._old_targets.pop()


class redirect_stderr(object):

    """Context manager for temporarily redirecting stderr to another file

    This is a modified backport of Python3's `contextlib.redirect_stdout`.
    Cf. `redirect_stdout` for more information.
    """

    # pylint: disable=invalid-name, too-few-public-methods

    def __init__(self, new_target):
        self._new_target = new_target
        # We use a list of old targets to make this CM re-entrant
        self._old_targets = []

    def __enter__(self):
        self._old_targets.append(sys.stderr)
        sys.stderr = self._new_target
        return self._new_target

    def __exit__(self, exctype, excinst, exctb):
        sys.stderr = self._old_targets.pop()


