#! /usr/bin/env python2

"""Implement the parsing of variant TMVA methods in INI files.

The init_run script takes a methods.ini file that describes the TMVA
methods to deploy.
It is possible to specify more than one method in one section, in which
case one ANN folder is deployed for each method.
The syntax is as follows:

- If the value of an entry is a dict literal, it is parsed as such into
  a value `d`.
  In that case, all 1-prong folders use the value `d[1]` whereas all
  3-prong folders use the value `d[3]`.
  If `d` lacks either of the two keys, an exception is raised.
- If a value of an entry is either a list or a set literal, it is parsed
  as such.
  After that, `init_run` iterates through each possible combination of
  list items for all lists and deploy an ANN folder for each.
  See the method `` for an example.
- If you want to combine both behaviors, nest the list inside the dict,
  not the other way around. Make sure that the lists for all keys of `d`
  have the same number of items.
"""

from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import itertools as its
from collections import Iterable, Mapping


class VariantMethod(object):

    """An TMVAMethod iterator behaving as described in the module docstring.

    Usage:
    >>> settings = dict(NCycles={1: 1000, 3: 2000},
    ...                 TrainingMethod=["BP", "BFGS"],
    ...                 NeuronType=["sigmoid", "tanh"])
    >>> variant = VariantMethod(settings)
    >>> for method in variant.choose(1).combinations():
    ...     print method.options
    {'NCycles': 1000, 'NeuronType': 'sigmoid', 'TrainingMethod': 'BP'}
    {'NCycles': 1000, 'NeuronType': 'sigmoid', 'TrainingMethod': 'BFGS'}
    {'NCycles': 1000, 'NeuronType': 'tanh', 'TrainingMethod': 'BP'}
    {'NCycles': 1000, 'NeuronType': 'tanh', 'TrainingMethod': 'BFGS'}
    """

    def __init__(self, tmva_type, name, options):
        """Construct a new instance.

        tmva_type, name, options : Items of a TMVAMethod namedtuple.

        `tmva_method.options` may not only contain numbers and strings,
        but also sets, lists, and dicts.
        These can be unpacked and iterated by the methods `choose` and
        `combinations`.
        """
        self.type = tmva_type
        self.name = name
        self.options = options

    def __iter__(self):
        """Yields the attributes `type`, `name`, `options` in that order."""
        # This allows tuple-unpacking objects of this class.
        return iter((self.type, self.name, self.options))

    def __repr__(self):
        return "{}(tmva_type={}, name={}, options={})".format(
            type(self).__name__, self.type, self.name, self.options,
            )

    def choose(self, values_key):
        """Replace all top-level dicts with one of their values.

        This returns a `VariantMethod(new_options)` where `new_options`
        is a copy of `self.options` where each dict-like value `d` has
        been replaced by `d[key]`.
        """
        new_options = self.options.copy()
        for opt_key, value in new_options.items():
            if isinstance(value, Mapping):
                new_options[opt_key] = value[values_key]
        return VariantMethod(self.type, self.name, new_options)

    def combinations(self):

        # pylint: disable=star-args
        """Iterate all `TMVAMethod`s described by this object.

        From all list- or set-like values of `settings`, this iterator will
        choose one value out of the list or set.
        This iterator then yields each possible combination of options.

        Example:
        >>> settings = dict(NCycles=1000, TrainingMethod=["BP", "BFGS"],
        ...                 NeuronType=["sigmoid", "tanh"])
        >>> variant = VariantMethod(settings)
        >>> for method in variant.combinations():
        ...     print method.options
        {'NCycles': 1000, 'NeuronType': 'sigmoid', 'TrainingMethod': 'BP'}
        {'NCycles': 1000, 'NeuronType': 'sigmoid', 'TrainingMethod': 'BFGS'}
        {'NCycles': 1000, 'NeuronType': 'tanh', 'TrainingMethod': 'BP'}
        {'NCycles': 1000, 'NeuronType': 'tanh', 'TrainingMethod': 'BFGS'}
        """

        iterable_values = []
        for key, value in self.options.items():
            if isinstance(value, Iterable) and not isinstance(value, str):
                iterable_value = zip(its.repeat(key), value)
                iterable_values.append(iterable_value)
        # iterable_values == [[(key1, val1), (key1, val2), ...],
        #                     [(key2, val1), (key2, val2), ...]]

        for combination in its.product(*iterable_values):
            # combination == [(key1, valx), (key2, valy), (key3, valz), ...]
            new_options = self.options.copy()
            new_options.update(combination)
            yield VariantMethod(self.type, self.name, new_options)
