#! /usr/bin/env python2

"""Module for reading and writing TMVA methods from config files.

This module lets you specify TMVA methods in dedicated INI files like this:
```
[method_name]
Type: method_type
OptionA: value
OptionB: value
```
"""

from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import ast

from ConfigParser import RawConfigParser
from collections import namedtuple

# FIRST change ROOT configuration, THEN import TMVA.
import ROOT
ROOT.gROOT.SetBatch(True)
ROOT.PyConfig.IgnoreCommandLineOptions = True
from ROOT import TMVA  # pylint: disable=no-name-in-module


TMVAMethod = namedtuple("TMVAMethod", ["type", "name", "options"])


def parse_python(dict_):
    """Return new dict with Python list, set, and dict literals parsed.

    Take dict `dict_` whose values all are strings. If a string begins
    with any of the characters "[", "{", "'", or "\"", parse it as a
    Python literal.
    This uses `ast.literal_eval` for safe parsing of untrusted strings.
    This does not modify the original dict, but instead returns a new one.
    """
    return {key: (ast.literal_eval(value) if value[0] in """['"{""" else value)
            for key, value in dict_.items()}


def read_methods(filename):
    """Iterate over TMVA methods saved in a config file.

    Generator that yields TMVAMethod namedtuples.

    filename : Path to an INI file read by a RawConfigParser.
    """
    parser = RawConfigParser()
    # Make option names case-sensitive.
    parser.optionxform = str
    parser.read(filename)
    for section in parser.sections():
        method_name = section
        method_type = getattr(TMVA.Types, parser.get(section, "Type"))
        # Build options string in two steps.
        options = dict(parser.items(section))
        # Remove type, it isn't an actual option.
        del options["Type"]
        yield TMVAMethod(method_type, method_name, options)


def write_methods(methods, file_):
    """Write TMVA methods to config file.

    methods  : iterable of tuples `(type_, name, options)`.
        type    : A member of ROOT.TMVA.Types. Describes the MVA method
                  to use.
        name    : Name of the analysis.
        options : dict of options `{key: value}` to write.
    filename : A file-like object.
    """
    parser = RawConfigParser()
    parser.optionxform = str
    for type_, name, options in methods:
        parser.add_section(name)
        parser.set(name, "Type", _find_tmva_type_name(type_))
        for option, value in options.viewitems():
            parser.set(name, option, str(value))
    parser.write(file_)


def _find_tmva_type_name(tmva_type):
    """Return TMVA::Types constant with same value as tmva_type.

    tmva_type : an int, member of tmva_type
    """
    names = (name for name in dir(TMVA.Types) if name.startswith("k"))
    for name in names:
        if getattr(TMVA.Types, name) == tmva_type:
            return name
    raise KeyError("Could not find TMVA::Types constant with value {}"
                   .format(tmva_type))


def get_default_bdt():
    """Return the default BDT as a TMVAMethod namedtuple."""
    options = {
        "H": False,
        "V": False,
        "NTrees": 100,
        "MaxDepth": 8,
        "nCuts": 200,
        "MinNodeSize": 0.1,
        "BoostType": "AdaBoost",
        "AdaBoostBeta": 0.2,
        "UseYesNoLeaf": False,
        "SeparationType": "GiniIndex",
        "PruneMethod": "NoPruning",
        "DoBoostMonitor": True,
        }
    return TMVAMethod(TMVA.Types.kBDT, "BDT", options)


def get_default_mlp():
    """Return the default MLP as a TMVAMethod namedtuple."""
    options = {
        "Type": "kMLP",
        "RandomSeed": 42,
        "NeuronType": "tanh",
        "HiddenLayers": "20,20,20,20",
        "TrainingMethod": "BFGS",
        "VarTransform": "N",
        "NCycles": 2000,
        }
    return TMVAMethod(TMVA.Types.kMLP, "MLP", options)

