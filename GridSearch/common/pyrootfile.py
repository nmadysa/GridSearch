#! /usr/bin/env python2

"""Contains the wrapper class PyRootFile."""

from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import os
import types
from collections import namedtuple

import ROOT
ROOT.gROOT.SetBatch(True)
ROOT.PyConfig.IgnoreCommandLineOptions = True


def _methodize(func, instance):
    """Turn a function object into a bound instance method."""
    return types.MethodType(func, instance, instance.__class__)


def force_create(filename, mode="interactive"):
    """Open a ROOT file in mode "CREATE".

    If the file already exists, proceed as follows:
    - If `mode=="abort"`, raise an `IOError`.
    - If `mode=="force"`, truncate the file and open it.
    - If `mode=="interactive"` (the default), prompt the user for how to
      continue. When in doubt, `IOError` is raised.
    - If mode is neither of these values, ValueError is raised.

    In the case of success, the opened file is returned as a `PyRootFile`.
    """
    mode = mode.lower()
    if mode == "abort":
        return PyRootFile(filename, "CREATE")
    if mode == "force":
        return PyRootFile(filename, "RECREATE")
    else:
        try:
            return PyRootFile(filename, "CREATE")
        except IOError:
            reply = raw_input("{!r} already exists. Overwrite? (y/[n]) "
                              .format(filename))
            if reply.lower().startswith("y"):
                return PyRootFile(filename, "RECREATE")
            else:
                raise


# --- Class PyRootFile ---

ParsedFilename = namedtuple("ParsedFilename",
                            ["cycle", "type", "sample", "others"])


class PyRootFile(ROOT.TFile):

    """A thin wrapper class around ROOT.TFile.

    For a few select methods, this class raises exceptions instead
    of returning error codes if something goes wrong.
    Everything else is completely unmodified.
    """

    # pylint: disable=invalid-name

    # --- Constructor wrappers ---

    def __init__(self, path, mode="READ", title="", compress=1):
        
        """Create/open a wrapped TFile object.

        Raise IOError on failure.
        """
        
        ROOT.TFile.__init__(self, path, mode, title, compress)
        if self.IsZombie():
            raise IOError("Error opening file {!r}".format(path))
            
        # Overwrite static methods with bound instance methods.
        def parse_filename(self):
            """Parse filename and return namedtuple of its components."""
            return self.__class__.parse_filename(self.GetName())
        def is_signal_file(self):
            """Return True if this file contains signal events."""
            return self.__class__.is_signal_file(self.GetName())
            
        self.is_signal_file = _methodize(is_signal_file, self)
        self.parse_filename = _methodize(parse_filename, self)

    @staticmethod
    def Open(name, mode="", title="", compress=1, netopt=0):
        """Create/open either a TFile or TFile-derived object.

        This is a wrapper method around ROOT.TFile.Open. Note that due
        to its nature as a factory function, this method cannot and will
        not return a PyRootFile object.

        Raise IOError on failure.
        """
        result = ROOT.TFile.Open(name, mode, title, compress, netopt)
        if result.IsZombie():
            raise IOError("Error opening file {!r}".format(path))
        return result

    # --- Filename parsing ---

    @staticmethod
    def parse_filename(filename):
        """Parse filename and return namedtuple of its components.

        Assumed format: <CycleName>.<TypeSpec>.<Sample>.[<others.]*root.

        <CycleName> must end in "Cycle".
        <TypeSpec> must be either "mc" or "data".
        <Sample> must not be empty.
        The name may contain zero or more <others> components.
        The resulting tuple's `others` attribute is always a list.
        """
        # Split base name into components.
        filename = os.path.basename(filename)
        components = filename.split(".")
        if len(components) < 4:
            raise ValueError("Name must have at least 4 components: {!s}"
                             .format(filename))
        cycle, type_, sample = components[:3]
        others, suffix = components[3:-1], components[-1]
        # Component validation.
        if suffix != "root":
            raise ValueError("File suffix isn't 'root': {!s}".format(filename))
        elif not (cycle and sample):
            raise ValueError("Empty name component: {!s}".format(filename))
        elif not cycle.endswith("Cycle"):
            raise ValueError("Bad cycle name {!r}: {!s}".format(cycle, filename))
        elif not type_ in {"mc", "data"}:
            raise ValueError("Bad type spec {!r}: {!s}".format(type_, filename))
        else:
            return ParsedFilename(cycle, type_, sample, others)

    @classmethod
    def is_signal_file(cls, filename):
        """Return True if this file contains signal events."""
        return cls.parse_filename(filename).type == "mc"

    # --- Various other wrappers ---

    def Get(self, namecycle):
        """Return the object identified by namecycle.

        Raise KeyError if the object could not be retrieved.
        """
        result = ROOT.TFile.Get(self, namecycle)
        if result:
            return result
        else:
            raise KeyError("Could not find {key} in file {name}"
                           .format(key=namecycle, name=self.GetName()))

    def mkdir(self, name, title=""):
        """Create a new subdirectory."""
        result = ROOT.TFile.mkdir(self, name, title)
        if result:
            return result
        else:
            # Identify the source of the error.
            if self.Get(name):
                raise IOError(17, "Object exists: "+name)
            else:
                raise IOError("Could not create directory: "+name)

    def cd(self, path=""):
        """Change to a directory.

        If `path` is empty or not passed, change to `self`.
        If `path` is an absolute path, change to that directory.
        If `path` is a relative path, it is relative to `self`.
        """
        if not ROOT.TFile.cd(self, path):
            # Identify the source of the error.
            if not self.Get(path):
                raise IOError(2, "No such directory: "+path)
            else:
                raise IOError("Could not cd to: "+path)

    def GetDirectory(self, apath="", do_raise=True):
        """Find a directory named `apath`.

        If `path` is empty or not passed, return *this* directory.
        If `do_raise` is passed and False, do not raise an exception
        if the directory doesn't exist. Instead return a null pointer.
        """
        result = ROOT.TFile.GetDirectory(self, apath)
        if do_raise and not result:
            raise IOError("Could not get directory: "+apath)
        return result

    # --- Reflection wrappers ---

    def __str__(self):
        return "<{cls} object ({name!r}) at 0x{id:x}>".format(
            cls=type(self).__name__,
            name=self.GetName(),
            id=id(self),
            )

    __repr__ = __str__



class CachedRootFile(PyRootFile):

    """Subclass of PyRootFile, caches results of method `Get`."""

    # pylint: disable=invalid-name

    # --- Constructor wrappers ---

    def __init__(self, path, mode="READ", title="", compress=1):
        """Cf. superclass method for description."""
        super(CachedRootFile, self).__init__(path, mode, title, compress)
        self._cache = {}

    def Get(self, namecycle):
        """Return the object identified by namecycle.

        Raise KeyError if the object could not be retrieved.
        """
        try:
            return self._cache[namecycle]
        except KeyError:
            result = super(CachedRootFile, self).Get(namecycle)
            self._cache[namecycle] = result
            return result

