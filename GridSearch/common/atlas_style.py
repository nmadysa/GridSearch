#! /usr/bin/env python2

"""Python implementation of Atlas style."""

import ROOT
ROOT.gROOT.SetBatch(True)
ROOT.PyConfig.IgnoreCommandLineOptions = True

class AtlasStyle(ROOT.TStyle):

    """Subclass of ROOT.TStyle that conforms with ATLAS guidelines.

    Usage:
    >>> AtlasStyle().SetStyle()
    """

    def __init__(self, name="AtlasStyle", title="ATLAS style object"):
        ROOT.TStyle.__init__(self, name, title)
        self.SetName(name)
        self.SetTitle(title)
        self._configure()
    
    #_______________________________________
    def SetStyle(self):
        ROOT.gROOT.SetStyle(self.GetName())
        ROOT.gROOT.ForceStyle()

    #_______________________________________
    def _configure(self):
        # Use plain black on white colors:
        icol = ROOT.kWhite
        self.SetFrameBorderMode(icol)
        self.SetFrameFillColor(icol)
        self.SetCanvasBorderMode(icol)
        self.SetCanvasColor(icol)
        self.SetPadBorderMode(icol)
        self.SetPadColor(icol)
        self.SetStatColor(icol)

        # set the paper & margin sizes
        self.SetPaperSize(20,26)

        # set margin sizes
        self.SetPadTopMargin(0.05)
        self.SetPadRightMargin(0.05)
        self.SetPadBottomMargin(0.16)
        self.SetPadLeftMargin(0.16)

        # set title offsets (for axis label)
        self.SetTitleXOffset(1.4)
        self.SetTitleYOffset(1.4)

        # use large fonts
        #Int_t font=72 # Helvetica italics
        font = 42 # Helvetica
        tsize = 0.05
        self.SetTextFont(font)

        self.SetTextSize(tsize)
        self.SetLabelFont(font,"x")
        self.SetTitleFont(font,"x")
        self.SetLabelFont(font,"y")
        self.SetTitleFont(font,"y")
        self.SetLabelFont(font,"z")
        self.SetTitleFont(font,"z")

        self.SetLabelSize(tsize,"x")
        self.SetTitleSize(tsize,"x")
        self.SetLabelSize(tsize,"y")
        self.SetTitleSize(tsize,"y")
        self.SetLabelSize(tsize,"z")
        self.SetTitleSize(tsize,"z")

        # use bold lines and markers
        self.SetMarkerStyle(20)
        self.SetMarkerSize(1.2)
        self.SetHistLineWidth(2)
        self.SetLineStyleString(2,"[12 12]") # postscript dashes

        # get rid of X error bars 
        # self.SetErrorX(0.001)
        # get rid of error bar caps
        self.SetEndErrorSize(0.)

        # do not display any of the standard histogram decorations
        self.SetOptTitle(0)
        #self.SetOptStat(1111)
        self.SetOptStat(0)
        #self.SetOptFit(1111)
        self.SetOptFit(0)

        # put tick marks on top and RHS of plots
        self.SetPadTickX(1)
        self.SetPadTickY(1)

