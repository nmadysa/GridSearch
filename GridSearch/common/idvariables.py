#! /usr/bin/env python2

"""Module for reading ID variable config files.

This module lets you specify variables used for ID in dedicated INI
files like this:
```
[Variables]
1-prong: var1 var2 var3
multi-prong: var1 var2 var4 var5
```
"""

# The warning protected-access is caused by our usage of self-defined
# protected members on function objects.
# The warning too-few-public-methods is self-explanatory.
# pylint: disable=protected-access, too-few-public-methods


from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import re
from collections import namedtuple
from ConfigParser import RawConfigParser

from GridSearch.common.pairs import ProngPair

LOG_PATTERN = re.compile(r"log\((.*)\)")

class TranslationError(KeyError):
    """Class of exceptions raised by missing translation table entries."""
    pass


class IDVariables(object):

    """Capsulated collection of variable names for ID and training."""

    def __init__(self, filename=None):
        """Initializes a new object from an INI file or from defaults.

        filename : An INI file with sections "Variables" and
                   "TrainingVariables". Each section must have keys
                   "1-prong" and "multi-prong".
                   Each key's value must be a space-separated list of
                   variable names.

        If `filename` is None or not passed, this calls the staticmethod
        _default_names to get a ProngPair of default variable names.
        """
        if filename is None:
            self.for_ntuple = self._default_names()
        else:
            parser = RawConfigParser()
            parser.read(filename)
            self.for_ntuple = ProngPair(
                parser.get("Variables", "1-prong").split(),
                parser.get("Variables", "multi-prong").split(),
                )
        self.for_ttnt = ProngPair(
            [get_ttnt_name(name) for name in self.for_ntuple.sp],
            [get_ttnt_name(name) for name in self.for_ntuple.mp],
            )

    def write(self, file_):
        """Write the ID variables in this object to a file-like object."""
        parser = RawConfigParser()
        parser.add_section("Variables")
        parser.set("Variables", "1-prong", " ".join(self.for_ntuple.sp))
        parser.set("Variables", "multi-prong", " ".join(self.for_ntuple.mp))
        parser.write(file_)

    @staticmethod
    def _default_names():
        """Return default list of ntuple variable names."""
        return ProngPair(
            [
                "CentFrac0102",
                "FTrk02",
                "pi0_vistau_m",
                "ptRatio",
                "TrkAvgDist",
                "pi0_n",
                "IpSigLeadTrk",
                "NTracksdrdR",
                ],
            [
                "CentFrac0102",
                "FTrk02",
                "pi0_vistau_m",
                "ptRatio",
                "TrkAvgDist",
                "pi0_n",
                "MassTrkSys",
                "DrMax",
                "TrFligthPathSig",
                ],
            )


def get_ttnt_name(name):
    """Translate an ntuple name to a ttnt name.

    Returns a tuple `(ttnt_name, ttnt_type)` where `ttnt_type`
    is one of the two string "F" and "I", depending on whether the
    variable is integer- or float-based.

    The same variables have different names in files output by
    the TTNTCycle and by the NTupleCycle.
    This function translates from the latter to the former.
    """
    try:
        log_match = LOG_PATTERN.match(name)
        if log_match:
            name = log_match.group(1)
            ttnt_name, _ = get_ttnt_name._table[name]
            return "log({})".format(ttnt_name), "F"  # Logarithms are always floats.
        else:
            return get_ttnt_name._table[name]
    except KeyError:
        raise TranslationError("Cannot translate NTuple name: " + repr(name))
get_ttnt_name._table = {
    "CentFrac0102": ("tau_calcVars_corrCentFrac", "F"),
    "FTrk02": ("tau_calcVars_corrFTrk", "F"),
    "pi0_vistau_m": ("tau_pi0_visTau_m", "F"),
    "ptRatio": ("tau_ptRatio", "F"),
    "TrkAvgDist": ("tau_seedCalo_trkAvgDist", "F"),
    "pi0_n": ("tau_pi0_n", "I"),
    "IpSigLeadTrk": ("tau_ipSigLeadTrk", "F"),
    "NTracksdrdR": ("tau_seedCalo_wideTrk_n", "I"),
    "MassTrkSys": ("tau_massTrkSys", "F"),
    "DrMax": ("tau_seedCalo_dRmax", "F"),
    "TrFligthPathSig": ("tau_trFlightPathSig", "F"),
    "panTauCellBased_nChrg0204": ("panTauCellBased_nChrg0204", "F"),
    "panTauCellBased_nChrg01": ("panTauCellBased_nChrg01", "F"),
    "panTauCellBased_nChrg02": ("panTauCellBased_nChrg02", "F"),
    "panTauCellBased_nChrg04": ("panTauCellBased_nChrg04", "F"),
    "panTauCellBased_nNeut0204": ("panTauCellBased_nNeut0204", "F"),
    "panTauCellBased_nNeut01": ("panTauCellBased_nNeut01", "F"),
    "panTauCellBased_nNeut02": ("panTauCellBased_nNeut02", "F"),
    "panTauCellBased_nNeut04": ("panTauCellBased_nNeut04", "F"),
    "panTauCellBased_massChrgSys01": ("panTauCellBased_massChrgSys01", "F"),
    "panTauCellBased_massChrgSys02": ("panTauCellBased_massChrgSys02", "F"),
    "panTauCellBased_massChrgSys04": ("panTauCellBased_massChrgSys04", "F"),
    "panTauCellBased_massNeutSys01": ("panTauCellBased_massNeutSys01", "F"),
    "panTauCellBased_massNeutSys02": ("panTauCellBased_massNeutSys02", "F"),
    "panTauCellBased_massNeutSys04": ("panTauCellBased_massNeutSys04", "F"),
    "panTauCellBased_visTauM01": ("panTauCellBased_visTauM01", "F"),
    "panTauCellBased_visTauM02": ("panTauCellBased_visTauM02", "F"),
    "panTauCellBased_visTauM04": ("panTauCellBased_visTauM04", "F"),
    "panTauCellBased_dRmax01": ("panTauCellBased_dRmax01", "F"),
    "panTauCellBased_dRmax02": ("panTauCellBased_dRmax02", "F"),
    "panTauCellBased_dRmax04": ("panTauCellBased_dRmax04", "F"),
    "panTauCellBased_ipSigLeadTrk": ("panTauCellBased_ipSigLeadTrk", "F"),
    "panTauCellBased_trFlightPathSig": ("panTauCellBased_trFlightPathSig", "F"),
    "panTauCellBased_ptRatio01": ("panTauCellBased_ptRatio01", "F"),
    "panTauCellBased_ptRatio02": ("panTauCellBased_ptRatio02", "F"),
    "panTauCellBased_ptRatio04": ("panTauCellBased_ptRatio04", "F"),
    "panTauCellBased_ptRatioNeut01": ("panTauCellBased_ptRatioNeut01", "F"),
    "panTauCellBased_ptRatioNeut02": ("panTauCellBased_ptRatioNeut02", "F"),
    "panTauCellBased_ptRatioNeut04": ("panTauCellBased_ptRatioNeut04", "F"),
    "panTauCellBased_ptRatioChrg01": ("panTauCellBased_ptRatioChrg01", "F"),
    "panTauCellBased_ptRatioChrg02": ("panTauCellBased_ptRatioChrg02", "F"),
    "panTauCellBased_ptRatioChrg04": ("panTauCellBased_ptRatioChrg04", "F"),
    "panTauCellBased_fLeadChrg01": ("panTauCellBased_fLeadChrg01", "F"),
    "panTauCellBased_fLeadChrg02": ("panTauCellBased_fLeadChrg02", "F"),
    "panTauCellBased_fLeadChrg04": ("panTauCellBased_fLeadChrg04", "F"),
    "panTauCellBased_eFrac0102": ("panTauCellBased_eFrac0102", "F"),
    "panTauCellBased_eFrac0204": ("panTauCellBased_eFrac0204", "F"),
    "panTauCellBased_eFrac0104": ("panTauCellBased_eFrac0104", "F"),
    "panTauCellBased_eFracChrg0102": ("panTauCellBased_eFracChrg0102", "F"),
    "panTauCellBased_eFracChrg0204": ("panTauCellBased_eFracChrg0204", "F"),
    "panTauCellBased_eFracChrg0104": ("panTauCellBased_eFracChrg0104", "F"),
    "panTauCellBased_eFracNeut0102": ("panTauCellBased_eFracNeut0102", "F"),
    "panTauCellBased_eFracNeut0204": ("panTauCellBased_eFracNeut0204", "F"),
    "panTauCellBased_eFracNeut0104": ("panTauCellBased_eFracNeut0104", "F"),
    "panTauCellBased_eFrac010201": ("panTauCellBased_eFrac010201", "F"),
    "panTauCellBased_eFrac010202": ("panTauCellBased_eFrac010202", "F"),
    "panTauCellBased_eFrac010204": ("panTauCellBased_eFrac010204", "F"),
    "panTauCellBased_eFracChrg010201": ("panTauCellBased_eFracChrg010201", "F"),
    "panTauCellBased_eFracChrg010202": ("panTauCellBased_eFracChrg010202", "F"),
    "panTauCellBased_eFracChrg010204": ("panTauCellBased_eFracChrg010204", "F"),
    "panTauCellBased_eFracNeut010201": ("panTauCellBased_eFracNeut010201", "F"),
    "panTauCellBased_eFracNeut010202": ("panTauCellBased_eFracNeut010202", "F"),
    "panTauCellBased_eFracNeut010204": ("panTauCellBased_eFracNeut010204", "F"),
    "panTauCellBased_eFrac020401": ("panTauCellBased_eFrac020401", "F"),
    "panTauCellBased_eFrac020402": ("panTauCellBased_eFrac020402", "F"),
    "panTauCellBased_eFrac020404": ("panTauCellBased_eFrac020404", "F"),
    "panTauCellBased_eFracChrg020401": ("panTauCellBased_eFracChrg020401", "F"),
    "panTauCellBased_eFracChrg020402": ("panTauCellBased_eFracChrg020402", "F"),
    "panTauCellBased_eFracChrg020404": ("panTauCellBased_eFracChrg020404", "F"),
    "panTauCellBased_eFracNeut020401": ("panTauCellBased_eFracNeut020401", "F"),
    "panTauCellBased_eFracNeut020402": ("panTauCellBased_eFracNeut020402", "F"),
    "panTauCellBased_eFracNeut020404": ("panTauCellBased_eFracNeut020404", "F"),
    "panTauCellBased_rCal01": ("panTauCellBased_rCal01", "F"),
    "panTauCellBased_rCal02": ("panTauCellBased_rCal02", "F"),
    "panTauCellBased_rCal04": ("panTauCellBased_rCal04", "F"),
    "panTauCellBased_rCalChrg01": ("panTauCellBased_rCalChrg01", "F"),
    "panTauCellBased_rCalChrg02": ("panTauCellBased_rCalChrg02", "F"),
    "panTauCellBased_rCalChrg04": ("panTauCellBased_rCalChrg04", "F"),
    "panTauCellBased_rCalNeut01": ("panTauCellBased_rCalNeut01", "F"),
    "panTauCellBased_rCalNeut02": ("panTauCellBased_rCalNeut02", "F"),
    "panTauCellBased_rCalNeut04": ("panTauCellBased_rCalNeut04", "F"),
    "panTauCellBased_PtChrg04": ("panTauCellBased_PtChrg04", "F"),
    "panTauCellBased_dRminmaxPtChrg04": ("panTauCellBased_dRminmaxPtChrg04",
                                         "F"),
    }


def get_ntuple_name(name):
    """Reverse-look up an ntuple name based on a known ttnt name."""
    # Only build this function's table if it's necessary.
    if get_ntuple_name._table is None:
        get_ntuple_name._table = {
            _ttnt: _ntuple
            for (_ntuple, (_ttnt, _)) in get_ttnt_name._table.items()
            }
    try:
        log_match = LOG_PATTERN.match(name)
        if log_match:
            name = log_match.group(1)
            return "log({})".format(get_ntuple_name._table[name])
        else:
            return get_ntuple_name._table[name]
    except KeyError:
        raise TranslationError("Cannot translate TTNT name: " + repr(name))
get_ntuple_name._table = None


def get_nice_name(ntuple_name):
    """Return a nice ROOT.TLatex formula for the given variable."""
    # Handle "log(varname)".
    log_match = LOG_PATTERN.match(ntuple_name)
    if log_match:
        ntuple_name = log_match.group(1)
        return "log({})".format(get_nice_name(ntuple_name))
    # Base case.
    if ntuple_name.startswith("panTauCellBased_"):
        return SubstructureName(ntuple_name).nice()
    else:
        try:
            return get_nice_name._std_vars[ntuple_name]
        except KeyError:
            raise TranslationError("Not a standard variable: " +
                                   repr(ntuple_name))
get_nice_name._std_vars = {
    "CentFrac0102": "f_{core}^{corr}",
    "FTrk02": "f_{track}^{corr}",
    "pi0_vistau_m": "m_{#tau}^{vis} [MeV]",
    "ptRatio": "f_{vis-p_{T}}",
    "TrkAvgDist": "R_{track}",
    "pi0_n": "N_{#pi^{0}}",
    "IpSigLeadTrk": "S_{lead trk}^{IP}",
    "NTracksdrdR": "N_{track}^{iso}",
    "MassTrkSys": "m_{tracks} [MeV]",
    "DrMax": "#DeltaR_{max}",
    "TrFligthPathSig": "S_{T}^{flight}",
    }


class SubstructureName(object):

    """Represents a substructure variable name."""

    cone_regions = {"01": "cent", "02": "core", "04": "iso"}

    Info = namedtuple("Info", ["main", "sub", "sup", "unit"])

    _nice_names = {
        "rCal" : Info("R", ["cal"], [], None),
        "eFrac" : Info("f", ["E_{T}"], [], None),
        "n" : Info("N", [], [], None),
        "mass" : Info("m", [], [], "GeV"),
        "ptRatio" : Info("f", ["p_{T}"], [], None),
        "dRmax" : Info("#DeltaR", ["max"], [], None),
        "visTauM" : Info("m", ["vis"], [], "GeV"),
        "fLead" : Info("f", ["lead"], [], None),
        "dRminmaxPt" : Info("#DeltaR", ["p_{T}^{minmax}"], [], None),
        "Pt": Info("p", ["T"], [], None),
        "ipSigLeadTrk": Info("S", ["lead trk"], ["IP"], None),
        "trFlightPathSig": Info("S", ["T"], ["flight"], None),
        }

    def __init__(self, ntuple_name):
        """Create an instance"""
        # Remove prefix.
        _, _, wo_prefix = ntuple_name.partition("panTauCellBased_")
        if not wo_prefix:
            raise TranslationError("Not a substructure variable: " +
                                   repr(ntuple_name))
        # Get cones:
        self.cones = []
        while wo_prefix[-2:] in self.cone_regions:
            wo_prefix, cone = wo_prefix[:-2], wo_prefix[-2:]
            self.cones.append(cone)
        self.cones.reverse()
        # Get charge filter. Remainder is actual variable name.
        for suffix in ["neut", "chrg", "neutsys", "chrgsys"]:
            if wo_prefix.lower().endswith(suffix):
                self.charge = suffix.partition("sys")[0]
                self.bare_name = wo_prefix[:-len(suffix)]
                break
        else:
            self.charge = ""
            self.bare_name = wo_prefix

    def nice(self):
        """Translate to a nice ROOT.TLatex formula."""
        main, sub, sup, unit = self._nice_names[self.bare_name]
        # Shallow copy to avoid mutating mutable values.
        sub = sub[:]
        sup = sup[:]
        # Add subscript based on charge filters.
        if self.charge and self.bare_name != "dRminmaxPt":
            sub.append(self.charge)
        # Add super-script based on cone regions.
        if self.bare_name == "n" and self.cones == ["02", "04"]:
            sup.append("wide")
        else:
            for cone in self.cones:
                sup.append(self.cone_regions[cone])
        # Translate.
        return "{main}_{{{sub}}}^{{{sup}}}{unitspec}".format(
            main=main, sub=",".join(sub), sup="-".join(sup),
            unitspec=(" [{}]".format(unit) if unit else ""),
            )
