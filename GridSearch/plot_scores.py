#! /usr/bin/env python2

"""Plot score histograms from efficiency evaluation output."""

from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import os
import sys
import argparse

import ROOT  # pylint: disable=import-error
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)

from GridSearch import plotting
from GridSearch import common
from GridSearch.common import PyRootFile, SigBkgPair


class ScorePlotter(object):

    """Class used to create the plot scores.

    Usage:
    >>> plotter = ScorePlotter(nprong, sig_file, bkg_file)
    >>> for canvas in plotter.get_plots():
    ...     canvas.SaveAs(canvas.GetName()+".png")
    """

    # Bounding rectangle specification for the score histograms' legends.
    # These vales are, in order: left, bottom, right, top.
    legend_rect = [0.6, 0.7, 0.9, 0.9]

    def __init__(self, nprong, sig_filename, bkg_filename):
        """Construct a ScorePlotter object.

        nprong: Denotes the prongness of the input files.
                1: 1-prong
                2: multi-prong
                3: 3-prong
        sig_filename,
        bkg_filename  : Input files. The results of the
                        EfficiencyEvaluationCycle.
        """
        if nprong not in {1, 2, 3}:
            raise common.NProngError(nprong)
        self.files = SigBkgPair(PyRootFile(sig_filename),
                                PyRootFile(bkg_filename))
        self.nprong = nprong
        self.mva_methods = ["BDTScore", "defaultBDTScore", "MLPScore"]
        self.all_histograms = {}

    def get_plots(self):
        """Create score histograms of all MVA methods.

        Return a list of TCanvas objects, each containing a
        score histogram.
        """
        canvases = []
        prong_name = {1: "1p", 2: "mp", 3: "3p"}[self.nprong]
        prong_text = {1: "1-prong", 2: "multi-prong",
                      3: "3-prong"}[self.nprong]
        for method in self.mva_methods:
            hists = self.load_histograms(method)
            canvas = ROOT.TCanvas("Score_{}_{}".format(method, prong_name),
                                  "Scores", 800, 700)
            canvas.cd()
            # Style and draw histograms.
            self.draw_histograms(hists.sig, hists.bkg)
            hists.sig.GetXaxis().SetTitle(method)
            # Additional formatting on the canvas.
            plotting.create_legend(hists, self.legend_rect, "P")
            plotting.create_text(prong_text, (0.2, 0.9), size=0.04)
            plotting.create_atlas_label(x_y=(0.2, 0.85),
                                        offset=(0.0, -0.04))
            canvas.Update()
            canvases.append(canvas)
        return canvases

    # --- Helper methods ---

    def load_histograms(self, method_name):
        """Load the score distribution of an MVA method.

        Load the histogram "h_{method_name}" from signal and background
        file and normalize them.

        method_name : Name of the MVA method whose histograms are
                      to be loaded.
        Returns a SigBkgPair tuple of signal and background histogram.
        """
        # Load from files, give them good names.
        hists = SigBkgPair(self.files.sig.Get("h_"+method_name),
                           self.files.bkg.Get("h_"+method_name))
        hists.sig.SetTitle("signal")
        hists.bkg.SetTitle("data")
        # Rebin and normalize.
        for hist in hists:
            hist.Rebin(5)
            hist.Scale(1.0 / hist.Integral())
        # Cache histograms.
        self.all_histograms[method_name] = hists
        return hists

    @staticmethod
    def draw_histograms(sig_histo, bkg_histo):
        """Draw the passed histograms to the current canvas.

        Convenience function that sets colors and adjusts the Y axis.
        """
        sig_histo.SetMarkerColor(2)
        sig_histo.SetMarkerStyle(20)
        sig_histo.Draw("P")
        bkg_histo.Draw("sameP")
        y_max = max(sig_histo.GetMaximum(), bkg_histo.GetMaximum())
        sig_histo.SetMaximum(1.5*y_max)
        sig_histo.SetMinimum(sig_histo.GetMaximum()/1.e4)
        bkg_histo.SetMinimum(bkg_histo.GetMaximum()/1.e4)


def get_parser():
    """Return an argparse.ArgumentParser instance."""
    parser = argparse.ArgumentParser(
        description="Plotter for score distributions")
    parser.add_argument("filelist", metavar="FILE", nargs=2,
                        help="Input filelist (efficiency evaluation output)")
    parser.add_argument("--nprong", "-n", type=int, required=True,
                        help="Prongness of the file. 1, 2, or 3.")
    parser.add_argument("--outdir", "-o", default=os.curdir,
                        help="Output directory")
    return parser


def main(argv):
    """Main function. You should pass `sys.argv[1:]` as argument."""
    args = get_parser().parse_args(argv)

    if PyRootFile.is_signal_file(args.filelist[0]):
        sig_name, bkg_name = args.filelist
    else:
        bkg_name, sig_name = args.filelist

    plotting.set_atlas_style()
    score_plotter = ScorePlotter(args.nprong, sig_name, bkg_name)
    for canvas in score_plotter.get_plots():
        outpath = os.path.join(args.outdir, canvas.GetName()+".eps")
        canvas.SaveAs(outpath)

if __name__ == '__main__':
    main(sys.argv[1:])
