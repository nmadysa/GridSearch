#! /usr/bin/env python2

"""Plot the signal efficiency over pT or mu for different working points."""

import os
import sys
import argparse

import ROOT  # pylint: disable=import-error
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)

from GridSearch import common
from GridSearch import plotting

sys.path.append(os.path.join(os.pardir, os.pardir, "TauCommon", "macros"))
try:
    import HistTools
    from HistFormatter import PlotOptions as PO
finally:
    sys.path.pop()

plotter = HistTools.Plotter()
formatter = HistTools.Formatter()

class StylePlots(object):

    """Purely static class to separate plotting from computation."""

    @staticmethod
    def plot_reco_efficiency(reco_eff):
        """Plot a single curve and label it "reco efficiency"."""
        return plotter.plotHistograms(
            [reco_eff],
            plotOptions=[PO(marker_color=ROOT.kBlue, marker_style=21)],
            drawOptions=["P"],
            canvasName="reco efficiency",
            )


    @staticmethod
    def plot_single_efficiency(loose, medium, tight, varname="pt"):
        """Plot signal efficiency curves for three working points.

        loose, medium, tight : TH1F instances to be plotted.
        varname              : Name of the independent variable.
        """
        return plotter.plotHistograms(
            [loose, medium, tight],
            plotOptions=[
                PO(marker_color=ROOT.kGreen, marker_style=22),
                PO(marker_color=ROOT.kBlue, marker_style=21),
                PO(marker_color=ROOT.kRed, marker_style=23)
                ],
            drawOptions=3*["P"],
            canvasName="signal efficiency vs {}".format(varname),
            )

    @staticmethod
    def plot_combined_efficiencies(first, second, varname="pt"):
        """Plot signal efficiency curves for two different methods.

        Like plot_single_efficiency, but plots two different TMVA methods
        in the same canvas.
        Makes sure all curves can be distinguished by maker color and style.

        first, second : Tuples `(loose, medium, tight)` of
                        TH1F instances to be plotted.
        varname       : Name of the independent variable.
        """
        assert len(first) == len(second) == 3
        return plotter.plotHistograms(
            first + second,
            plotOptions=[
                PO(marker_color=ROOT.kGreen, marker_style=22),
                PO(marker_color=ROOT.kBlue, marker_style=21),
                PO(marker_color=ROOT.kRed, marker_style=23),
                PO(marker_color=ROOT.kGreen, marker_style=26),
                PO(marker_color=ROOT.kBlue, marker_style=25),
                PO(marker_color=ROOT.kRed, marker_style=32),
                ],
            drawOptions=6*["P"],
            canvasName="signal efficiency vs {}".format(varname),
            )

    @staticmethod
    def add_legend(canvas, **kwargs):
        """Wrapper around HistTools.Formatter.addLegendToCanvas.

        This is a wrapper to have all dependencies on TauCommon
        in one place.
        """
        return formatter.addLegendToCanvas(canvas, **kwargs)

    @staticmethod
    def add_default_labels(canvas, nprong, method="", top=0.86):
        """Add various text labels to a canvas.

        Add luminposity information, the ATLAS label, and information
        about prongness and the used TMVA method to a canvas.

        nprong : 1, 2, or 3.
        method : Name of the TMVA method that was used.
        top    : Coordinate where to put the top-most text.
                 Usual values are 0.8 (far up) and 0.4 (far down).
        """
        prong_text = {1: "1 prong", 2: "multi-prong", 3: "3 prong"}[nprong]
        if method:
            method_text = "{}, {}".format(prong_text, method)
        else:
            method_text = prong_text
        plotting.create_energy_text(x_y=(0.6, top))
        plotting.create_text(method_text, x_y=(0.6, top-0.05))

    @staticmethod
    def _set_axes(graph, x_range, y_range, x_title, y_title):
        """Set axis ranges and labels."""
        xlow, xup = x_range
        ylow, yup = y_range
        formatter.setMinMax(graph, xlow, xup, 'x')
        formatter.setMinMax(graph, ylow, yup, 'y')
        formatter.setTitle(graph, x_title, 'x')
        formatter.setTitle(graph, y_title, 'y')

    @classmethod
    def set_pt_signal_axes(cls, graph):
        """Set x axis to true pT-vis, the y axis to signal efficiency."""
        cls._set_axes(graph, (10, 160), (0, 1.3), "true p^{vis}_{T} [GeV]",
                      "signal efficiency")

    @classmethod
    def set_mu_signal_axes(cls, graph):
        """Set x axis to mu, the y axis to signal efficiency."""
        cls._set_axes(graph, (0, 40), (0, 1.3), "#mu", "signal efficiency")

    @classmethod
    def set_pt_reco_axes(cls, graph):
        """Set x axis to true pT-vis, the y axis to reco efficiency."""
        cls._set_axes(graph, (10, 160), (0, 1.2), "true p^{vis}_{T} [GeV]",
                      "reco efficiency")

    @classmethod
    def set_mu_reco_axes(cls, graph):
        """Set x axis to mu, the y axis to reco efficiency."""
        cls._set_axes(graph, (0, 40), (0, 1.2), "#mu", "reco efficiency")



class EfficiencyPlotter(object):

    """Main class of this script. Creates all necessary plots.

    Usage:
    >>> plotter = EfficiencyPlotter("EfficiencyEvaluationCycle.mc.Signal.root",
    ...                             nprong=2)
    >>> for canvas in plotter.plotEfficiencyVsPt():
    ...     canvas.SaveAs(".eps")
    """

    def __init__(self, sig_file, nprong, overlap=False):
        """Create a new EfficiencyPlotter instance.

        sig_file : Paths to the ROOT file.
        nprong   : Prongness of the input files. Either 1, 2, or 3.
        overlap  : If passed and True, so also compute the overlap of
                   MLP and BDT.
        """
        if nprong not in {1, 2, 3}:
            raise common.NProngError(nprong)
        self.nprong = nprong
        self.outputdirectory = {1: "1prong", 2: "mprong",
                                3: "3prong"}[self.nprong]

        if not common.PyRootFile.is_signal_file(sig_file):
            raise ValueError("Not a signal file: {!s}".format(sig_file))
        self.infile = common.PyRootFile(sig_file)
        self.workpoints = ["loose", "medium", "tight"]
        self.rebin = 4
        self.plotdict = {}
        if overlap:
            self.algos = ["MLP", "BDT", "defaultBDT", "MLP_BDT"]
        else:
            self.algos = ["MLP", "BDT", "defaultBDT"]

    def getHistPt(self, wp, algo):
        """Return the histogram of passing signal events over pT."""
        return self.infile.Get('h_passed_%s_%s_pt' % (algo, wp))

    def getHistMu(self, wp, algo):
        """Return the histogram of passing signal events over mu."""
        return self.infile.Get('h_passed_%s_%s_mu' % (algo, wp))

    def getTotalHistPt(self):
        """Return the histogram of reconstructed signal events over pT."""
        return self.infile.Get('h_total_pt')

    def getTotalHistMu(self):
        """Return the histogram of reconstructed signal events over mu."""
        return self.infile.Get('h_total_mu')

    def getPtNormHist(self):
        """Return histogram of truth particles over pT."""
        prong_bin = 1+(1 if self.nprong == 1 else 3)
        h_prong_pt_mu = self.infile.Get('h_truthPt')
        h_pt = h_prong_pt_mu.ProjectionY("norm", prong_bin, prong_bin, 0, -1)
        return h_pt.Rebin(self.rebin)

    def getMuNormHist(self):
        """Return histogram of truth particles over mu."""
        prong_bin = 1+(1 if self.nprong == 1 else 3)
        h_prong_pt_mu = self.infile.Get('h_truthPt')
        h_mu = h_prong_pt_mu.ProjectionZ("norm", prong_bin, prong_bin, 0, -1)
        return h_mu

    def plotEfficiencyVsPt(self):
        """Return iterator that yields canvases of signal eff. over pT."""
        norm_hist = self.getPtNormHist()
        for algo in self.algos:
            # Calculation.
            lmt_graphs = []
            for workpoint in self.workpoints:
                h_eff = self.getHistPt(workpoint, algo)
                h_eff.Rebin(self.rebin)
                h_eff.Divide(norm_hist)
                h_eff.SetName(workpoint)
                lmt_graphs.append(h_eff)
            self.plotdict[algo] = lmt_graphs
            # Plotting.
            canvas = StylePlots.plot_single_efficiency(*lmt_graphs,
                                                       varname="pt")
            StylePlots.set_pt_signal_axes(lmt_graphs[0])
            StylePlots.add_default_labels(canvas, self.nprong, algo)
            StylePlots.add_legend(
                canvas, overwriteDrawOptions=3*['P'],
                legendOptions={'x1': 0.2, 'x2': 0.4, 'y1': 0.65, 'y2': 0.9},
                )
            canvas.Update()
            canvas.SetName("SignalEfficiency_{}_{}_pt"
                           .format(algo, self.outputdirectory))
            yield canvas

    def plotEfficiencyVsMu(self):
        """Return iterator that yields canvases of signal eff. over mu."""
        norm_hist = self.getMuNormHist()
        for algo in self.algos:
            # Calculation.
            lmt_graphs = []
            for workpoint in self.workpoints:
                h_eff = self.getHistMu(workpoint, algo)
                h_eff.Divide(norm_hist)
                h_eff.SetName(workpoint)
                lmt_graphs.append(h_eff)
            # Plotting.
            canvas = StylePlots.plot_single_efficiency(*lmt_graphs,
                                                       varname="mu")
            StylePlots.set_mu_signal_axes(lmt_graphs[0])
            StylePlots.add_default_labels(canvas, self.nprong, algo)
            StylePlots.add_legend(
                canvas, overwriteDrawOptions=3*['P'],
                legendOptions={'x1': 0.2, 'x2': 0.4, 'y1': 0.7, 'y2': 0.9},
                )
            canvas.Update()
            canvas.SetName("SignalEfficiency_{}_{}_mu"
                           .format(algo, self.outputdirectory))
            yield canvas

    def plotCombinedEfficiencies(self):
        """Return iterator that yields canvases of signal eff. over pt.

        This method plots both a method and its default variant.
        """
        a_graph = self.plotdict["BDT"][0]  # Grab an arbitrary TGraph object.
        for algo in ['BDT']:
            canvas = StylePlots.plot_combined_efficiencies(
                self.plotdict['default'+algo], self.plotdict[algo], "pt")
            StylePlots.set_pt_signal_axes(a_graph)
            StylePlots.add_default_labels(canvas, self.nprong, algo)
            StylePlots.add_legend(
                canvas,
                legendOptions={'x1': 0.2, 'x2': 0.4, 'y1': 0.65, 'y2': 0.9},
                overwriteLabels=['default loose', 'default medium',
                                 'default tight', 'loose', 'medium', 'tight'],
                overwriteDrawOptions=6*['P'],
                )
            canvas.Update()
            canvas.SetName("CombinedSignalEfficiency_{}_{}_pt"
                           .format(algo, self.outputdirectory))
            yield canvas

    def plotOverlap(self):
        """Return canvas of a signal eff. over pT plot for BDT and overlap."""
        a_graph = self.plotdict["BDT"][0]  # Grab an arbitrary TGraph object.
        canvas = StylePlots.plot_combined_efficiencies(
            self.plotdict['BDT'], self.plotdict['LLH_BDT'], "pt")
        StylePlots.set_pt_signal_axes(a_graph)
        StylePlots.add_default_labels(canvas, self.nprong, "overlap BDT LLH")
        StylePlots.add_legend(
            canvas,
            legendOptions={'x1': 0.2, 'x2': 0.4, 'y1': 0.65, 'y2': 0.9},
            overwriteLabels=['loose BDT', 'medium BDT', 'tight BDT',
                             'loose overlap', 'medium overlap',
                             'tight overlap'],
            overwriteDrawOptions=6*['P'],
            )
        canvas.Update()
        canvas.SetName("OverlapSignalEfficiency_{}_pt"
                       .format(self.outputdirectory))
        return canvas

    def plotRecoEffPt(self):
        """Return canvas of signal reconstruction efficiency over pT."""
        # Calculation.
        reco_eff = self.getTotalHistPt()
        reco_eff.Rebin(self.rebin)
        norm_hist = self.getPtNormHist()
        reco_eff.Divide(norm_hist)
        # Plotting.
        canvas = StylePlots.plot_reco_efficiency(reco_eff)
        StylePlots.set_pt_reco_axes(reco_eff)
        StylePlots.add_default_labels(canvas, self.nprong, method=None,
                                      top=0.4)
        StylePlots.add_legend(
            canvas,
            legendOptions={'x1': 0.2, 'x2': 0.5, 'y1': 0.3, 'y2': 0.4},
            overwriteLabels=['reco efficiency'],
            overwriteDrawOptions=['P'],
            )
        canvas.Update()
        canvas.SetName("RecoEfficiency_{}_pt".format(self.outputdirectory))
        return canvas

    def plotRecoEffMu(self):
        """Return canvas of signal reconstruction efficiency over mu."""
        # Calculation.
        reco_eff = self.getTotalHistMu()
        norm_hist = self.getMuNormHist()
        reco_eff.Divide(norm_hist)
        # Plotting.
        canvas = StylePlots.plot_reco_efficiency(reco_eff)
        StylePlots.set_mu_reco_axes(reco_eff)
        StylePlots.add_default_labels(canvas, self.nprong, method=None,
                                      top=0.4)
        StylePlots.add_legend(
            canvas,
            legendOptions={'x1': 0.2, 'x2': 0.5, 'y1': 0.3, 'y2': 0.4},
            overwriteLabels=['reco efficiency'],
            overwriteDrawOptions=['P'],
            )
        canvas.Update()
        canvas.SetName("RecoEfficiency_{}_mu".format(self.outputdirectory))
        return canvas


def save_canvas(canvas, outdir):
    """Convenience function to save a canvas in a specific folder."""
    filename = canvas.GetName() + ".eps"
    filepath = os.path.join(outdir, filename)
    canvas.SaveAs(filepath)

def get_parser():
    """Return an argparse.ArgumentParser instance."""
    parser = argparse.ArgumentParser(
        description="Plot signal efficiency over pT and mu")
    parser.add_argument("file", metavar="FILE",
                        help="input file (efficiency evaluation output)")
    parser.add_argument("--nprong", "-n", required=True,
                        choices={1, 2, 3}, type=int,
                        help="prongness of the input file")
    parser.add_argument("--outdir", "-o", default=os.curdir,
                        help="output directory")
    parser.add_argument("--overlap", action="store_true",
                        help="test overlap of llh and bdt")
    return parser


def main(argv):
    """Main function. You should pass `sys.argv[1:]` as argument."""
    args = get_parser().parse_args(argv)

    plotter = EfficiencyPlotter(args.file, args.nprong, args.overlap)
    for canvas in  plotter.plotEfficiencyVsPt():
        save_canvas(canvas, args.outdir)
    for canvas in  plotter.plotEfficiencyVsMu():
        save_canvas(canvas, args.outdir)
    for canvas in  plotter.plotCombinedEfficiencies():
        save_canvas(canvas, args.outdir)
    if args.overlap:
        canvas = plotter.plotOverlap()
        save_canvas(canvas, args.outdir)
    for get_canvas in [plotter.plotRecoEffPt, plotter.plotRecoEffMu]:
        canvas = get_canvas()
        save_canvas(canvas, args.outdir)


if __name__ == '__main__':
    main(sys.argv[1:])
