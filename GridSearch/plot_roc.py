#! /usr/bin/env python2

"""Script to create ROC curves and their ratio plots.

# Usage

This script takes a list of fully evaluated classifiers and plots their
ROC curves in a single diagram.

A classifier is defined as a tuple `(folder, name)`, where:
    * `folder` is an ANN folder created by `GridSearch.init_run` and
      fully evaluated by `GridSearch.do_run`,
    * `name` is the classifier name that defines it inside the output
      files of OfflineLLH::EfficiencyEvaluationCycle.
      The three most common names are "MLP", "BDT", and "defaultBDT".

Classifiers may be passed to this script on two ways: by method or by
folder. Both ways allow you to pass a series of the same command-line
option to identify all passed classifiers:
    * --by-method <name> <ANN folder> [<ANN folder>...]
      This iterates over all passed ANN folders and searches for the
      classifier <name> in all of them.
    * --by-folder <ANN folder> <name> [<name>...]
      This takes a single ANN folder and searches for all classifier
      names inside it.

The option --names allows renaming some or all of the passed
classifiers.

The option --nprong is required to let the script know where to search
for the classifiers *inside* the ANN folders.
`--nprong 1` locates the files in "<ANN folder>/1prong/evaluation",
while `--nprong 2` and `--nprong 3` locate them in
"<ANN folder>/3prong/evaluation".

# Examples

Compare a BDT and an MLP trained in the same ANN folder:
```
python -m GridSearch.plot_roc -n1 -F ANN_00000 BDT MLP
```

Compare two MLPs, which have been trained by different algorithms, to a
BDT which has been trained in a third folder.
```
python -m GridSearch.plot_roc -n1 --names - BackProp BFGS \\
    -M BDT ANN_bdt \\
    -M MLP ANN_backprop ANN_bfgs
```
"""

from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import os
import sys
import glob
import argparse

import ROOT
ROOT.gROOT.SetBatch(True)  # pylint: disable=no-member
ROOT.PyConfig.IgnoreCommandLineOptions = True

from GridSearch import roc
from GridSearch import common
from GridSearch import plotting


class RocPlotter(object):

    """Class that creates ROC diagrams both with and without ratio plots.

    Usage:
    >>> plotter = RocPlotter(nprong, luminosity=20.2814)
    >>> plotter.add_reference_curve("defaultBDT", curves[0])
    >>> plotter.add_roc_curve("MLP", curves[1])
    >>> canvas = plotter.get_roc_plot()
    >>> canvas.SaveAs(".eps")
    """

    x_range = (0.01, 3.0)
    roc_y_range = (1.01e0, 1e4)
    ratio_y_range = (0.6, 1.3)
    graph_colors = [
        ROOT.kBlack,    # pylint: disable=no-member
        ROOT.kRed-9,    # pylint: disable=no-member
        ROOT.kBlue-5,   # pylint: disable=no-member
        ROOT.kGreen-1,  # pylint: disable=no-member
        3, 11, 12, 13, 14, 15, 16,
        ]
    graph_linestyles = list(range(1, 11))
    n_max_graphs = min(len(l) for l in [graph_colors, graph_linestyles])

    def __init__(self, nprong, pt_min_max=(0, -1), luminosity=None):
        """Create an object of class RocPlotter.

        nprong     : prongness to be written on the plot.
                     `1` for 1-prong, `2` for multi-prong, `3` for
                     3-prong.
        pt_min_max : pT range in which to select particles.
        luminosity : If passed and not None, this luminosity value
                     will be written on the plot.
        """
        if nprong not in {1, 2, 3}:
            raise ValueError("Invalid prong value: {}".format(nprong))
        self.nprong = nprong
        self.ptmin, self.ptmax = pt_min_max
        self.luminosity = luminosity
        self._roc_curves = []
        self._ratio_curves = []
        self._reference_roc = None

    # --- ROC curve handling ---

    def add_roc_curve(self, title, roc_curve, is_ref=False):
        """Add a ROC curve to this plot.

        title     : Title of the curve. Used in the plot's legend.
        roc_curve : A `roc.Curve` instance.
        is_ref    : If passed and `True`, `roc_curve` shall be the
                    reference curve of this plot.

        The reference ROC curve will be drawn in black and used as the
        denominator in ratio plots.
        """
        try:
            index = len(self._roc_curves)
            color = self.graph_colors[index]
            linestyle = self.graph_linestyles[index]
        except IndexError:
            raise IndexError("Cannot add more than {:d} graphs to a "
                             "RocPlotter".format(self.n_max_graphs))
        # Style the curve's graph as necessary.
        graph = roc_curve.graph
        graph.SetTitle(title)
        graph.GetXaxis().SetTitle("Signal Efficiency")
        graph.GetXaxis().SetRangeUser(*self.x_range)
        graph.GetYaxis().SetTitle("Inverse Background Efficiency")
        graph.GetYaxis().SetRangeUser(*self.roc_y_range)
        plotting.style_object(graph, markerstyle=21, markercolor=color,
                              markersize=0.8, linewidth=3,
                              linestyle=linestyle, linecolor=color)
        self._roc_curves.append(roc_curve)
        if is_ref:
            self._reference_roc = roc_curve

    # --- Canvas-returning functions ---

    def get_roc_plot(self):
        """Return a canvas with a plot of all ROC curves."""
        canvas = self._create_canvas("roc")
        self._draw_roc_diagram()
        self._add_labels(0.04)
        return canvas

    def get_roc_ratio_plot(self):
        """Return a canvas with a normal and ratio plot of the ROC curves."""
        # Create the canvas.
        canvas = self._create_canvas("ratio")
        # Create and fill the pads.
        y_frac = 0.29
        up_pad, low_pad = self._create_ratio_pads(canvas, y_frac)
        # Draw everything.
        up_pad.cd()
        roc_graphs = self._draw_roc_diagram()
        self._add_labels(0.06)
        low_pad.cd()
        ratio_graphs = self._draw_ratio_diagram()
        canvas.cd()
        up_pad.Draw()
        low_pad.Draw()
        # Correct margins, offsets, and font sizes.
        self._hide_axis_labels(roc_graphs[0].GetXaxis())
        plotting.scale_axes(roc_graphs[0], 1.)
        plotting.scale_axes(ratio_graphs[0], 1./y_frac - 1.5)
        return canvas

    # --- Plotting functions ---
    # These are used by the methods get_roc_plot and get_roc_ratio_plot.

    def _draw_roc_diagram(self):
        """Draws all ROC curves into the current plot.

        This method draws all ROC curves and adds a legend.

        For convenience, this returns all drawn `ROOT.TGraph` objects.
        """
        if not self._roc_curves:
            raise TypeError("No ROC curves to draw")
        # Draw all curves.
        graphs = [curve.graph for curve in self._roc_curves]
        self._draw_graphs(graphs)
        plotting.create_legend([c.graph for c in self._roc_curves],
                               (0.6, 0.7, 0.9, 0.9), "L")
        ROOT.gPad.SetLogy()  # pylint: disable=no-member
        return graphs

    def _draw_ratio_diagram(self):
        """Draws all ratios into the current plot.

        This method draws all curves, and modifies the pad's axes.

        For convenience, this returns all drawn `ROOT.TGraph` objects.
        """
        # Draw the reference curve like every other curve.
        if not self._roc_curves:
            raise TypeError("No ROC curves to draw")
        # Draw all curves.
        graphs = list(self._iter_ratio_curves())
        self._draw_graphs(graphs)
        plotting.create_hline(graphs[0].GetXaxis(), 1)
        return graphs


    def _add_labels(self, font_size):
        """Draw luminosity info and the ATLAS label."""
        line_skip = 1.2 * font_size
        text_x, text_y = 0.2, 0.22
        # Add ATLAS label.
        plotting.create_atlas_label(x_y=(text_x, text_y), offset=(0.11, 0.),
                                    size=font_size)
        # Add information about applied cuts.
        text_y += line_skip
        prongtext = {1: "1-prong", 2: "multi-prong", 3: "3-prong"}[self.nprong]
        if self.ptmin != 0 and self.ptmax != -1:
            cuts_text = "{}, {} < pt < {} GeV".format(
                prongtext, self.ptmin, self.ptmax)
        elif self.ptmin != 0:
            cuts_text = "{}, pt > {} GeV".format(prongtext, self.ptmin)
        elif self.ptmax != -1:
            cuts_text = "{}, pt < {} GeV".format(prongtext, self.ptmax)
        else:
            cuts_text = prongtext
        plotting.create_text(cuts_text, (text_x, text_y), size=font_size)
        # Add luminosity information.
        text_y += line_skip
        if self.luminosity is not None:
            plotting.create_energy_text(x_y=(text_x, text_y), size=font_size)

    # --- Plotting helpers ---
    # These are used by _draw_roc_diagram and _draw_ratio_diagram for
    # simplification.

    def _create_canvas(self, kind):
        """Create a canvas with a cute and memorable name.

        kind : The part of the name that shows what kind of plot this is.
        """
        canvas_name = "{kind}_{methods}_pt{ptmin}{ptmax}_{prong}".format(
            kind=kind,
            methods="_".join(curve.graph.GetTitle()
                             for curve in self._roc_curves),
            prong={1: "1p", 2: "mp", 3: "3p"}[self.nprong],
            ptmin=self.ptmin, ptmax=self.ptmax,
            )
        canvas = plotting.create_canvas_by_client_rect(
            canvas_name, canvas_name, 800, 600)
        ROOT.SetOwnership(canvas, False)  # pylint: disable=no-member
        return canvas

    @staticmethod
    def _create_ratio_pads(canvas, y_frac):
        """Create two pads for a ratio plot diagram.

        canvas : A canvas that will contain the two pads.
        y_frac : Fraction of the canvas height reserved for the
                 ratio pad.
        Returns : Tuple (top_pad, bottom_pad) of the two pads created.
        """
        # Create top pad.
        canvas_name = canvas.GetName()
        top_pad = ROOT.TPad(canvas_name+"_top", "top", 0, y_frac, 1, 1)  # pylint: disable=no-member
        top_pad.SetBottomMargin(0.16)
        # Create bottom pad.
        bottom_pad_extra_height = (0.95-y_frac) * top_pad.GetBottomMargin()
        bottom_pad = ROOT.TPad(canvas_name+"_bottom", "bottom",          # pylint: disable=no-member
                               0, 0, 1, y_frac + bottom_pad_extra_height)
        ROOT.SetOwnership(bottom_pad, False)                             # pylint: disable=no-member
        bottom_pad.SetBottomMargin(0.3)
        return top_pad, bottom_pad

    def _iter_ratio_curves(self):
        """Iterate over `roc.Curve` instances representing the ratio plot.

        This iterator won't yield the reference curve's ratio curve.

        All yielded ratio curves are saved in an internal attribute.
        This attribute is reset if you call this method again.
        Thus, you may *not* use two iterators returned by this method
        at the same time.
        """
        # Copy the reference to the reference curve so that nobody can
        # switch the reference curve during iteration.
        self._ratio_curves = []
        ref_curve = self._reference_roc
        for curve in self._roc_curves:
            if curve is ref_curve:
                continue
            ratio = curve.__div__(ref_curve)
            # Style the graph as necessary.
            ratio.GetXaxis().SetTitle("Signal Efficiency")
            ratio.GetXaxis().SetRangeUser(*self.x_range)
            ratio.GetYaxis().SetTitle("Ratio")
            ratio.GetYaxis().SetRangeUser(*self.ratio_y_range)
            self._ratio_curves.append(ratio)
            yield ratio

    @staticmethod
    def _draw_graphs(graphs):
        """Takes an iterable of `ROOT.TGraph` objects and draws them."""
        # First, draw axis and lines, no error bars.
        draw_style = "LXA"
        for graph in graphs:
            graph.Draw(draw_style)
            # For subsequent graphs, do not clear and do not redraw axis.
            draw_style = "LXsame"

    @staticmethod
    def _hide_axis_labels(axis):
        """Change axis label and title colors to hide them."""
        axis.SetLabelColor(0)
        axis.SetTitleColor(0)


def _get_files_from_folder(folder, nprong):
    """Find EfficiencyEvaluationCycle output in an ANN folder.

    Search for files using the pattern:
    "<folder>/<1_or_3>prong/evaluation/EfficiencyEvaluationCycle.*"
    and return a `common.SigBkgPair` of the results.
    This raises an exception if not exactly two files match this pattern.

    folder : Path to an ANN folder.
    nprong : Either 1 or 2 or 3.
    """
    if not nprong in {1, 2, 3}:
        raise common.NProngError()
    search_folder = os.path.join(
        folder,
        "1prong" if nprong == 1 else "3prong",
        "evaluation",
        )
    pattern = os.path.join(search_folder, "EfficiencyEvaluationCycle.*")
    files = glob.glob(pattern)
    if len(files) != 2:
        raise IOError("Expected 2 files, found {} instead: {}"
                      .format(len(files), search_folder))
    # Determine the ordering of the files.
    if common.PyRootFile.is_signal_file(files[0]):
        return common.SigBkgPair(files[0], files[1])
    else:
        return common.SigBkgPair(files[1], files[0])


def _parse_input(args):
    """Parse a the list of requested ROC curves.

    Returns a list of tuples `(method_name, files)`, where `files` is
    a `common.SigBkgPair` of paths.
    """
    classifiers = []
    if args.by_folder is None:
        # Iterate over all --by-method options.
        for argument in args.by_method:
            method, folders = argument[0], argument[1:]
            # Iterate over all folders passed to the option.
            for folder in folders:
                files = _get_files_from_folder(folder, args.nprong)
                classifiers.append((method, files))
    elif args.by_method is None:
        # Iterate over all --by-folder options.
        for argument in args.by_folder:
            folder, methods = argument[0], argument[1:]
            # Iterate over all methods passed to the option.
            for method in methods:
                files = _get_files_from_folder(folder, args.nprong)
                classifiers.append((method, files))
    else:
        raise ValueError("No methods passed")
    return classifiers


def get_method_title(method, nameslist, index):
    """Return proper title for a given MVA method.

    Returns `method` if `nameslist is None` or `nameslist[index]=="-"`.
    Returns `nameslist[index]` otherwise.
    """
    if nameslist is None:
        return method
    elif nameslist[index] == "-":
        return method
    else:
        return nameslist[index]


def get_parser():
    """Return an argparse.ArgumentParser instance."""
    parser = argparse.ArgumentParser(
        description=__doc__.split("\n")[0],
        epilog="\n".join(__doc__.split("\n")[2:]),
        formatter_class=argparse.RawDescriptionHelpFormatter,
        )
    parser.add_argument("--nprong", "-n", type=int, required=True,
                        help="Prongness of the file. 1, 2, or 3.")
    parser.add_argument("--out", "-o", default=None,
                        help="output file name. By default, a name is "
                        "generated from --names, --ptmin, --ptmax, and "
                        "--nprong.")
    parser.add_argument("--no-ratio", action="store_true", default=False,
                        help="Never produce a ratio plot.")
    parser.add_argument("--lumi", "-l", metavar="LUMINOSITY", type=float,
                        default=20.2814, help="Luminosity in fb^-1 "
                        "(default: 20.2814)")
    parser.add_argument("--ptmin", "-min", type=int, default=0,
                        help="minimum for pt cut")
    parser.add_argument("--ptmax", "-max", type=int, default=-1,
                        help="maximum for pt cut")
    parser.add_argument("--names", "-N", nargs="+", metavar="METHODNAME",
                        help="Names of the methods to appear in the plot "
                        "legend. You must supply as many names as "
                        "methods. Pass '-' to use the method name itself.",
                        default=None)
    inputs = parser.add_mutually_exclusive_group(required=True)
    inputs.add_argument("--by-method", "-M", nargs="+", action="append",
                        metavar=("METHOD", "FOLDERS"),
                        help="Add ROC curves for a method out each "
                        "evaluation folder.")
    inputs.add_argument("--by-folder", "-F", nargs="+", action="append",
                        metavar=("FOLDER", "METHODS"),
                        help="Add ROC curves out of a given folder for "
                        "each method.")
    return parser


def main(argv):
    """Main function. You should pass `sys.argv[1:]` as argument."""
    args = get_parser().parse_args(argv)
    plotting.set_atlas_style()
    pt_range = (args.ptmin, args.ptmax)
    plotter = RocPlotter(args.nprong, pt_range, args.lumi)
    calculator = None
    n_curves = 0
    for method, files in _parse_input(args):
        # Create new calculator if this is the first calculator or we
        # need to open a new file.
        if not calculator or calculator.files.sig.GetName() != files.sig:
            calculator = roc.Calculator(files.sig, files.bkg,
                                        args.nprong, pt_range)
        # Compute ROC curve and pass it to the plotter.
        title = get_method_title(method, args.names, n_curves)
        curve = calculator.get_roc_curve(method)
        plotter.add_roc_curve(title, curve, is_ref=(n_curves == 0))
        n_curves += 1
    # Save plots. Do ratio plot if there's more than one curve and it
    # wasn't explicitly disallowed.
    if n_curves > 1 and not args.no_ratio:
        canvas = plotter.get_roc_ratio_plot()
    else:
        canvas = plotter.get_roc_plot()
    if args.out is None:
        canvas.SaveAs(canvas.GetName()+".eps")
    elif os.path.isdir(args.out):
        canvas.SaveAs(os.path.join(args.out, canvas.GetName()+".eps"))
    else:
        canvas.SaveAs(args.out)


if __name__ == '__main__':
    main(sys.argv[1:])
