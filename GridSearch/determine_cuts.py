
"""Calculate score cuts depending on transverse momentum.

Multivariate analysis (MVA) methods are used for classification of
particles.
They distinguish between charged tau leptons (signal or mc) and
QCD jets (background or data).

This distinction or, preferrably called, "classification" is done via
a score.
Each particle is given a score, rating how tau-like it is.
The goal is to give high scores to tau leptons and low scores to
QCD jets.

In order to transition from continuous scores to discrete classes, we
decide on a certain "score cut". The MVA considers particles with
a score higher than this cut to be signal taus, and particles with
a score lower than this cut to be background (or fake taus).

Since we want to be able to adjust the signal efficiency
(identified taus divided by all true taus), we have to calculate the
correct score cut for each "working point".

Our observation is that particles with a high transverse momentum pT are
more likely to have a high score than those with low pT.
For various reasons, this is undesirable.

Thus, the score cut is a function not only of the signal efficiency,
but also of pT.
And we choose this score cut in such a manner, that, when applying it
for a working point, the curve signal_efficiency(truth_pt) will be
as flat as possible.

This script does the following:
For each method, and for each working point, it computes the function
score_cut(reco_pt).
This function is saved in a ROOT file as a ROOT.TGraph object.

We use reco_pt instead truth_pt, because we want to apply this cut on
background data as well -- and since these are not Monte-Carlo generated,
a truth_pt does not exist for these.

There are the following working points:
  * The LMT cuts: loose, medium, and tight, with pre-defined, agreed-upon,
    universally accepted signal efficiencies.
    (The efficiencies are different for 1- and 3-prong.)
  * The flexible cuts: Basically, we compute the score cuts for each
    signal efficiency from 0.0 to 0.xx with step width=0.01.
"""

from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import os
import sys
import operator
import argparse
import warnings
from array import array
from collections import namedtuple

import ROOT  # pylint: disable=import-error
ROOT.gROOT.SetBatch(True)
ROOT.PyConfig.IgnoreCommandLineOptions = True

# Import sibling package `common` for the class CachedRootFile.
# CachedRootFile is a simple wrapper around ROOT.TFile with the
# following additional features:
#   * Raises exceptions on any kind of IOError.
#   * Keeps results of all calls to `CachedRootFile.Get` in a per-instance
#     dictionary.
# For all purposes, it may be replaced by a normal ROOT.TFile object
# without breaking the code. (though there might be a performance hit.)
from GridSearch import common


# Declaration of namedtuple used by
# `AbstractCutPlotter._calculate_graph_coordinates`.
PtListsTuple = namedtuple("PtLists", ["truth_pt", "reco_pt", "rms_reco_pt"])  # pylint: disable=invalid-name


class ListSorter(object):

    # pylint: disable=too-few-public-methods

    """ListSorter allows sorting a group of lists using another list as keys.

    Passing a list to the constructor saves a sorted copy of that list
    as the `keys` attribute of the created `ListSorter`.
    Passing another list to `ListSorter.sort` copies it and recreates the
    `keys` list's ordering.

    Usage:
    >>> keys = [10, 15, 25, 20, 30]
    >>> values = [1, 2, 3, 4, 5]
    >>> sorter = ListSorter(keys)
    >>> sorter.keys
    [10, 15, 20, 25, 30]
    >>> sorter.sorted(values)
    [1, 2, 4, 3, 5]

    This is equivalent to the following:
    >>> keys = [10, 15, 25, 20, 30]
    >>> values = [1, 2, 3, 4, 5]
    >>> sorted_tuples = sorted(zip(keys, values), key=lambda k,v: k)
    >>> zip(*sorted_tuples)[0]
    [10, 15, 20, 25, 30]
    >>> zip(*sorted_tuples)[1]
    [1, 2, 4, 3, 5]
    """

    def __init__(self, keys, key=None):
        """Construct a ListSorter object.

        keys : List that will be used to sort other lists. A sorted copy
               is saved as `self.keys`.
        key  : key function as for built-in `sorted` and `list.sort`.
               Default: Use inherent ordering of the elements of `keys`.
        """
        # Default for parameter `key`.
        if key is not None:
            key_func = lambda index_key_tuple: key(index_key_tuple[1])
        else:
            key_func = lambda index_key_tuple: index_key_tuple[1]
        # Sort `keys` as specified, but carry the indices.
        sorted_tuples = sorted(enumerate(keys), key=key_func)
        # Separate indices and keys.
        self.indices, self.keys = zip(*sorted_tuples)  # pylint: disable=star-args

    def sorted(self, a_list):
        """Return a rearranged copy of `a_list` using this object's ordering.

        It is assumed that each item of `a_list` corresponds to the item
        with the same index in the `keys` argument to ListSorter's
        constructor.
        This method sorts `a_list` in such a way that the association
        between `a_list` and `self.keys` is kept.
        """
        # Sort `a_list` using the previously created sorting information.
        return [a_list[i] for i in self.indices]


class CutCalculator(object):

    """Class for calculating pT-dependent score cuts.

    The typical usage is to use `iter_score_histograms` to iterate over
    score histograms, normalize them with `get_total_signal` and then
    pass them to `find_score_cuts`.

    >>> iterator = self.calculator.iter_score_histograms(method_name)
    >>> for (low_pt, up_pt), score_hist in iterator:
    ...     norm = self.calculator.get_total_signal(low_pt, up_pt)
    ...     score_hist.Scale(1.0 / norm)
    ...     score_cuts = self.calculator.find_score_cuts(
    ...         score_hist,
    ...         my_target_effs,
    ...         )
    """

    # --- Construction and initialization ---

    def __init__(self, filename, nprong):
        """Construct a CutCalculator object.

        filename : path to an EfficiencyOptimisationCycle.mc.Signal.root file.
        nprong   : prongness of the input files. Either 1 or 3.
        """
        if nprong not in {1, 3}:
            raise common.NProngError(nprong)
        # Set various attributes.
        self.nprong = nprong
        self.pt_bins = (range(15, 25, 10) + range(25, 100) +
                        range(100, 200, 10) + range(200, 1000, 200))
        # Open and read ROOT file.
        self.infile = common.CachedRootFile(filename)

    # --- Utility methods ---

    @staticmethod
    def _pt_to_bins(axis, ptlow, ptup):
        """Convert a range of pTs to the corresponding range of pT bins."""
        return axis.FindBin(ptlow), axis.FindBin(ptup-1e-6)

    @staticmethod
    def _bins_to_pt(axis, lowbin, upbin):
        """Convert a range of pT bins to the corresponding range of pTs."""
        return axis.GetBinLowEdge(lowbin), axis.GetBinUpEdge(upbin)

    # --- Public methods ---

    def get_total_signal(self, low_pt, up_pt):
        """Count total signal particles in a given pT range.

        low_pt, up_pt : range of pT in which to count signal taus.
        """
        # Note: h_truthPt is a 3D histogram with the following axes:
        #   X: Prongness. Bin no. 2 contains 1-prong events, bin no. 4
        #      contains 2- and 3-prong events.
        #   Y: Transverse momentum in GeV.
        #   Z: Mu, the average number of primary vertices per bunch
        #      crossing. We integrate it out, so it doesn't matter much.
        hist_nprong_truthpt_mu = self.infile.Get("h_truthPt")
        pt_axis = hist_nprong_truthpt_mu.GetYaxis()
        low_bin, up_bin = self._pt_to_bins(pt_axis, low_pt, up_pt)
        prong_bin = self.nprong+1
        return hist_nprong_truthpt_mu.Integral(prong_bin, prong_bin,
                                               low_bin, up_bin, 0, -1)

    def get_reco_efficiency(self):
        """Get the maximum efficiency reachable by ID algorithms."""
        # Note: h_TruthpT_OfflinepT is a 2D histogram with the following axes:
        #   X: The event's true visible pT in GeV.
        #   Y: The reconstructed pT in GeV.
        truth_reco_pt_hist = self.infile.Get("h_TruthpT_OfflinepT")
        n_reconstructed = truth_reco_pt_hist.Integral()
        hist_nprong_truthpt_mu = self.infile.Get("h_truthPt")
        prong_bin = self.nprong+1
        n_truth = hist_nprong_truthpt_mu.Integral(prong_bin, prong_bin,
                                                  0, -1, 0, -1)
        return n_reconstructed / n_truth

    def iter_score_histograms(self, method_name):
        """Get slices of a MVA method's pT-score histogram.

        method_name : Name of an MVA method.
                      The histogram "h_pT_{method_name}Score" is read
                      from the input file.
        Returns: An iterator that yields tuples `(pt_range, hist)`.
        pt_range : A tuple `(ptlow, ptup)` of the truth pT range that
                   the score histogram relates to.
                   The transverse momentum pT is given in GeV.
        hist     : The 1D score histogram that is a slice out of the
                   full pT-score histogram.
        """
        # Reverse the bin egdes list so we can pop easily.
        bin_edges = self.pt_bins[::-1]
        # Note: h_pT_BDTScore is a 2D histogram with the following axes:
        #   X: The event's reconstructed pT in GeV.
        #   Y: The BDT's score output for this event.
        pt_score_hist = self.infile.Get("h_pT_{}Score".format(method_name))
        pt_axis = pt_score_hist.GetXaxis()
        # Convert from self.pt_bins's binning to hist_pt_scores's binning.
        low_pt, up_pt = bin_edges.pop(), bin_edges.pop()
        while True:
            low_bin, up_bin = self._pt_to_bins(pt_axis, low_pt, up_pt)
            score_hist = pt_score_hist.ProjectionY("_py", low_bin, up_bin)
            # If the histogram is not empty, yield it and go to the next
            # bin. Otherwise, extend the current bin.
            if score_hist.Integral() != 0:
                # Testing for !=0 is enough. We won't improve our
                # statistical uncertainty much by enforcing more events
                # per score histogram.
                yield (low_pt, up_pt), score_hist
                low_pt = up_pt
            try:
                up_pt = bin_edges.pop()
            except IndexError:
                # Stop the loop once we've reached the end of the pT range.
                break

    def get_reco_pt(self, low_pt, up_pt):
        """Return reco_pt mean and RMS to a given range of truth_pt.

        low_pt, up_pt : range in truth_pt for which mean and RMS of reco_pt
                        are calculated.

        Result: mean and RMS of reco_pt for the given truth_pt range.
        """
        # Note: See method `get_reco_efficiency` for an explanation of
        # h_TruthpT_OfflinepT.
        truth_reco_pt_hist = self.infile.Get("h_TruthpT_OfflinepT")
        # Convert from self.pt_bins's binning to hist_truth_reco_pt's binning.
        truth_axis = truth_reco_pt_hist.GetXaxis()
        low_bin, up_bin = self._pt_to_bins(truth_axis, low_pt, up_pt)
        # Project on reco_pt axis and return result.
        # Substract 1 because ProjectionY includes the final bin.
        reco_hist = truth_reco_pt_hist.ProjectionY("_px", low_bin, up_bin)
        return reco_hist.GetMean(), reco_hist.GetRMS()

    def find_score_cuts(self, hist, target_effs):
        # pylint: disable=no-self-use
        """Find proper score cut for each target efficiency.

        For each target efficiency, a cut on the score is calculated.
        We only accept particles with a score higher than this cut.
        This results in a signal efficiency at least as high as the
        target efficiency.

        hist        : The (normalized) TH1F score histogram.
        target_effs : A dict `{name: eff}` of target efficiencies
                      for which score cuts should be computed.
        Returns: A dict `{name: cut}` with cut values for each passed
                 target efficiency.
        """
        # Transform target_effs into a sorted iterator of tuples.
        target_effs = sorted(target_effs.items(),
                             key=operator.itemgetter(1))
        target_effs = iter(target_effs)
        current_target, current_eff = next(target_effs)
        # Code gets error-prone if the overflow bin contains enough events
        # to satisfy one or more target efficiencies. Be loud about this.
        last_bin = hist.GetNbinsX()+1
        if hist.GetBinContent(last_bin) > current_eff:
            warnings.warn("Overflow bin is too full! Lowest-efficiency "
                          "cuts may all have the same value.")
        # Iterate over all bins.
        # Stop when we've met all target efficiencies.
        try:
            integral = 0
            cuts = {}
            for i_bin in reversed(xrange(last_bin+1)):
                integral += hist.GetBinContent(i_bin)
                # By adding one bin, we might hit more than one targets.
                while integral > current_eff:
                    cuts[current_target] = hist.GetBinCenter(i_bin)
                    current_target, current_eff = next(target_effs)
        except StopIteration:
            return cuts
        else:
            # Hack: If the reco efficiency is lower than the remaining
            # target efficiencies, we just add the lowest-possible cut
            # for the remaining target efficiencies.
            #~ raise ValueError("Not all target efficiencies reached")
            cuts[current_target] = hist.GetBinCenter(0)
            for current_target, current_eff in target_effs:
                cuts[current_target] = hist.GetBinCenter(0)
            return cuts



class AbstractCutPlotter(object):

    # pylint: disable=too-few-public-methods

    """Abstract base class of the cut calculators.

    This class contains most of the logic involved in computing the
    pT-dependant score cuts.
    The concrete subclasses only implement a few working point-related
    issues that we don't want to specify just yet.

    The only public methods are the constructor `__init__` and the method
    `main`.
    Everything else is "just" helper functions.
    """

    # `main` is at the very bottom of this class definition and the functions
    # it relies on are above it. Depending on your preferred way of reading,
    # you might want to read from bottom to top or from top to bottom.

    # --- Construction and initialization ---

    def __init__(self, calculator, nprong, methods):
        """Construct a CutCalculator object.

        calculator : The CutCalculator instance to use.
        nprong     : prongness of the input files. Either 1 or 3.
        methods    : an iterable of TMVA method names.
        """
        if nprong not in {1, 3}:
            raise ValueError("Expected nprong to be 1 or 3, got {} instead"
                             .format(nprong))
        # Set various attributes.
        self.calculator = calculator
        self.methods = list(methods)
        self.nprong = nprong
        self.target_effs = self._init_get_target_efficiencies()

    def _init_get_target_efficiencies(self):
        """Return a dictionary of target efficiencies.

        The CutCalculator will compute the minimum score (the score cut)
        for a particle to pass the MVA method and be considered a
        signal tau lepton.

        Subclasses must implement this method.
        """
        raise NotImplementedError()

    # --- Utility methods ---

    @staticmethod
    def _create_graph(x_list, y_list, **kwargs):
        """Create a ROOT.TGraph object on-the-fly.

        x_list, y_list : sequences of x and y coordinates.

        Possible keyword arguments:
        name    : Passed to TGraph.SetName.
        title   : Passed to TGraph. SetTitle.
        x_axis  : Passed to TGraph.GetXaxis().SetTitle.
        y_axis  : Passed to TGraph.GetYaxis().SetTitle.
        """
        if len(x_list) != len(y_list):
            raise ValueError("Lists are of unequal length: {} vs. {}"
                             .format(len(x_list), len(y_list)))
        # Create the graph.
        x_list, y_list = array("f", x_list), array("f", y_list)
        graph = ROOT.TGraph(len(x_list), x_list, y_list)
        # Set its options.
        graph.Draw("")
        graph.SetName(kwargs.get("name", ""))
        graph.SetTitle(kwargs.get("title", ""))
        graph.GetXaxis().SetTitle(kwargs.get("x_axis", ""))
        graph.GetYaxis().SetTitle(kwargs.get("y_axis", ""))
        return graph

    # --- Calculating the plots ---

    # This method is the work horse of this class.
    # It contains the essential part of the algorithm.
    def _calculate_graph_coordinates(self, method):

        """Calculate lists of X and Y coordinates for the TGraphs.

        Return two collection objects with various lists of
        X and Y coordinates respectively.
        The graphs that are built from these coordinates are showing
        score cuts necessary for one specific efficiency over
        transverse momentum.

        Result: namedtuple `pt_lists`, dict `score_cut_lists`.
        The keys of `pt_lists` are {"truth_pt", "reco_pt", "rms_reco_pt"}.
        `score_cut_lists` has the same keys as `self.target_effs`.
        """

        # Moved out of the for loop below for readability.
        def update_pt_lists(pt_lists, low_pt, up_pt):
            """Append truth, reco, and RMS reco pt to the pt_lists."""
            mean_reco_pt, rms_reco_pt = self.calculator.get_reco_pt(low_pt,
                                                                    up_pt)
            pt_lists.truth_pt.append((low_pt+up_pt)/2.0)
            pt_lists.reco_pt.append(mean_reco_pt)
            pt_lists.rms_reco_pt.append(rms_reco_pt)

        # Moved out of the for loop below for readability.
        def get_score_cuts(low_pt, up_pt, score_hist):
            """Return dict of score cuts for the passed score_hist.

            low_pt and up_pt are needed for normalization.
            """
            norm = self.calculator.get_total_signal(low_pt, up_pt)
            score_hist.Scale(1.0 / norm)
            return self.calculator.find_score_cuts(score_hist,
                                                   self.target_effs)

        # Moved out of the for loop below for readability.
        def update_score_cut_lists(score_cut_lists, score_cuts):
            """Append each score cut to its proper list."""
            for name, cut in score_cuts.items():
                score_cut_lists[name].append(cut)

        # Add arbitrary zeroth item so that the plots created later are
        # more sensibly-looking.
        # Note: Zeroth item of all score_cut_lists will be changed later.
        pt_lists = PtListsTuple([0], [0], [0])
        score_cut_lists = {key: [0] for key in self.target_effs}

        # Main loop, don't underestimate it because of its shortness!
        iterator = self.calculator.iter_score_histograms(method)
        for (low_pt, up_pt), score_hist in iterator:
            update_pt_lists(pt_lists, low_pt, up_pt)
            score_cuts = get_score_cuts(low_pt, up_pt, score_hist)
            update_score_cut_lists(score_cut_lists, score_cuts)

        # Hack: Change zeroth item of all score_cut_lists to the
        # first item. The only reason is so that all pt_lists may have
        # a zeroth item of value 0.
        for key, cuts_list in score_cut_lists.items():
            try:
                cuts_list[0] = cuts_list[1]
            except IndexError:
                # Rephrase the message of IndexError to make debugging easier.
                raise IndexError("cuts list for key {} is too small, "
                                 "expected at least two items.".format(key))
        return pt_lists, score_cut_lists

    def _create_cut_plots(self, method, mean_reco_pts, score_cut_lists):
        """Draw diagrams of all the score_cuts over truth pt.

        method          : Name of the MVA method under examination
        mean_reco_pts   : sequence of pt values for the X axis.
        score_cut_lists : dict of score cut value sequences for the Y axis.

        Returns : dict of TGraphs, each graph corresponding to an item
                  from score_cut_lists.
        """
        title = "{} {} cut for {}-prong"
        # Beware! In the graphs that we create here, we want the data
        # points to be sorted by increasing mean_reco_pt.
        # However, we must *not* assume that `mean_reco_pts` already
        # is sorted.
        # The class `ListSorter` takes care of that.
        sorter = ListSorter(mean_reco_pts)
        return {
            target_eff_name: self._create_graph(
                x_list=sorter.keys,
                y_list=sorter.sorted(score_cuts),
                x_axis="reco pt",
                y_axis="min score for passing",
                name=target_eff_name,
                title=title.format(target_eff_name, method, self.nprong),
                )
            for target_eff_name, score_cuts in score_cut_lists.viewitems()
            }

    def _create_control_plots(self, truth_pts, mean_reco_pts, rms_reco_pts):
        """Draw diagrams of reco pt over truth pt.

        Arguments are sequences of pt values.
        Results are TGraphs truth_vs_mean_reco_pt and truth_vs_rms_reco_pt.
        """
        title = "truth vs {} reco pt for {} prong"
        return [
            self._create_graph(
                x_axis="truth pt", x_list=truth_pts,
                y_axis="{} reco pt".format(name), y_list=pts,
                name="truth_vs_{}Reco_pt".format(name),
                title=title.format(name, self.nprong),
                )
            for name, pts in [("mean", mean_reco_pts), ("rms", rms_reco_pts)]
            ]

    # --- ROOT file output ---

    def _write_plots_to_file(self, filename, plots):
        """Write plots to ROOT file.

        The ROOT file with path `filename` is opened in UPDATE mode.
        Then, a subdirectory "1prong" or "3prong", depending on the
        attribute self.nprong, is created and all plots are dumped there.
        """
        prongdir = "{}prong".format(self.nprong)
        outfile = ROOT.TFile.Open(filename, "UPDATE")
        try:
            # Delete results of previous runs.
            outfile.Delete("{};*".format(prongdir))
            # There have been occurrences of glibc detecting a "corrupted
            # double-linked list" upon program exit.
            # The following line seemed to fix it, though the exact reason
            # hasn't been found yet.
            #~ time.sleep(1)
            outfile.mkdir(prongdir)
            outfile.cd(prongdir)
            for plot in plots:
                plot.Write()
        finally:
            outfile.Close()

    @staticmethod
    def _get_method_filename(method):
        """Return name of a ROOT file for cuts for an MVA method.

        Subclasses must implement this method.
        """
        raise NotImplementedError()

    @staticmethod
    def _get_control_filename():
        """Return name of a ROOT file for control plots.

        Subclasses must implement this method.
        """
        raise NotImplementedError()

    def main(self, outdir):
        """Main method of CutCalculator.

        Calculate score cuts, reco and truth pT, create corresponding
        plots and save them in ROOT files in outdir.
        """
        for method in self.methods:
            pt_lists, cut_lists = self._calculate_graph_coordinates(method)
            truth_pts, reco_pts, rms_reco_pts = pt_lists
            cut_plots = self._create_cut_plots(method, reco_pts, cut_lists)
            control_plots = self._create_control_plots(truth_pts, reco_pts,
                                                       rms_reco_pts)
            self._write_plots_to_file(
                os.path.join(outdir, self._get_method_filename(method)),
                cut_plots.values())
            self._write_plots_to_file(
                os.path.join(outdir, self._get_control_filename()),
                control_plots)


class LMTCutPlotter(AbstractCutPlotter):

    # pylint: disable=too-few-public-methods

    """Concrete CutCalculator for the LMT working points."""

    def _init_get_target_efficiencies(self):
        """Override of abstract superclass method"""
        if self.nprong == 1:
            return {"tight": 0.4, "medium": 0.6, "loose": 0.7}
        else:
            return {"tight": 0.35, "medium": 0.55, "loose": 0.65}

    @staticmethod
    def _get_method_filename(method):
        """Override of abstract superclass method"""
        return "LMT_cuts_{}.root".format(method)

    @staticmethod
    def _get_control_filename():
        """Override of abstract superclass method"""
        return "controlPlots.root"


class FlexibleCutPlotter(AbstractCutPlotter):

    # pylint: disable=too-few-public-methods

    """Concrete CutCalculator for the full-range working points.

    "Full range" actually means the range from 0.0 to 0.77 in 0.01 steps.
    """

    def _init_get_target_efficiencies(self):
        """Override of abstract superclass method"""
        target_effs = {"0": 0.0}
        for frac in range(1, 77):
            target_effs[format(frac, "03")] = frac / 100.
        return target_effs

    @staticmethod
    def _get_method_filename(method):
        """Override of abstract superclass method"""
        return "cuts_{}.root".format(method)

    @staticmethod
    def _get_control_filename():
        """Override of abstract superclass method"""
        return "controlPlots.root"


def get_parser():
    """Return an argparse.ArgumentParser instance."""
    parser = argparse.ArgumentParser(description="script to determine cuts")
    parser.add_argument("file", metavar="FILE", type=str,
                        help="input file (efficiency optimisation output)")
    parser.add_argument("--nprong", "-n", required=True, type=int,
                        help="prongness. Either 1 or 3.")
    parser.add_argument("--outdir", "-o", default=os.curdir,
                        help="output directory")
    return parser


def main(argv):
    """Main function. You should pass `sys.argv[1:]` as argument."""
    args = get_parser().parse_args(argv)
    # Create a default canvas to avoid ROOT printing a dumb warning.
    _ = ROOT.TCanvas("c1")
    calculator = CutCalculator(args.file, args.nprong)
    for ConcretePlotter in LMTCutPlotter, FlexibleCutPlotter:  # pylint: disable=invalid-name
        plotter = ConcretePlotter(calculator, args.nprong,
                                  ["BDT", "MLP", "defaultBDT"])
        plotter.main(args.outdir)


if __name__ == "__main__":
    main(sys.argv[1:])
