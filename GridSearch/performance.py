#! /usr/bin/env python2

"""Compute a single figure of merit for an MVA method and print it.

This script computes a figure of merit informally called "Area Under the
ROC Curve" or AUC for short.

Note that because our ROC curves are differently defined than usual
(inverse eff_bkg vs. eff_sig instead of eff_sig vs. eff_bkg), our
AUC, too, will be different than usually defined.
"""

from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import sys
import argparse

from GridSearch import roc
from GridSearch.common import PyRootFile


def integrate(points, x_range):
    # pylint: disable=invalid-name
    """Integrate a curve over the summary's working range.

    points : An iterable of points `[(sig_eff, bkg_rej)]` where:
        sig_eff : signal efficiency
        bkg_rej : Inverse background efficiency
    x_range : A tuple `(low, high)` denoting the range over which to
              integrate.

    Returns : A float, the integral over the interval
              self.working_range.
    """
    # We avoid using NumPy because it isn't available per default
    # on the Taurus computing cluster and because this would be
    # the first script except committee_weights to need it.
    x_min, x_max = x_range
    integral = 0.0
    # Drop the last point because of how integration works.
    points = sorted((x, y) for (x, y) in points if x_min <= x <= x_max)
    for i, (x1, y1) in enumerate(points[:-1]):
        x2, y2 = points[i+1]
        dx = x2-x1
        h = 0.5 * (y1+y2)
        integral += h*dx
    return integral


def get_parser():
    """Return an argparse.ArgumentParser instance."""
    parser = argparse.ArgumentParser(
        description="Compute Area Under ROC Curve (AUC) for a classifier",
        )
    parser.add_argument("filelist", nargs=2,
                        help="input filelist (efficiency evaluation output)")
    parser.add_argument("--nprong", "-n", type=int, required=True,
                        help="Prongness of the file. 1, 2, or 3.")
    parser.add_argument("--out", "-o", default=None,
                        help="output file (default is stdout)")
    parser.add_argument("--range", "-r", default=None, required=False,
                        nargs=2, type=float,
                        help="Interval over which to integrate. "
                        "Default is [0.35, 0.70].")
    parser.add_argument("--method", "-m", action="append", required=True,
                        help="Print the performance for this MVA method. "
                        "(May be passed more than once.)")
    return parser


def main(argv):

    """Main function. You should pass `sys.argv[1:]` as argument."""

    # Handle arguments and their defaults.
    args = get_parser().parse_args(argv)
    if PyRootFile.is_signal_file(args.filelist[0]):
        sig_name, bkg_name = args.filelist
    else:
        bkg_name, sig_name = args.filelist
    x_range = args.range or (0.35, 0.70)
    if x_range[0] >= x_range[1]:
        raise ValueError("Bad range: {}".format(args.range))

    # The output format depends on if we're interested in more than one
    # classifier or not.
    if len(args.method) > 1:
        output_template = "{m}: {p}"
    else:
        output_template = "{p}"

    # Actual main part. Defer binding of `outfile` for as long as possible.
    outfile = sys.stdout if args.out is None else open(args.out, "w")
    try:
        calculator = roc.Calculator(sig_name, bkg_name, args.nprong)
        for method in args.method:
            curve = calculator.get_roc_curve(method)
            performance = integrate(curve, x_range)
            print(output_template.format(m=method, p=performance),
                  file=outfile)
    finally:
        # We don't want to close stdout.
        if outfile is not sys.stdout:
            outfile.close()


if __name__ == '__main__':
    main(sys.argv[1:])
