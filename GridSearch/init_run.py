#! /usr/bin/env python2

"""Set up the environment for a run of training and evaluating ANNs.

This script creates one or more folders of the name ANN_????? or ENS_?????,
where each question mark may represent a digit.

ANN folders prefixed with "ANN_" are *simple ANN folders*.
They are used for training and evaluating regular neural networks.

ANN folders prefixed with "ENS_" are *ensemble ANN folders*.
They don't contain any ANN of their own, but refer to other,
simple ANN folders.
They use these simple ANN folders as members of an *ensemble* whose output
is the averaged output of all members.

Each of these ANN folders has the following contents:
- Common_libs.xml (soft link)
- Common_packages.xml (soft link)
- JobConfig.dtd (soft link)
- ttnt (soft link, depends on --indir)
- ntuple (soft link, depends on --indir)
- event_weights (soft link, depends on --indir)
- methods.ini (created from --methods)
- variables.ini (created from --variables)
- 1prong, 3prong

The *prong folders* 1prong and 3prong contain each:
- logs, optimisation, cuts, evaluation, plots (empty folders)
- weights (folder, only empty when training simple ANN folders.)
- EfficiencyBase_config.xml (depends on prongness, variables, ensembling)
- RunEfficiencyOptimisationCycle.xml (copied)
- RunEfficiencyEvaluationCycle.xml (copied)

The folder *weights* contains:
- nothing for simple ANN folders (to be filled by TMVA)
- an empty file TMVAClassification_MLP.weights.xml (mock file to make
  GridSearch.do_run think TMVA training has run already);
  and a file TMVAClassification_BDT.weights.xml which is copied from
  the first member's ANN folder (in order to also have a BDT to compare to).
"""

from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import os
import sys
import abc
import shutil
import argparse

from GridSearch import common
from GridSearch.common import tmva_configs


SELF_PATH = os.path.abspath(os.path.dirname(__file__))
"""Path to the GridSearch package."""
RES_PATH = os.path.join(SELF_PATH, "res")


def _tmva_weights_name(mva_name):
    """Return the name of a TMVAClassification_*.weights.xml file."""
    # This method is so that the file name template is in only one place.
    return "TMVAClassification_{}.weights.xml".format(mva_name)


def _tmva_weights_path(ann_folder, nprong, mva_name):
    """Return path of a TMVAClassification_*.weights.xml file.

    ann_folder : Path to an ANN folder where the method is located.
    nprong     : Either 1 or 3.
    mva_name   : Name of the TMVA method in the file.
    """
    if nprong not in {1, 3}:
        raise common.NProngError(nprong)
    return os.path.join(ann_folder, "{}prong".format(nprong), "weights",
                        _tmva_weights_name(mva_name))


# pylint: disable=too-few-public-methods

class Deployable(object):

    """Abstract base class of objects which may be deployed.

    The concept of Deployables is that each object of this class represents
    a file, folder, or similar that may be created in a given target
    path.
    To do so, the object is first created and configured via its
    constructor and varying attributes and methods.
    Finally, the Deployable is created, i.e. *deployed*, by a call to
    the `deploy` method.
    The `deploy` method is only passed the target path, as all other
    information is saved by the object.
    """

    @abc.abstractmethod
    def deploy(self, target):
        """Deploy this object in the path `target`."""


class Folder(Deployable):

    """Create a folder and deploy its contents in it."""

    def __init__(self, name, contents=None):
        """Define a folder that may be created at some location.

        name     : Name of the folder that should be created.
        contents : Iterable of the objects that should be deployed inside
                   the folder.
        """
        self.name = name
        self.contents = list(contents) if contents is not None else []

    def deploy(self, target):
        """Cf. Deployable.deploy for more information."""
        path = os.path.join(target, self.name)
        os.mkdir(path)
        for content in self.contents:
            content.deploy(path)


class EmptyFile(Deployable):

    """Touch an empty file."""

    def __init__(self, name):
        """Define a file that may be created at some location.

        name     : Name of the file that should be created.
        """
        self.name = name

    def deploy(self, target):
        """Cf. Deployable.deploy for more information."""
        path = os.path.join(target, self.name)
        open(path, "a")


class AbstractFile(Deployable):

    # pylint: disable=abstract-method

    """Transfer a file in some way.

    This is an abstract base class for use cases such as copies, symlinks,
    template instantiations.
    """

    def __init__(self, source, destname=None):
        """Define a transfer of a source file to some other location.

        source   : Path to the source file.
        destname : Name of the deployed file. Defaults to the last
                   component of source.
        """
        # Ignore trailing slashes of `source`.
        if source.endswith(os.path.pathsep):
            source = source[:-len(os.path.pathsep)]
        self.source = source
        if destname is not None:
            self.destname = destname
        else:
            _, self.destname = os.path.split(source)


class SymLink(AbstractFile):

    """Create a symlink to another file."""

    def __init__(self, source, destname=None):
        """Define a symlink that may be created at some location.

        Cf. AbstractFile for more information.
        """
        super(SymLink, self).__init__(source, destname)

    def deploy(self, target):
        """Cf. Deployable.deploy for more information."""
        os.symlink(self.source, os.path.join(target, self.destname))


class Copy(AbstractFile):

    """Copy another file."""

    def __init__(self, source, destname=None):
        """Define a copy that may be created at some location.

        Cf. AbstractFile for more information.
        """
        super(Copy, self).__init__(source, destname)

    def deploy(self, target):
        """Cf. Deployable.deploy for more information."""
        shutil.copy(self.source, os.path.join(target, self.destname))


class Template(AbstractFile):

    """Instantiate a file template."""

    def __init__(self, source, destname=None, format_args=None):
        """Define a template that may be instantiated at some location.

        Template instantiation means a call of str.format on the contents
        of the template file.
        `format_args` specifies a dict of further arguments to that call.

        Cf. AbstractFile for more information.
        """
        super(Template, self).__init__(source, destname)
        self.format_args = dict(format_args) if format_args is not None else {}

    def deploy(self, target):
        """Cf. Deployable.deploy for more information."""
        with open(self.source, "r") as infile:
            content = infile.read()
        content = content.format(**self.format_args)
        with open(os.path.join(target, self.destname), "w") as outfile:
            outfile.write(content)


class MethodsFile(Deployable):

    """Write `tmva_configs.TMVAMethod` objects to disk."""

    def __init__(self, methods, destname):
        """Define a TMVA methods INI file that may be written.

        methods  : Iterable of `tmva_configs.TMVAMethod` objects to write.
        destname : Name of the file to create.
        """
        self.methods = list(methods)
        self.destname = destname

    def deploy(self, target):
        """Cf. Deployable.deploy for more information."""
        with open(os.path.join(target, self.destname), "w") as outfile:
            tmva_configs.write_methods(self.methods, outfile)


class VariablesFile(Deployable):

    """Write `common.IDVariables` objects to disk."""

    def __init__(self, id_variables, destname):
        """Define a Variables INI file that may be written.

        id_variables  : `common.IDVariables` object to write.
        destname      : Name of the file to create.
        """
        self.id_variables = id_variables
        self.destname = destname

    def deploy(self, target):
        """Cf. Deployable.deploy for more information."""
        with open(os.path.join(target, self.destname), "w") as outfile:
            self.id_variables.write(outfile)


class AbstractProngFolder(Folder):

    """Base class for simple and ensemble prong folders.

    It ignores the following objects, which are deployed in the subclasses:
    - the folder "weights"
    - the file "EfficiencyBase_config.xml"
    """

    def __init__(self, nprong, id_variables):
        """Define a prong folder to be deployed later.

        nprong       : Either 1 or 3.
        id_variables : A `common.IDVariables` object to describe the
                       variables used during training.
        """
        if nprong not in {1, 3}:
            raise common.NProngError(nprong)
        self.name = "{}prong".format(nprong)
        self.nprong = nprong
        self.id_variables = id_variables
        # Define contents.
        super(AbstractProngFolder, self).__init__(self.name, contents=[
            Folder("logs"),
            Folder("optimisation"),
            Folder("cuts"),
            Folder("evaluation"),
            Folder("plots"),
            Copy(os.path.join(RES_PATH, "RunEfficiencyOptimisationCycle.xml")),
            Copy(os.path.join(RES_PATH, "RunEfficiencyEvaluationCycle.xml")),
            ])

    def _get_format_args(self, use_ensembles, weights_files):
        """Return dict for EfficiencyBase_config.template.xml.

        With the dict returned by this function, the EfficiencyBase_config
        template may be instantiated.
        To be used only by subclasses.

        use_ensembles : Either True or False
        weights_files : A list of absolute paths which may not contain
                        spaces.
        """
        return {
            "nprong": 2 if self.nprong == 3 else 1,
            "use_mlp_ensembles": "true" if use_ensembles else "false",
            "member_weight_files": " ".join(weights_files),
            "id_variables_1p": " ".join(self.id_variables.for_ntuple.sp),
            "id_variables_3p": " ".join(self.id_variables.for_ntuple.mp),
            }


class SimpleProngFolder(AbstractProngFolder):

    """Create a simple prong folder as defined in the module docstring."""

    def __init__(self, nprong, id_variables, methods):
        """Define a prong folder to be deployed later.

        Cf. `AbstractProngFolder` for more information.

        methods : Cf. `MethodsFile` for more information.
        """
        super(SimpleProngFolder, self).__init__(nprong, id_variables)
        weights_file = _tmva_weights_path(os.pardir, self.nprong, "MLP")
        # Define contents.
        self.contents.extend([
            Folder("weights"),
            Template(
                os.path.join(RES_PATH, "EfficiencyBase_config.template.xml"),
                destname="EfficiencyBase_config.xml",
                format_args=self._get_format_args(False, [weights_file]),
                ),
            MethodsFile(methods, "methods.ini"),
            ])


class EnsembleProngFolder(AbstractProngFolder):

    """Create an ensemble prong folder as defined in the module docstring."""

    def __init__(self, nprong, id_variables, members):
        """Define a prong folder to be deployed later.

        Cf. `AbstractProngFolder` for more information.

        members : An iterable of ANN folder paths containing members of
                  the ensemble to be created.
        """
        super(EnsembleProngFolder, self).__init__(nprong, id_variables)
        self.use_ensembles = True
        # Make sure all member paths are absolute and complain
        # if any doesn't exist.
        weights_files = []
        for member in members:
            member = os.path.normpath(os.path.abspath(member))
            assert os.path.isdir(member), member
            member = _tmva_weights_path(member, self.nprong, "MLP")
            weights_files.append(member)
        # Copy the first member's trained BDT to have one with the
        # correct properties and variables for ourselves.
        bdt_source = _tmva_weights_path(os.path.abspath(members[0]),
                                        self.nprong, "BDT")
        # Define contents.
        self.contents.extend([
            Folder("weights", contents=[
                EmptyFile(_tmva_weights_name("MLP")),
                Copy(bdt_source, _tmva_weights_name("BDT")),
                ]),
            Template(
                os.path.join(RES_PATH, "EfficiencyBase_config.template.xml"),
                destname="EfficiencyBase_config.xml",
                format_args=self._get_format_args(True, weights_files),
                ),
            ])


class AbstractANNFolder(Folder):

    """Base class for simple and ensemble ANN folders.

    This class implements what is common to both types of ANN folders.
    The specializations then add what's necessary.
    """

    @abc.abstractmethod
    def __init__(self, name, indir, id_variables):
        """Define an ANN folder to be deployed later.

        indir        : Path to a folder which contains subfolders
                       "ttnt", "ntuple", and "event_weights" with the
                       appropriate ROOT files inside.
        id_variables : A `common.IDVariables` object to describe the
                       variables used during training.
        """
        indir = os.path.normpath(os.path.abspath(indir))
        super(AbstractANNFolder, self).__init__(name, contents=[
            SymLink(os.path.join(RES_PATH, "Common_libs.xml")),
            SymLink(os.path.join(RES_PATH, "Common_packages.xml")),
            SymLink(os.path.join(RES_PATH, "JobConfig.dtd")),
            SymLink(os.path.join(indir, "ttnt")),
            SymLink(os.path.join(indir, "ntuple")),
            SymLink(os.path.join(indir, "event_weights")),
            VariablesFile(id_variables, "variables.ini"),
            ])


class SimpleANNFolder(AbstractANNFolder):

    """Create a simple ANN folder as defined in the module docstring."""

    # pylint: disable=too-many-arguments
    def __init__(self, name, indir, id_variables, bdt_method, mlp_methods):
        """Define an ANN folder to be deployed later.

        bdt_method  : A `TMVAMethod` instance for the BDT to be trained.
        mlp_methods : A `ProngPair` of `TMVAMethod` instance for the
                      MLPs to be trained.

        Cf. `AbstractANNFolder` for more information.
        """
        super(SimpleANNFolder, self).__init__(name, indir, id_variables)
        # Append what's necessary.
        self.contents.extend([
            SimpleProngFolder(1, id_variables, [bdt_method, mlp_methods.sp]),
            SimpleProngFolder(3, id_variables, [bdt_method, mlp_methods.mp]),
            ])


class EnsembleANNFolder(AbstractANNFolder):

    """Create a ensemble ANN folder as defined in the module docstring."""

    def __init__(self, name, indir, id_variables, members):
        """Define an ANN folder to be deployed later.

        methods : Cf. `ProngFolder` for more information.

        Cf. `AbstractANNFolder` for more information.
        """
        super(EnsembleANNFolder, self).__init__(name, indir, id_variables)
        # Append what's necessary.
        self.contents.extend([
            EnsembleProngFolder(1, id_variables, members),
            EnsembleProngFolder(3, id_variables, members),
            ])


def get_ann_folder_names(prefix, count):
    """Return list of `count` unoccupied folder names."""
    results = []
    ann_id = 0
    while count > 0:
        name = prefix + format(ann_id, "05d")
        if not os.path.exists(name):
            results.append(name)
            count -= 1
        ann_id += 1
    return results


def get_ann_folder_name(prefix):
    """Return an unoccupied folder name."""
    return get_ann_folder_names(prefix, count=1)[0]


def parse_methods_file(methods_file):
    """Read and validate BDT and an MLP from the file passed via --methods.

    Returns a tuple `(bdt_method, mlp_methods)`.
        bdt_method  : A `TMVAMethod` tuple.
        mlp_methods : A `VariantMethod` instance.
    """
    # Read INI file.
    methods = tmva_configs.read_methods(methods_file)
    bdt_method, mlp_methods = sorted(methods)
    if bdt_method.name != "BDT" or mlp_methods.name != "MLP":
        raise ValueError("TMVA method names do not match. Expected "
                         "BDT, MLP, got instead {0.name}, {1.name}"
                         .format(bdt_method, mlp_methods))
    # Turn `mlp_methods` into a `VariantMethod`.
    mlp_methods = common.VariantMethod(
        mlp_methods.type, mlp_methods.name,
        tmva_configs.parse_python(mlp_methods.options),
        )
    return bdt_method, mlp_methods


def unpack_variant_method(variant):
    """Turn a `VariantMethod` instance into a list of `ProngPair` tuples."""
    # Get a pair of lists, one for each prongness.
    combinations = common.ProngPair(
        list(variant.choose(1).combinations()),
        list(variant.choose(3).combinations()),
        )
    # Make sure both lists are of equal length.
    assert len(combinations.sp) == len(combinations.mp), \
        "{}=={}".format(len(combinations.sp), len(combinations.mp))
    # Reorganize the pair of lists into a list of pairs.
    pairs = [common.ProngPair(sp, mp) for sp, mp in zip(*combinations)]  # pylint: disable=star-args
    return pairs


def get_parser():
    """Return an argparse.ArgumentParser instance."""
    parser = argparse.ArgumentParser(
        description="Set up folders for ANN grid search")
    parser.add_argument("--indir", "-i", type=str, required=True,
                        help="Folder containing ttnt, ntuple, and "
                        "event_weights subfolders")
    parser.add_argument("--variables", "-V", metavar="INIFILE", type=str,
                        help='INI file containing a section "Variables"'
                        'with keys "1-prong" and "multi-prong".'
                        "Each keys value must be a space-separated list "
                        "of variable names which will be used for ID by "
                        "the produced classifier.")
    mode = parser.add_mutually_exclusive_group()
    mode.add_argument("--methods", "-M", metavar="INIFILE", type=str,
                      help="INI file containing a BDT and an MLP. The "
                      "MLP may contain lists in place of option values. "
                      "These lists will be searched exhaustively.")
    mode.add_argument("--ensemble", "-E", metavar="ANNFOLDER", type=str,
                      nargs="+", help="Create folder for a ANN committee. "
                      "The folder names passed are assumed to be folders "
                      "of the ANNs that are part of this committee.")
    return parser


def main(argv):
    """Main function. You should pass `sys.argv[1:]` as argument."""
    args = get_parser().parse_args(argv)
    id_variables = common.IDVariables(args.variables)
    if args.ensemble:
        # Create one ensemble.
        folder_name = get_ann_folder_name("ENS_")
        folders = [EnsembleANNFolder(folder_name, args.indir, id_variables,
                                     members=args.ensemble)]
    else:
        # Create several ANNs.
        bdt_method, mlp_methods = parse_methods_file(args.methods)
        mlp_methods = unpack_variant_method(mlp_methods)
        folder_names = get_ann_folder_names("ANN_", count=len(mlp_methods))
        folders = [
            SimpleANNFolder(folder_name, args.indir, id_variables,
                            bdt_method, mlp_method_pair)
            for folder_name, mlp_method_pair in zip(folder_names, mlp_methods)
            ]
    for folder in folders:
        folder.deploy(os.path.curdir)


if __name__ == "__main__":
    main(sys.argv[1:])
