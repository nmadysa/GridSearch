#! /usr/bin/env python2

"""Module for training via TMVA."""

from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import sys
import argparse

import ROOT  # pylint: disable=import-error
ROOT.gROOT.SetBatch(True)
ROOT.PyConfig.IgnoreCommandLineOptions = True

from GridSearch import common

class Cuts(list):

    """List-like object which may be converted to a ROOT.TCut."""

    def to_root_cut(self):
        """Convert this cuts list to a ROOT.TCut object.

        This places the boolean and (&&) between all elements of this list.
        """
        subcuts = map(ROOT.TCut, self)
        return sum(subcuts, ROOT.TCut())

    def __repr__(self):
        inner = super(Cuts, self).__repr__()
        return "{0.__name__}({1})".format(type(self), inner)


def _add_tree(factory, rootfile, treename):
    """Add ROOT file's tree `treename` to `factory`."""
    tree = rootfile.Get(treename)
    if rootfile.is_signal_file():
        factory.AddSignalTree(tree)
    else:
        factory.AddBackgroundTree(tree)


def _get_weights_functions(weights_dir, nprong):
    """Return tuple `(get_sig_mu_weights, get_bkg_pt_weights)`.

    weights_dir : Folder containing scripts weights_{1,3}prong.py.
    nprong      : Either 1 or 3. Determines which script to import.
    """
    # Append weights_dir so import finds the scripts.
    sys.path.append(weights_dir)
    try:
        # pylint: disable=import-error
        if nprong == 1:
            import weights_1prong as weights
            return common.SigBkgPair(weights.GetWeights_SignalMu_1p,
                                     weights.GetWeights_BackgroundPt_1p)
        elif nprong == 3:
            import weights_3prong as weights
            return common.SigBkgPair(weights.GetWeights_SignalMu_3p,
                                     weights.GetWeights_BackgroundPt_3p)
        else:
            raise common.NProngError(nprong)
    finally:
        # Remove weights_dir under all circumstances.
        sys.path.remove(weights_dir)


def _serialize_method_options(options):
    """Turn `dict(key1="val", key2="more")` into string "key1=val:key2=more"."""
    return ":".join("{!s}={!s}".format(option, value)
                    for option, value in options.items())


class TMVAAnalysis(object):

    """Class representing a run of TMVA.

    An object of this class trains, tests, and evaluates a BDT and an
    MLP classifier for a given set of input events and tau prongness.
    The results are saved to a given file.
    Also, TMVA automatically created a folder called "weights" which
    contains weights files for all trained classifiers so they may
    be re-used later.

    Usage:
    >>> analysis = TMVAAnalysis(infiles, outname, nprong,
    ...                         "./weights/current_result/")
    >>> analysis.book_config_file_methods("mva_methods.ini")
    >>> analysis.run()
    """

    factory_name = "TMVAClassification"
    """Class-wide default factory name."""

    tree_name = "tau"
    """Class-wide default tree name."""

    def __init__(self, nprong):
        """Prepare TMVA analysis. `nprong` is either 1 or 3.

        Afterwards, the attributes of the object may be changed to suit
        the needs of the analysis.
        """
        if nprong not in {1, 3}:
            raise common.NProngError(nprong)
        self.factory_options = []
        self.tree_options = []
        self.id_variables = common.IDVariables()
        self.methods = []
        self.nprong = nprong
        self.cuts = Cuts(["tau_numTrack=={:d}".format(nprong)])
        self.weights_dir = ""

    def book_config_file_methods(self, filepath):
        """Read and parse config file, add TMVA methods accordingly.

        This uses GridSearch.common.tmva_configs to read TMVA methods
        from INI files. Read the docstring on that module for more
        information.
        """
        self.methods.extend(common.tmva_configs.read_methods(filepath))

    def run(self, innames, outname):

        """Train, test, and evaluate all booked methods.

        innames : Iterable of ROOT file names for event input.
        outname : ROOT file name for TMVA output.
        """

        assert self.methods
        assert self.weights_dir

        # Open input files.
        infiles = [common.PyRootFile(name) for name in innames]

        # Initialize Factory.
        ROOT.TMVA.Tools.Instance()
        outfile = ROOT.TFile.Open(outname, "RECREATE")
        factory = ROOT.TMVA.Factory(self.factory_name, outfile,
                                    ":".join(self.factory_options))

        # Add variables.
        variables = {1: self.id_variables.for_ttnt.sp,
                     3: self.id_variables.for_ttnt.mp}[self.nprong]
        for var_name, var_type in variables:
            factory.AddVariable(var_name, var_type)

        # Add and prepare trees.
        for infile in infiles:
            _add_tree(factory, infile, self.tree_name)
        get_weights = _get_weights_functions(self.weights_dir, self.nprong)
        factory.SetSignalWeightExpression(get_weights.sig())
        factory.SetBackgroundWeightExpression(get_weights.bkg())
        factory.PrepareTrainingAndTestTree(self.cuts.to_root_cut(),
                                           ":".join(self.tree_options))

        # Book TMVA methods.
        for method in self.methods:
            factory.BookMethod(method.type, method.name,
                               _serialize_method_options(method.options))

        # Run TMVA.
        factory.TrainAllMethods()
        factory.TestAllMethods()
        factory.EvaluateAllMethods()
        outfile.Close()


def get_parser():
    """Return an argparse.ArgumentParser instance."""
    parser = argparse.ArgumentParser(description="TMVA training script")
    parser.add_argument("infiles", metavar="FILE", nargs="+",
                        help="input file (TTNT output)")
    parser.add_argument("--methods", "-m", metavar="INIFILE", required=True,
                        help="INI file with the MVA methods to be trained")
    parser.add_argument("--nprong", "-n", metavar="N", required=True,
                        type=int, help="prongness (1 or 3)")
    parser.add_argument("--weights-dir", "-w", metavar="DIR", required=True,
                        help="Directory of pT/nVtx weights scripts.")
    parser.add_argument("--variables", "-v", metavar="INIFILE",
                        help="INI file containing space-sparated variable "
                        'lists on keys "1-prong" and "multi-prong" in a'
                        'section "TrainingVariables".')
    return parser

def main(argv):
    """Main function."""
    # Parse command-line arguments.
    args = get_parser().parse_args(argv)
    # Main part of this script.
    analysis = TMVAAnalysis(args.nprong)
    analysis.book_config_file_methods(args.methods)
    analysis.weights_dir = args.weights_dir
    analysis.id_variables = common.IDVariables(args.variables)
    analysis.factory_options = ["!V", "!Silent", "!Color", "!DrawProgressBar"]
    analysis.tree_options = [
        "SplitMode=Alternate",
        "MixMode=Random",
        "NormMode=EqualNumEvents",
        "nTrain_Signal=0",
        "nTrain_Background=0",
        "nTest_Signal=0",
        "nTest_Background=0",
        ]
    analysis.run(args.infiles, "TMVA.root")


if __name__ == '__main__':
    main(sys.argv[1:])


