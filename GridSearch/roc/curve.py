#! /usr/bin/env python2

"""Module that contains the class `roc.Curve`."""

from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

from collections import namedtuple

import ROOT
ROOT.gROOT.SetBatch(True)  # pylint: disable=no-member
ROOT.PyConfig.IgnoreCommandLineOptions = True

Point = namedtuple("Point", ["x", "y"])

class Curve(object):

    # pylint: disable=too-few-public-methods

    """Class to hide implementation details of ROC curves."""

    max_value = 1.e6
    """Maximum value of background rejection to avoid infinities."""

    def __init__(self, sig_eff, bkg_eff):
        """Create an instance.

        sig_eff : A list of increasing signal efficiencies.
        bkg_eff : A list of decreasing background efficiencies.
        """
        assert len(sig_eff) == len(bkg_eff), \
            "Unequal length: {} vs. {}".format(len(sig_eff), len(bkg_eff))
        # Get background rejection.
        bkg_rej = [1/eff if eff else self.max_value for eff in bkg_eff]
        # Create the graph.
        self.graph = ROOT.TGraph(len(sig_eff)+1)  # pylint: disable=no-member
        ROOT.SetOwnership(self.graph, False)      # pylint: disable=no-member
        # Hack to force the correct axis length. If the ROC curve does
        # not start at sig_eff=0, ROOT refuses to have the X axis go
        # down to 0. Thus, we force an arbitrary point (0.0, max) to be
        # included in our graph.
        self.graph.SetPoint(0, 0.0, self.max_value)
        # Fill the graph.
        for i, (sig, bkg) in enumerate(zip(sig_eff, bkg_rej), 1):
            self.graph.SetPoint(i, sig, bkg)

    @classmethod
    def from_histograms(cls, hists):
        """Create an instance.

        hists : A `common.SigBkgPair` of histograms from which to
                extract signal and background efficiency.
        """
        sig_eff = [hists.sig.GetBinContent(i)
                   for i in range(0, hists.sig.GetNbinsX() + 1, 1)]
        bkg_eff = [hists.bkg.GetBinContent(i)
                   for i in range(0, hists.bkg.GetNbinsX() + 1, 1)]
        return cls(sig_eff, bkg_eff)

    def __len__(self):
        return self.graph.GetN()

    def iterx(self):
        """Iterate over all X coordinates of the graph."""
        x_axis = self.graph.GetX()
        for i in xrange(len(self)):
            yield x_axis[i]

    def itery(self):
        """Iterate over all Y coordinates of the graph."""
        y_axis = self.graph.GetX()
        for i in xrange(len(self)):
            yield y_axis[i]

    def iterpoints(self):
        """Iterate over all points of the graph."""
        x_axis, y_axis = self.graph.GetX(), self.graph.GetY()
        for i in xrange(len(self)):
            yield Point(x_axis[i], y_axis[i])

    __iter__ = iterpoints

    def __getitem__(self, key):
        if key < 0:
            key += len(self)
        return Point(self.graph.GetX()[key], self.graph.GetY()[key])

    def __div__(self, other):
        """Compute the ratio of two curves and return a `ROOT.TGraph`."""
        assert len(self) == len(other), \
            "Unequal length: {} vs. {}".format(len(self), len(other))
        # Copy the style of our own `TGraph`.
        ratio = ROOT.TGraph(self.graph)
        # We assume that the denominator's points are well spread over
        # the X axis while the numerator may behave more badly.
        for i, numerator in enumerate(self):
            denominator = _find_closest(numerator.x, other)
            ratio.SetPoint(i, (numerator.x + denominator.x) / 2,
                           numerator.y / denominator.y)
        return ratio


def _find_closest(target_x, curve):
    """Find the point of a `Curve` which is closest to `target_x`.

    This assumes that the points of `curve` are sorted by increasing x.
    """
    best_point = None
    best_dx = float("inf")
    for point in curve:
        current_dx = abs(target_x - point.x)
        if current_dx > best_dx:
            break
        best_point, best_dx = point, current_dx
    return best_point
