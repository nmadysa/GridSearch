#! /usr/bin/env python2

"""Module that contains the class `roc.Calculator`."""

from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import ROOT
ROOT.gROOT.SetBatch(True)  # pylint: disable=no-member
ROOT.PyConfig.IgnoreCommandLineOptions = True

from .curve import Curve
from GridSearch.common import PyRootFile, SigBkgPair, NProngError


class Calculator(object):

    """Object to calculate ROC curves.

    A ROC curve is a graph of background rejection over signal efficiency.
    Objects of this class take two ROOT files and read score-pt histograms
    from them to compute the ROC curve.

    Usage:
    >>> calculator = RocCalculator("sig.root", "bkg.root", 2)
    >>> roc_curve = calculator.get_roc_curve("myMVAMethod")
    >>> matplotlib.pyplot.plot(roc_curve.sig, roc_curve.bkg)
    """

    def __init__(self, sig_file, bkg_file, nprong, pt_min_max=(0, -1)):
        """Create a new instance.

        sig_file,
        bkg_file   : Paths to ROOT files.
        nprong     : prongness of the input files. Either 1, 2, or 3.
        pt_min_max : pT range in which to select particles.
        """
        if nprong not in {1, 2, 3}:
            raise NProngError(nprong)
        self.nprong = nprong
        self.ptmin, self.ptmax = pt_min_max
        # Open files and read total counts.
        self.files = SigBkgPair(PyRootFile(sig_file), PyRootFile(bkg_file))
        self.total_counts = self.get_total_count()

    def get_roc_curve(self, method):
        """Compute ROC curve of an MVA method.

        Load the histograms "h_passed_{method}_pt" of both signal and
        background file and use them to compute the ROC curve.

        method : Name of the method to be used.

        Returns : A `roc.Curve` instance.
        """
        # Create efficiency histograms.
        hists = SigBkgPair(self.get_histogram(method, "sig"),
                           self.get_histogram(method, "bkg"))
        hists.sig.Scale(1. / self.total_counts.sig)
        hists.bkg.Scale(1. / self.total_counts.bkg)
        return Curve.from_histograms(hists)

    def get_histogram(self, method, kind):
        """Return an histogram of passing taus over target efficieny.

        This loads the histograms "h_passed_{method}_pt" of either the
        signal or the background file and integrates over pT.

        method : Name of the method to be used.
        kind   : Either "sig" or "bkg".

        Returns : A `ROOT.TH1F` of passing taus over the working point.
        """
        # Load the 2D efficiency/pT histogram from the ROOT file.
        rootfile = getattr(self.files, kind)
        eff_pt_hist = rootfile.Get("h_passed_{}_pt".format(method))
        # Integrate over pT, leaving an efficiency histogram.
        return eff_pt_hist.ProjectionX("eff_{}_{}".format(method, kind),
                                       self.ptmin+1, self.ptmax)

    def get_total_count(self):
        """Return total number of events for signal and background.

        Load the histograms "h_truthPt" and "h_total_pt" and use them
        to count the total number of signal and background events.
        Only events that match this object's prong and pT constraints
        are counted.
        """
        # Add 1 because bin indices begin at 1.
        prong_bin = (1 if self.nprong == 1 else 3)+1
        # h_truthPt is a 3D-histogram with axes prong/truth_pt/mu.
        h_truth_pt = self.files.sig.Get("h_truthPt")
        h_sig = h_truth_pt.ProjectionY("", prong_bin, prong_bin, 0, -1)
        h_bkg = self.files.bkg.Get("h_total_pt")
        # Integrate over pT in signal and background histogram.
        return SigBkgPair(h_sig.Integral(self.ptmin+1, self.ptmax),
                          h_bkg.Integral(self.ptmin+1, self.ptmax))

