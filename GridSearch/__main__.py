#! /usr/bin/env python2

"""
Usage: python -m GridSearch <submodule> [<args>...]
Execute one of the scripts contained in this package.
"""

from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import sys


SCRIPTS = [
    "training",
    "plot_roc",
    "plot_scores",
    "plot_sig_eff",
    "performance",
    "determine_cuts",
    "init_run",
    "do_run",
    "tools.plot_effvscut",
    "tools.filter_ann",
    "tools.rename_branch",
    "tools.weights.calculate",
    "tools.weights.check",
    "tools.weights.root_to_py",
    "tools.committee.weights",
    "tools.performance.average",
    "tools.tmva.patchwork",
    "tools.tmva.plot_errfunc",
    "tools.tmva.plot_vars",
    "tools.tmva.plot_corr",
    ]


def print_help_exit(error=None, no_help=False):
    """Print usage information and exit.

    If `error` is specified, it is printed to stderr and the exit code is
    non-zero. The default exit code is zero.
    If `no_help` is specified and True, do not print a help message.
    """
    if not no_help:
        print(__doc__)
        print("The following scripts are available:")
        for name in sorted(SCRIPTS):
            print("\t", name, sep="")
    if error is not None:
        print(__package__+":", "error:", error, file=sys.stderr)
        sys.exit(2)
    else:
        sys.exit(0)


def main(argv):
    """Main function."""
    if not len(argv):
        print_help_exit(error="no submodule specified")
    command = argv[0]
    if command in ("-h", "--help"):
        print_help_exit()
    if command not in SCRIPTS:
        print_help_exit(error="no submodule "+repr(command),
                        no_help=True)
    qualname = ".".join([__package__, command])
    # Tweak sys.argv so that the subcommands' ArgumentParser knows
    # what their name is.
    sys.argv[0] = qualname
    # Import submodule and run its main method.
    module = __import__(qualname, fromlist=[command])
    module.main(argv[1:])


if __name__ == "__main__":
    main(sys.argv[1:])
