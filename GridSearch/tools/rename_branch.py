#! /usr/bin/env python2

"""Rename a branch in one or several ROOT files.

Renaming a branch always involves copying that tree, so the operation
should be expected to scale with the size of the tree.
"""

from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import os
import sys
import argparse

from GridSearch import common

def rename_branch(tree, old_name, new_name):
    """Rename branch `old_name` to `new_name` on the given `tree`."""
    copy = tree.CopyTree("")
    tree.Delete("all")
    for branch in copy.GetListOfBranches():
        if branch.GetName() == old_name:
            branch.SetName(new_name)
            break
    copy.Write()


def get_parser():
    """Return an argparse.ArgumentParser instance."""
    parser = argparse.ArgumentParser(
        description="Rename a branch of a TTree saved in a ROOT file.")
    parser.add_argument("--tree", "-t", type=str, default="tau",
                        help="Name of the TTree to which the branch "
                        "belongs. (default: \"tau\")")
    parser.add_argument("old_name", metavar="<old>", type=str,
                        help="Name of the branch to be renamed.")
    parser.add_argument("new_name", metavar="<new>", type=str,
                        help="New name of the branch.")
    parser.add_argument("files", metavar="<file>", nargs="+", type=str,
                        help="ROOT files which contain the branch.")
    return parser


def main(argv):
    """Main function. You should pass `sys.argv[1:]` as argument."""
    args = get_parser().parse_args(argv)
    for filename in args.files:
        rootfile = common.PyRootFile(filename, "UPDATE")
        tree = rootfile.Get(args.tree)
        rename_branch(tree, args.old_name, args.new_name)


if __name__ == "__main__":
    main(sys.argv[1:])
