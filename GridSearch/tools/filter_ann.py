#! /usr/bin/env python2

"""
"""

from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import os
import sys
import shutil
import argparse


def match_all_patterns(lines, patterns):
    """Return True if all patterns appear in the passed lines.

    Check if all patterns appear somewhere in the iterable of strings.
    Membership is simply tested using the `in` operator.

    lines    : An iterable of strings. That may be a list, a file, ...
    patterns : A finite iterable of substrings.
    """
    must_be_found = list(patterns)
    if not must_be_found:
        return True
    for line in lines:
        for pattern in list(must_be_found):
            if pattern in line:
                must_be_found.remove(pattern)
        if not must_be_found:
            break
    return not must_be_found


def retrieve_ann_plots(folder, name_trunk, outdir=os.curdir):
    """Copy EPS files from the ?prong/plots subdirectories.

    Copies the files "<folder>/{1,3}prong/plots/<name_trunk>_?p.eps"
    to outdir. There, they are given the name "<folder>_?p.eps".

    folder     : ANN parent folder from which to copy.
    name_trunk : Name without prong spec and file suffix.
    outdir     : Folder where to copy the files.
    """
    prongs = [(1, "1p"), (3, "mp")]
    for numprong, letterprong in prongs:
        inpath = os.path.join(folder, "{}prong".format(numprong), "plots",
                              "{}_{}.eps".format(name_trunk, letterprong))
        outpath = os.path.join(outdir, "{}_{}.eps".format(folder,
                                                          letterprong))
        shutil.copy2(inpath, outpath)


def list_matching_folders(folders, patterns, common_filename):
    """Return folders in whose common file contains all patterns.

    folders         : List of folder names whose common file is checked
    patterns        : List of substrings that must be contained in a
                      file to match.
    common_filename : Name of the file in each folder that is checked
                      using `match_all_patterns`.

    Result : sublist of `folders`.
    """
    result = []
    for folder in folders:
        with open(os.path.join(folder, common_filename), "r") as infile:
            if match_all_patterns(infile, patterns):
                result.append(folder)
    return result


def split_largs(largs, pivot="--"):
    """Split command-line arguments into two lists.

    largs must either contain a pivot element or consist of
    exactly two elements.
    In the former case, the lists contain all elements before and after
    the pivot resp.
    In the latter case, the lists contain the first and the second
    element resp.

    If largs cannot be split unambiguously, ValueError is raised.
    """
    try:
        index = largs.find(pivot)
        end_left, start_right = index, index+1
    except ValueError:
        if len(largs) == 2:
            end_left, start_right = 1, 1
        else:
            raise
    return largs[:end_left], largs[start_right:]


def get_parser():
    """Return an argparse.ArgumentParser instance."""
    parser = argparse.ArgumentParser(description="Event weight calculator")
    parser.add_argument("folders", metavar="FOLDER", nargs="+",
                        help="Folders whose common file should be checked.")
    parser.add_argument("--pattern", "-p", metavar="PATTERN",
                        action="append", required=True,
                        help="A string that must be contained in the "
                        "folders' common file for the folder to match. "
                        "You may pass more than one pattern.")
    parser.add_argument("--retrieve", "-R", metavar="TRUNK", default=None,
                        help="Retrieve files named <TRUNK>_<prong>.eps "
                        "from the matching folders' <prong>/plot "
                        "subfolders.")
    parser.add_argument("--outdir", "-o", metavar="DIR", default=os.curdir,
                        help="Output directory, used in conjunction "
                        "with --retrieve.")
    parser.add_argument("--common", "-c", metavar="FILE",
                        default="tmva_methods.ini",
                        help="Common file of all folders that are "
                        "checked.")
    return parser


def main(argv):
    """Main function. You should pass `sys.argv[1:]` as argument."""
    args = get_parser().parse_args(argv)
    matching_folders = list_matching_folders(args.folders, args.pattern,
                                             args.common)
    for folder in matching_folders:
        print(folder)
        if args.retrieve is not None:
            retrieve_ann_plots(folder, args.retrieve, args.outdir)


if __name__ == "__main__":
    main(sys.argv[1:])
