#! /usr/bin/env python2

"""Computes weights for neural networks in a committee.

A committee is a collection of neural networks that collectively vote
on a classification task.
In its most naive implementation, the collective vote of the committee
is simply the average arithmetic mean of all participants' output.
However, one can also extend the concept and compute a weighted average
of the neural network outputs.

The formula for the optimal weights is given by Bishop 1995, S.368.
This script calculates these weights.

Assume we train L different neural networks.
They are trained to fit a desired function h(x).
Each network's output function can be described as a
    y_i(x) = h(x) + e_i(x),    i = 1...L,
with a random error e_i(x).

We then define the error correlation matrix C with elements
    C_ij = expectation(e_i(x) * e_j(x)),
or, approximated with a sample of size N:
    C_ij = 1/N * sum((y_i(x_n) - h(x_n))(y_j(x_n) - h(x_n)) for n = 1...N)

Let then D be the inverse of this correlation matrix: C*D = id.
Then, the optimal committee weights a_i (i=1...L) are given by:
    a_i = sum(D_ij for j=1...L) / norm(D),
where
    norm(D) = sum(D_jk for j=1...L for k=1...L)

Note that
    sum(a_i for i=1...L) == 1,
but `a_i is in R`, i. e. the committee weights may be both positive or
negative.
A consequence of that is, that even if `y_i is in [0; 1]` for all networks,
the committee output may be greater than 1 or less than 0.
Although, typically, the fraction of events lying outside the interval
[0; 1] decreases when N, the number of events used to calculate C,
increases.

Warning!
The above considerations technically only apply to ANNs which minimize
the sum-of-squares error function. (Estimator=MSE in TMVA)
Minimizing the cross-entropy (Estimator=CE) results in a different
choice of committee weights:
    a_i = dev_i**-2 / sum(dev_i**-2 for i=1...L)
where a_i, C_ii, and L are defined as above.
    dev_i = sum(abs(y_i(x_n) - h(x_n)) for i=1...N)
Use the option "--cross-entropy" to activate this method of weights
computation.
"""

from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import sys
import numpy
import argparse
import itertools

import ROOT  # pylint: disable=import-error
ROOT.gROOT.SetBatch(True)
ROOT.PyConfig.IgnoreCommandLineOptions = True

from GridSearch import common


def get_common_length(trees):
    """Return trees[0].GetEntries, raise ValueError on unequal length."""
    entries = numpy.array([tree.GetEntries() for tree in trees])
    if len(numpy.unique(entries)) != 1:
        raise ValueError("Trees have unequal length: " +
                         ", ".join(str(entry) for entry in entries))
    return entries[0]


def get_covariance(arr):
    """Simple replacement for `numpy.cov`, which we cannot use here.

    The difference to `numpy.cov` is that this function will not substract
    each variable's mean from the cells. Instead, it assumes the user has
    already done so.
    It essentially computes:
    ```
    cov[i, j] = (arr[i] * arr[j]).sum() / (N - 1)
    ```

    arr : array of shape (nvars, ndata).
    """
    nvars, ndata = arr.shape
    covariance = numpy.zeros(shape=(nvars, nvars))
    for i in xrange(nvars):
        for j in xrange(nvars):
            covariance[i, j] = (arr[i] * arr[j]).sum()
    return covariance / (ndata - 1)


def get_tree_data(trees, method_name, n_entries):
    """Read information from list of ROOT.TTree objects.

    trees       : List of trees to iterate over.
    method_name : Name of the MVA whose scores shall be read.
    n_entries   : Break after that many entries of the trees.

    Returns:
    scores  : Array of shape `(n_entries, len(trees))`.
              The i-th column contains the scores given to the event by
              the MVA `method_name` from the i-th tree.
              The j-th row contains the scores of each MVA for te j-th
              event. This method assumes that the j-th event is the same
              for all trees.
    classes : Array of shape `(n_entries, 1)`.
              Contains the actual class IDs for each event. Is retrieved
              by reading the branch "classID" from the first tree. Which
              tree it is read from should not matter.
    """
    get_common_length(trees)
    scores = numpy.zeros(shape=(n_entries, len(trees)))
    classes = numpy.zeros(shape=(n_entries, 1))
    iterator = itertools.izip(*(iter(tree) for tree in trees))
    for i, events in enumerate(iterator):
        if i == n_entries:
            break
        classes[i] = events[0].classID
        for j, event in enumerate(events):
            scores[i, j] = getattr(event, method_name)
    return scores, classes


def get_weights(covariance):
    """Compute optimal weights from the given covariance matrix."""
    invcov = numpy.linalg.inv(covariance)
    norm = invcov.sum()
    return invcov.sum(0) / norm


def get_ce_weights(scores, classes):
    """Compute optimal weights for cross-entropy based networks.

    While get_weights considers covariance between networks, this function
    disregards them.
    Weights are calculated based on the inverse average absolute deviation
    from the target value.

    scores  : Array of shape `(n_entries, n_mvas)`.
              Each row contains the scores given to the event by each MVA.
    classes : Array of shape `(n_entries, 1)`.
              Contains the actual class IDs for each event.

    Returns:
    weights : Array of shape `(n_mvas,)`
    The optimal committee weight given to each MVA.
    """
    n_events = classes.shape[0]
    assert n_events == scores.shape[0]
    # Get average deviation from the target values.
    mean_devs = numpy.abs(scores - classes).sum(0) / n_events
    # Each weight is the squared inverse deviation.
    weights = mean_devs**(-2)
    # Normalize to 1.
    weights /= weights.sum()
    return weights


def print_error_arg_ignored(option, cause):
    """Prints to stderr that an argument has been ignored due to use of
    another argument.
    """
    message = "Ignored argument {} because {} was used.".format(option, cause)
    print(message, file=sys.stderr)


def get_parser():
    """Return an argparse.ArgumentParser instance."""
    parser = argparse.ArgumentParser(description="Compute committee "
                                     "weights of neural networks")
    parser.add_argument("infiles", metavar="FILE", nargs="+",
                        help="input file (TMVA output ROOT file)")
    parser.add_argument("--entries", "-n", metavar="N", type=int, default=500,
                        help="Number of events to consider")
    parser.add_argument("--out", "-o", metavar="FILE", required=False,
                        help="Output file (default is stdout)")
    parser.add_argument("--method", "-m", metavar="NAME", default="MLP",
                        help="Name of the method to consider in the "
                        "ROOT files")
    parser.add_argument("--tree", "-t", metavar="NAME", default="TestTree",
                        help="Name of the tree to read from the "
                        "ROOT files")
    parser.add_argument("--cross-entropy", "-c", action="store_true",
                        help="Compute cross-entropy-based instead "
                        "of sum-of-squares-based weights. (This overrides "
                        "--covariance and --determinant.)")
    parser.add_argument("--covariance", "-C", action="store_true",
                        help="Print the covariance matrix and exit")
    parser.add_argument("--determinant", "-D", action="store_true",
                        help="Print the covariance matrix determinant "
                        "and exit")
    return parser

def main(argv):
    """Main function. You should pass `sys.argv[1:]` as argument."""
    # Parse command-line arguments.
    args = get_parser().parse_args(argv)
    if args.out is None:
        outfile = sys.stdout
    else:
        outfile = open(args.out, "w")
    # Main part of this script.
    try:
        files = [common.PyRootFile(path) for path in args.infiles]
        trees = [file_.Get(args.tree) for file_ in files]
        scores, classes = get_tree_data(trees, args.method, args.entries)
        if cross_entropy:
            if args.covariance:
                print_error_arg_ignored("--covariance", "--cross-entropy")
            if args.determinant:
                print_error_arg_ignored("--covariance", "--cross-entropy")
            weights = get_ce_weights(scores, classes)
            print(*weights, file=outfile)
        else:
            covariance = get_covariance((scores - classes).T)
            if args.covariance or args.determinant:
                if args.covariance:
                    print(covariance)
                if args.determinant:
                    print(numpy.linalg.det(covariance))
            else:
                print(*get_weights(covariance), file=outfile)
    finally:
        if outfile is not sys.stdout:
            outfile.close()


if __name__ == '__main__':
    main(sys.argv[1:])


