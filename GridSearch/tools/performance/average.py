#! /usr/bin/env python3
# -*- encoding: utf-8

"""Parse output of show_performance.sh and compute average and STD.

This script takes the output of show_performance.sh via stdin.
It groups the ANNs by the first digit of their ANN ID.
For example, the ANNs with names "ANN_10" through "ANN_19" would
go into the same group.
After that, for each group, it computes mean and STD of the performance
for each group -- both for 1- and for multi-prong.
Then, everything is output to stdout.
"""

from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import re
import sys
import math
import argparse
from collections import defaultdict

class ProngTuple(tuple):

    """Quick and dirty replacement for a namedtuple.

    It has the advantage of being constructable without parameters.
    """

    def __new__(cls):
        # Create a tuple of two empty lists.
        self = super(ProngTuple, cls).__new__(cls, ([], []))
        # Add attributes `sp` and `mp`.
        self.sp, self.mp = self
        return self

class ANNGrouper():

    """Pass in information about an ANN and it will be sorted by
    the ANN name's first five letters.
    """

    grouping_length = 5
    """How many letters to consider when grouping ANNs based on their name.
    The default, for example, would put all ANNs with names "ANN_1*" in
    one group.
    """

    def __init__(self):
        """Constructor. Nothing special, carry on."""
        self.groups = defaultdict(ProngTuple)

    def append(self, ann_data):
        """Add an ANN to the proper group.

        ann_data : tuple (ann_name, 1prongvalue, mprongvalue).
        """
        # Unpack argument.
        ann_name, sp_val, mp_val = ann_data
        # Read five characters ("ANN_0" or "ENS_2") to find out the
        # right group.
        key = ann_name[:self.grouping_length]
        # Append data.
        data = self.groups[key]
        data.sp.append(sp_val)
        data.mp.append(mp_val)


def readline(line):
    """Read line formatted like so:
    "ANN_NAME 1prongvalue mprongvalue"
    and return tuple `(ann_name, 1prong, mprong)`.
    """
    # Read line.
    matches = readline._regex.match(line)
    if matches is None:
        raise ValueError("Can't read line: {!r}".format(line))
    # Extract the three pieces of information.
    groups = matches.groups()
    assert len(groups) == 3, line
    ann_name = groups[0]
    sp_val, mp_val = float(groups[1]), float(groups[2])
    return ann_name, sp_val, mp_val
# Save the regex as a function attribute so it is preserved between
# calls. (This way, we don't have to compile it again and again!)
readline._regex = re.compile(
    r"^(ANN_\d{2,5}|ENS_\d{2,5}): (\d+\.\d+) (\d+\.\d+)$")


def get_avg_std(data):
    """Return mean and standard deviation of a finite iterable."""
    # We do this ourselves to avoid a time-consuming import of numpy.
    if not len(data):
        raise ValueError("Mean of empty list: {!r}".format(data))
    mean = sum(data) / len(data)
    if len(data) == 1:
        std = 0.0
    else:
        std = math.sqrt(
            sum((value-mean)**2 for value in data) / (len(data)-1)
            )
    return mean, std


def get_parser():
    """Return an argparse.ArgumentParser instance."""
    parser = argparse.ArgumentParser(description="Compute average "
                                     "and STD of performance of groups "
                                     "of ANNs")
    parser.add_argument("--input", "-i", metavar="FILE", default="-",
                        help='Input file. "-" denotes stdin.')
    parser.add_argument("--output", "-o", metavar="FILE", default="-",
                        help='Output file. "-" denotes stdout.')
    parser.add_argument("--numletters", "-n", metavar="INT", default=5,
                        type=int, help="How many letters to consider "
                        "when grouping. Default is 5.")
    return parser


def main(argv):
    """Main function. You should pass `sys.argv[1:]` as argument."""
    args = get_parser().parse_args(argv)
    grouper = ANNGrouper()
    grouper.grouping_length = args.numletters
    infile = sys.stdin if args.input == "-" else open(args.input, "r")
    with infile:
        for line in infile:
            grouper.append(readline(line))
    outfile = sys.stdout if args.output == "-" else open(args.output, "w")
    with outfile:
        for group_name, prongs in sorted(grouper.groups.items()):
            print(group_name)
            for prong_name, data in [("1p", prongs.sp), ("mp", prongs.mp)]:
                avg, std = get_avg_std(data)
                value = "{:2.2f} ± {:2.2f}".format(avg, std)
                print("{}: {}".format(prong_name, value),
                      file=outfile)


if __name__ == '__main__':
    main(sys.argv[1:])
