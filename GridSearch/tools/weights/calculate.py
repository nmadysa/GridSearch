#! /usr/bin/env python2

"""Compute event weights based on the samples' pT and mu spectrum.

Our goal is that the spectrum of signal and background in pT and in mu
look the same.

For that, we create histograms in pT and mu for signal and background.
We do that separately for 1-prong and 3-prong.
Then, we create pT-dependent weights for the background by dividing
the signal pT histogram by the background pT histogram.
And we create mu-dependent weights for the signal by dividing the
background mu histogram by the signal mu histogram.

We apply the mu weights to the signal because the signal's mu
spectrum has no value for us.
The signal is MC-generated, so its mu spectrum is arbitrary
and unphysical.

We apply the pT weights to the background because we want to conserve
the signal's pT spectrum.
This is necessary to ensure that in later steps, the signal efficiency
will stay more or less constant in pT.

This script then creates several files in the specified output directory.
    weights.root  : A ROOT file with subdirs "1prong" and "3prong".
                    It contains the weights histograms created by this
                    script.
    controls.root : Plots signal and background distribution of pT and mu
                    with a ratio plot below.
"""

from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import os
import sys
import array
import argparse
import textwrap

import ROOT  # pylint: disable=import-error
ROOT.gROOT.SetBatch(False)
ROOT.PyConfig.IgnoreCommandLineOptions = True

from GridSearch.common.pyrootfile import PyRootFile, force_create
from GridSearch.plotting import set_atlas_style

# Import TauCommon packages.
sys.path.append(os.path.join(os.pardir, os.pardir, "TauCommon", "macros"))
try:
    # pylint: disable=import-error
    import InputHandler
    import HistTools
    from HistFormatter import PlotOptions as PO
finally:
    sys.path.pop()


SELF_PATH = os.path.dirname(os.path.normpath(os.path.abspath(__file__)))
RES_PATH = os.path.join(SELF_PATH, os.pardir, os.pardir, "res")


# pylint: disable=too-few-public-methods

class HistCollection(object):

    """Collection of histograms which may be accessed by legible means.

    The calculation of pT/n_Vtx weights depends on eight histograms in
    total.
    Each histogram is combines the three parameters:
    - kind: signal/background,
    - nprong: 1/3,
    - var: pt/mu.

    This class allows keeping track of these eight histograms in a sane
    manner.
    """

    def __init__(self):
        """Create an instance.

        It is initially empty. Fill it with `set`.
        """
        self._hists = {kind: {var: {nprong: None for nprong in [1, 3]}
                              for var in ["pt", "mu"]}
                       for kind in ["signal", "background"]}

    def get(self, kind=None, var=None, nprong=None):
        """Retrieve histograms.

        All parameters must be passed and are recommended to be passed
        in `name=value` form.
        """
        return self._hists[kind][var][nprong]

    def get_kinds(self, var=None, nprong=None):
        """Return signal/background histograms."""
        h_sig = self.get(kind="signal", var=var, nprong=nprong)
        h_bkg = self.get(kind="background", var=var, nprong=nprong)
        return h_sig, h_bkg

    def get_vars(self, kind=None, nprong=None):
        """Return pt/mu histograms."""
        h_pt = self.get(kind=kind, var="pt", nprong=nprong)
        h_mu = self.get(kind=kind, var="mu", nprong=nprong)
        return h_pt, h_mu

    def get_nprongs(self, kind=None, var=None):
        """Return 1-/3-prong histograms."""
        h_one = self.get(kind=kind, var=var, nprong=1)
        h_three = self.get(kind=kind, var=var, nprong=3)
        return h_one, h_three

    def set(self, hist, kind=None, var=None, nprong=None):
        """Add a histogram to the collection.

        All parameters must be passed and are recommended to be passed
        in `name=value` form.
        """
        # Attempt accessing the element beforehand so that no new keys
        # may be created.
        _ = self._hists[kind][var][nprong]
        self._hists[kind][var][nprong] = hist


class HistGetter(object):

    """Class that retrieves a HistCollection from LLHCycle files.

    The LLHCycle files must have been produced by OfflineLLH/LLHCycle.cxx.
    """

    # --- Constructor ---

    def __init__(self, filenames, luminosity):
        """Create an instance.

        filenames  : Iterable of paths to ROOT files. All files must be
                     the output of an LLHCycle.
        luminosity : Total luminosity in pb^-1.
        """
        # Validate input.
        for filename in filenames:
            cycle_name = PyRootFile.parse_filename(filename).cycle
            if cycle_name != "LLHCycle":
                raise ValueError("Expected LLHCycle output, got {}"
                                 "instead.".format(cycle_name))
        # Set attributes.
        self.filenames = list(filenames)
        self.luminosity = luminosity
        # Use xrange to explicitly fail when accidentally run with Python3.
        self.pt_binning = list(xrange(0, 100, 5)) + [150, 1000]
        self.mu_binning = [0, 6] + list(xrange(7, 40))

    def get_hists(self):
        """Return a HistCollection grabbed from the passed files."""
        hists = HistCollection()
        # Nested loop to find all eight histograms. Read this carefully!
        var_iter = [("pt", "tight_tau_pt", self.pt_binning),
                    ("mu", "nvtx", self.mu_binning)]
        for var, ext_var_name, binning in var_iter:
            for nprong in (1, 3):
                h_sig, h_bkg = self._get_sig_bkg_hists(
                    varname=ext_var_name,
                    nprong=nprong,
                    binning=binning,
                    )
                hists.set(h_sig, kind="signal", var=var, nprong=nprong)
                hists.set(h_bkg, kind="background", var=var, nprong=nprong)
        return hists

    # --- Protected methods ---

    def _get_sig_bkg_hists(self, varname, nprong, binning):
        """Retrieve merged signal and data histograms."""
        histname = "VariablePlotter/{}_{}P".format(varname, nprong)
        signal, data = self._read_and_merge(self.filenames, histname,
                                            self.luminosity)
        # Rebin and normalize.
        signal = self._rebin(signal, binning)
        data = self._rebin(data, binning)
        signal.Scale(1. / signal.Integral())
        data.Scale(1. / data.Integral())
        return signal, data

    @staticmethod
    def _rebin(histogram, bin_edges):
        """Rebin the passed TH1F to the passed bin edges."""
        bin_edges = array.array("d", bin_edges)
        nbins = len(bin_edges)-1
        return histogram.Rebin(nbins, histogram.GetName(), bin_edges)

    @staticmethod
    def _read_and_merge(filenames, histname, luminosity):
        """Adapter method to the TauCommon.InputHandler module."""
        config_file = os.path.join(RES_PATH, "reader_config.yml")
        reader = InputHandler.Reader(filenames, configFile=config_file)
        options = reader.mergeOptions
        mc_hists, data = reader.readHistogramAndMerge(histname, luminosity,
                                                      mergeOptions=options)
        return mc_hists["Signal"], data


class EventWeights(object):

    """Collection of per-event-weights histograms.

    The pT/n_Vtx weights are calculated for as follows:
    - signal: apply weights (data/signal) in mu.
    - background: apply weights (signal/data) in pT.
    The weights are calculated for 1- and 3-prong indepedently.

    This class calculates these weights from a HistCollection and allows
    keeping track of these four histograms in a sane manner.
    """

    def __init__(self, hists):
        """Create an instance.

        hists : A filled HistCollection used to calculate the weights.
        """
        self._hists = dict()
        for nprong in (1, 3):
            pt_sig, pt_bkg = hists.get_kinds(var="pt", nprong=nprong)
            mu_sig, mu_bkg = hists.get_kinds(var="mu", nprong=nprong)
            pt_weights = self.__divide(pt_sig, pt_bkg, "pt_weights")
            mu_weights = self.__divide(mu_bkg, mu_sig, "mu_weights")
            self._hists[nprong] = {"pt": pt_weights, "mu": mu_weights}

    def get(self, var=None, nprong=None):
        """Retrieve weights histogram.

        var    : Either "pt" or "mu".
        nprong : Either `1` or `3`.
        """
        return self._hists[nprong][var]

    def get_vars(self, nprong=None):
        """Return pt/mu histograms."""
        h_pt = self.get(var="pt", nprong=nprong)
        h_mu = self.get(var="mu", nprong=nprong)
        return h_pt, h_mu

    def get_nprongs(self, var=None):
        """Return 1-/3-prong histograms."""
        h_one = self.get(var=var, nprong=1)
        h_three = self.get(var=var, nprong=3)
        return h_one, h_three

    @staticmethod
    def __divide(numerator, denominator, new_name):
        """Divide one histogram by the other, return the result."""
        result = numerator.Clone(new_name)
        result.Divide(denominator)
        result.Draw("")
        return result


class AbstractRootFileWriter(object):

    """Class that whose `write` method creates prong folders in a ROOT file."""

    def write(self, rootfile):
        """Write into the passed ROOT file."""
        rootfile.cd()
        for nprong in (1, 3):
            dirname = "{}prong".format(nprong)
            prongdir = rootfile.mkdir(dirname)
            if not prongdir:
                raise IOError("Could not create directory " + repr(dirname))
            prongdir.cd()
            self._write_prongdir(prongdir, nprong)
        rootfile.cd()

    def _write_prongdir(self, prongdir, nprong):
        """Write the contents of a prong folder."""
        raise NotImplementedError()


class WeightsFileWriter(AbstractRootFileWriter):

    """Class that writes the pt/mu weights into a ROOT file."""

    def __init__(self, weights):
        """Create an instance.

        weights : An `EventWeights` instance.
        """
        self.weights = weights

    def _write_prongdir(self, prongdir, nprong):
        """Write the pt/mu weights into the passed ROOT file directory."""
        self.weights.get(var="pt", nprong=nprong).Write()
        self.weights.get(var="mu", nprong=nprong).Write()


class ControlPlotsWriter(AbstractRootFileWriter):

    """Class that outputs control plots of the pt/mu distributions."""

    class _ControlPlot(object):

        """Internal representation of control plots.

        This class handles adaption to the TauCommon modules.
        """

        def __init__(self, var_name, x_range):
            """Create an instance.

            var_name : Name of the X-axis.
            x_range  : Range of the X-axis.
            """
            self.var_name = var_name
            self.x_range = x_range
            self.canvas = None

        def plot_main(self, signal, data):
            """Plot two distributions called "signal" and "data"."""
            plotter = HistTools.Plotter()
            self.canvas = plotter.plotHistograms(
                [data, signal],
                plotOptions=[
                    PO(marker_color=ROOT.kBlack, marker_style=22),
                    PO(marker_color=ROOT.kRed, marker_style=23),
                    ],
                drawOptions=["P", "P"],
                canvasName="{} weight control plots".format(self.var_name),
                )
            xlow, xup = self.x_range  # pylint: disable=unpacking-non-sequence
            formatter = HistTools.Formatter()
            formatter.setMinMax(data, xlow, xup, "x")
            formatter.setTitle(data, "", "y")

        def add_legend(self, left, right, bottom, top):
            """Plot two distributions called "signal" and "data"."""
            formatter = HistTools.Formatter()
            formatter.addLegendToCanvas(
                self.canvas,
                legendOptions=dict(x1=left, x2=right, y1=bottom, y2=top),
                overwriteLabels=["data", "signal"],
                overwriteDrawOptions=["P", "P"],
                )

        def add_ratio(self, hist_ratio):
            """Add a ratio distribution to the canvas."""
            ratio_name = "c_{}_ratio".format(self.var_name)
            ratio_canvas = ROOT.TCanvas(ratio_name, ratio_name, 700, 700)
            ratio_canvas.cd()
            hist_ratio.Draw()
            xlow, xup = self.x_range  # pylint: disable=unpacking-non-sequence
            formatter = HistTools.Formatter()
            formatter.setMinMax(hist_ratio, xlow, xup, "x")
            formatter.setTitle(hist_ratio, "", "y")
            plotter = HistTools.Plotter()
            self.canvas = plotter.addRatioToCanvas(self.canvas, ratio_canvas)
            self.canvas.SetName("{}_control".format(self.var_name))

    def __init__(self, hists, weights):
        """Create an instance.

        hists   : A filled `HistCollection` instance.
        weights : An `EventWeights` instance.
        """
        self.hists = hists
        self.weights = weights

    def _write_prongdir(self, prongdir, nprong):
        """Write pt/mu distribution plots into the passed ROOT file folder."""
        pt_plot = self._plot_pt(nprong)
        mu_plot = self._plot_mu(nprong)
        for plot in (pt_plot, mu_plot):
            plot.canvas.Write()

    def _plot_pt(self, nprong):
        """Create _ControlPlot instance for pt distributions."""
        plot = self._ControlPlot("pt", x_range=(15, 150))
        signal, data = self.hists.get_kinds(var="pt", nprong=nprong)
        plot.plot_main(signal, data)
        plot.add_legend(left=.75, right=.9, bottom=.75, top=.9)
        plot.add_ratio(self.weights.get(var="pt", nprong=nprong))
        return plot

    def _plot_mu(self, nprong):
        """Create _ControlPlot instance for pt distributions."""
        plot = self._ControlPlot("mu", x_range=(7, 34))
        signal, data = self.hists.get_kinds(var="mu", nprong=nprong)
        plot.plot_main(signal, data)
        plot.add_legend(left=.2, right=.35, bottom=.75, top=.9)
        plot.add_ratio(self.weights.get(var="mu", nprong=nprong))
        return plot


class PythonScriptWriter(object):

    """Class that writes the pt/mu weights into two Python script files."""

    weights_script_template = textwrap.dedent('''\
    """Contains event weights for TMVA training on TTNT files.

    This file has been automatically created by macros/calculate_weights.
    The strings returned by the functions in this module may be wrapped
    in `ROOT.TCut` and passed to TMVA.Set*WeightExpression.
    """


    def GetWeights_BackgroundPt_{nprong:d}p():
        """Return string containing weights for background events."""
        return ("{pt_formula:s}")


    def GetWeights_SignalMu_{nprong:d}p():
        """Return string containing weights for signal events."""
        return ("{mu_formula:s}")

    ''')

    def __init__(self, weights):
        """Create an instance.

        weights : An `EventWeights` instance.
        """
        self.weights = weights

    def write(self, outdir):
        """Write files "weights_{1,3}prong.py" into the passed directory."""
        for nprong in (1, 3):
            outname = os.path.join(outdir, "weights_{}prong.py".format(nprong))
            with open(outname, "w") as outfile:
                outfile.write(self._get_script_content(nprong))

    def _get_script_content(self, nprong):
        """Return contents of a weights_?prong.py script as a string."""
        h_pt, h_mu = self.weights.get_vars(nprong=nprong)
        pt_formula = self._get_formula(hist=h_pt, var_name="tau_Et",
                                       conversion_factor=1000.)
        mu_formula = self._get_formula(hist=h_mu, var_name="mu")
        # Add line breaks to the formulas so the output files look
        # a lot better.
        return self.weights_script_template.format(
            nprong=nprong,
            pt_formula=pt_formula.replace(" + ", ' +"\n            "'),
            mu_formula=mu_formula.replace(" + ", ' +"\n            "'),
            )

    @staticmethod
    def _get_formula(hist, var_name, conversion_factor=1.0):
        """Turn `hist` into a string that may be passed to `ROOT.TCut`.

        hist     : A `ROOT.TH1F` instance.
        var_name : Name of the X-axis variable of the histogram.

        `conversion_factor` is an optional factor to be multiplied to the
        X-axis.
        This is useful if `hist`'s X axis is e.g. given in GeV, but the
        `ROOT.TCut` string needs its variable in MeV.
        In that case, set `conversion_factor=1e3`.
        """
        template = "{weight}*({low}<={var_name}&&{var_name}<={up})"
        n_last_bin = hist.GetNbinsX() + 1
        # Read carefully!
        return " + ".join(
            template.format(
                weight=hist.GetBinContent(i),
                var_name=var_name,
                low=conversion_factor*hist.GetBinLowEdge(i),
                up=conversion_factor*hist.GetBinLowEdge(i+1),
                )
            for i in xrange(1, n_last_bin)
            )


def get_parser():
    """Return an argparse.ArgumentParser instance."""
    parser = argparse.ArgumentParser(description="Event weight calculator")
    parser.add_argument("files", metavar="INPUT_FILES", nargs="+",
                        help="input files (LLH cycle output)")
    parser.add_argument("--lumi", "-l", metavar="LUMINOSITY", type=float,
                        default=20281.4,
                        help="Luminosity in pb^-1 (default: 20281.4)")
    parser.add_argument("--outdir", "-o", default=os.curdir,
                        help="output directory")
    return parser


def main(argv):
    """Main function. You should pass `sys.argv[1:]` as argument."""
    args = get_parser().parse_args(argv)
    set_atlas_style()
    # Calculate weights.
    hist_getter = HistGetter(args.files, args.lumi)
    hists = hist_getter.get_hists()
    weights = EventWeights(hists)
    # Write weights.root.
    writer = WeightsFileWriter(weights)
    outfile = force_create(os.path.join(args.outdir, "weights.root"), "force")
    writer.write(outfile)
    # Write controls.root.
    writer = ControlPlotsWriter(hists, weights)
    outfile = force_create(os.path.join(args.outdir, "controls.root"), "force")
    writer.write(outfile)
    # Write Python scripts.
    writer = PythonScriptWriter(weights)
    writer.write(args.outdir)


if __name__ == "__main__":
    main(sys.argv[1:])
