#! /usr/bin/env python2

"""Convert weights.root to Python scripts containing the same weights."""

from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import os
import sys
import argparse

import ROOT  # pylint: disable=import-error
ROOT.gROOT.SetBatch(False)
ROOT.PyConfig.IgnoreCommandLineOptions = True

weights_script_template = '''
"""Contains event weights for TMVA training on TTNT files.

This file has been automatically created by macros/calculate_weights.
The strings returned by the functions in this module may be wrapped
in `ROOT.TCut` and passed to TMVA.Set*WeightExpression.
"""


def GetWeights_BackgroundPt_{nprong:d}p():
    """Return string containing weights for background events."""
    return ("{pt_formula:s}")


def GetWeights_SignalMu_{nprong:d}p():
    """Return string containing weights for signal events."""
    return ("{mu_formula:s}")

'''

def to_tcut_string(histogram, variable):
    """Turn a histogram of event weights into a string.
    This string can be passed to the ROOT.TCut constructor.

    histogram : Histogram containing bin edges and event weights
    variable  : variable of the histograms
    """
    bin_formulas = []
    formula_template = "{val}*({low}<={var}&&{var}<={up})"
    for i in range(1, histogram.GetNbinsX()+1):
        bin_formulas.append(formula_template.format(
            val=histogram.GetBinContent(i),
            var=variable,
            low=histogram.GetBinLowEdge(i),
            up=histogram.GetBinLowEdge(i+1),
            ))
    return " + ".join(bin_formulas)

def to_python_script(directory, nprong, mu_weights, pt_weights):
    """Create a module weights_{nprong}p.py.

    Create a module that contains functions which will return
    cut strings that may be used to calculate event weights.

    directory  : directory in which the file will be created.
    nprong     : 1 or 3
    mu_weights : TH1F of signal mu weights
    pt_weights : TH1F of background p_t weights.
    """
    filepath = os.path.join(directory,
                            "weights_{}prong.py".format(nprong))
    with open(filepath, "w") as outfile:
        pt_formula = to_tcut_string(pt_weights, "tau_Et")
        mu_formula = to_tcut_string(mu_weights, "mu")
        # Add line breaks to the formulas so the output files look
        # a lot better.
        outfile.write(weights_script_template.format(
            nprong=nprong,
            pt_formula=pt_formula.replace(' + ', ' +"\n            "'),
            mu_formula=mu_formula.replace(' + ', ' +"\n            "'),
            ))



def get_parser():
    """Return an argparse.ArgumentParser instance."""
    parser = argparse.ArgumentParser(description="Event weight calculator")
    parser.add_argument("infile", metavar="<file>",
                        help="input file (weights.root)")
    return parser


def main(argv):
    """Main function. You should pass `sys.argv[1:]` as argument."""
    args = get_parser().parse_args(argv)
    rootfile = ROOT.TFile(args.infile)
    if rootfile.IsZombie():
        raise OSError(17, args.infile)
    for nprong in (1, 3):
        folder = rootfile.Get("{}prong".format(nprong))
        to_python_script(".", nprong, folder.Get("mu_weights"), folder.Get("pt_weights"))


if __name__ == "__main__":
    main(sys.argv[1:])
