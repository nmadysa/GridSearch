#! /usr/bin/env python2

"""Script to test whether the weights written by calculate_weights do
what they're supposed to do.
"""

from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import sys
import abc
import argparse
from functools import partial

import ROOT
ROOT.gROOT.SetBatch(True)
ROOT.PyConfig.IgnoreCommandLineOptions = True

from GridSearch import common


class AbstractTester(object):

    """Class that tests whether the weights have been computed correctly.

    Usage:
    tester = Tester("TTNTCycle.mc.Signal.root",
                    "TTNTCycle.data.Data.root",
                    1, "current_weights/weights.root")
    tester.show_results()
    """

    __metaclass__ = abc.ABCMeta

    # Specifies the number of events after which itertree should print
    # a small progress report.
    # Set to zero to deactivate this feature.
    n_feedback = 20000

    # Name of TTree to be loaded from each file.
    # Used by the method `get_tree`.
    # This must be set by each subclass.
    tree_name = ""

    # Name of the SFrame cycle handled by this class.
    # Used by the method `does_filename_match`.
    # This must be set by each subclass.
    cycle_name = ""


    def __init__(self, filenames, nprong):
        """
        filenames    : Iterable of paths to ROOT files.
        nprong       : 1, 2 or 3. 2 means "multiprong"
        """
        if not nprong in (1, 3):
            raise common.NProngError(nprong)
        self.nprong = nprong
        self.files = []
        for name in filenames:
            if not self.does_filename_match(name):
                raise ValueError("filename not accepted: "+name)
            self.files.append(common.PyRootFile(name))

    @classmethod
    def does_filename_match(cls, filename):
        """Return whether a given file is accepted by this object.

        This method is called by the constructor for each passed
        file name.
        """
        cycle_name = common.PyRootFile.parse_filename(filename).cycle
        return cycle_name == cls.cycle_name

    def show_results(self):
        """Show canvases for the pT and the mu distribution.

        If the weights were computed correctly, signal (red) and
        background (blue) should look more or less the same.

        Returns list of canvases and list of histogram pairs.
        """
        pt_hists = self.create_histograms("pt", 200, 0, 200e3)
        mu_hists = self.create_histograms("mu", 39, 0, 39)
        self.fill_histograms(pt_hists, mu_hists)
        canvases = self.draw(("pt", pt_hists), ("mu", mu_hists))
        return canvases, (pt_hists, mu_hists)

    def create_histograms(self, name, nbins, xlow, xup):
        """Creates tuple (signal, background) of TH1Fs to be filled.

        name : E.g. name of the variable being shown.
               This will get mangled with self.nprong and the postfix
               "sig"/"bkg"
        nbins, xlow, xup : Same as parameters to TH1F constructor.
        """
        # Create signal and background histogram as a tuple.
        name_template = "{}_{}_{}"
        hists = common.SigBkgPair(
            ROOT.TH1F(name_template.format(name, "sig", self.nprong),
                      "Signal", nbins, xlow, xup),
            ROOT.TH1F(name_template.format(name, "bkg", self.nprong),
                      "Background", nbins, xlow, xup),
            )
        # SetDirectory(0) so that the histograms don't get destroyed
        # when a ROOT file is closed.
        hists.sig.SetDirectory(0)
        hists.bkg.SetDirectory(0)
        # Everybody likes colors.
        hists.sig.SetLineColor(ROOT.kRed)
        hists.bkg.SetLineColor(ROOT.kBlue)
        return hists

    def fill_histograms(self, pt_hists, mu_hists):
        """Fill histograms with pT and mu.

        tree : ROOT.TTree containing the events
        pt_hist : Histogram to be filled with event.tau_Et
        mu_hist : Histogram to be filled with event.mu
        get_weights : function taking the tree and returning a tuple
                      (pt_weight, mu_weight)
        """
        # Fixate two of the three arguments to process_*_event, so
        # that we only have to call `process_*(event)`.
        process_signal = partial(self.process_sig_event,
                                 pt_hists.sig, mu_hists.sig)
        process_background = partial(self.process_bkg_event,
                                     pt_hists.bkg, mu_hists.bkg)
        # Loop over all events, call process_* for each one.
        self._iterallevents(process_signal, process_background)
        # Normalize.
        for hist in pt_hists + mu_hists:
            hist.Scale(1.0 / hist.Integral())

    def draw(self, *largs):
        """Draw histogram pairs and return list of canvases.

        largs      : Tuples (name, histograms).
        name       : Name of the canvas to be created.
        histograms : SigBkgPair namedtuple of the histograms to be
                     plotted.
        """
        canvases = []
        for name, histos in largs:
            canvas = ROOT.TCanvas("{}_{}".format(name, self.nprong))
            histos.sig.Draw("hist")
            histos.bkg.Draw("hist same")
            canvas.BuildLegend(0.75, 0.75, 0.9, 0.9)
            canvases.append(canvas)
        return canvases

    def _iterallevents(self, sig_callback, bkg_callback):
        """Iterate over all events on all files.

        Loop over all events in all files, call the proper callback
        function for each event, print progress reports to stdout.
        """
        # Loop over files.
        for file_ in self.files:
            print("Input file:", file_.GetName())
            callback = (sig_callback if file_.is_signal_file() else
                        bkg_callback)
            # Loop over events.
            i = 0
            tree = file_.Get(self.tree_name)
            for event in common.itertree(tree, self.n_feedback):
                if self.match_prong(event):
                    callback(event)
                    i += 1
            print("Counted {} events ({}-prong)".format(i, self.nprong))

    # --- Methods to be overridden by subclasses ---

    @abc.abstractmethod
    def process_sig_event(self, pt_hist, mu_hist, event):
        """Fill the passed pT and mu histograms with the passed event.

        This is a call-back method for fill_histograms.
        """
        pass

    @abc.abstractmethod
    def process_bkg_event(self, pt_hist, mu_hist, event):
        """Fill the passed pT and mu histograms with the passed event.

        This is a call-back method for fill_histograms.
        """
        pass

    def match_prong(self, event):
        """Return True if the current event matches the prong we want."""
        if self.nprong == 2:
            return 2 <= event.tau_numTrack <= 3
        else:
            return event.tau_numTrack == self.nprong


class TTNTTester(AbstractTester):

    """Concrete subclass of AbstractTester that deals with TTNT files.

    Reweighs the signal events with mu-dependent weights both in the
    mu and the pT histogram.
    Background events get pT-dependent weights only in the pT histogram.
    For the mu histogram, all background event weights are 1.0.
    """

    # Set abstract class attributes of the super class.
    tree_name = "tau"
    cycle_name = "TTNTCycle"

    def __init__(self, filenames, nprong, weights_name=None):
        """Create a weights tester for TTNT files.

        weights_name is the path to the ROOT file containing the weights
        histograms.
        If not passed, event weights are pulled from the TTNT file itself.
        """
        AbstractTester.__init__(self, filenames, nprong)
        if weights_name is not None:
            self.use_weights_file = True
            self.weights_file = common.PyRootFile(weights_name)
            self.pt_weights = self.weights_file.Get("{}prong/pt_weights"
                                                    .format(self.nprong))
            self.mu_weights = self.weights_file.Get("{}prong/mu_weights"
                                                    .format(self.nprong))
        else:
            self.use_weights_file = False

    def match_prong(self, event):
        """Return True if the current event matches the prong we want."""
        return event.tau_numTrack == self.nprong

    def process_sig_event(self, pt_hist, mu_hist, event):
        """Fill the passed pT and mu histograms with the passed event."""
        if self.use_weights_file:
            weight = self.mu_weights.GetBinContent(
                self.mu_weights.FindBin(event.mu))
        else:
            weight = event.tau_weight
        pt_hist.Fill(event.tau_Et, weight)
        mu_hist.Fill(event.mu, weight)

    def process_bkg_event(self, pt_hist, mu_hist, event):
        """Fill the passed pT and mu histograms with the passed event."""
        if self.use_weights_file:
            # Convert pT from MeV to GeV.
            pt = event.tau_Et / 1000.0
            weight = self.pt_weights.GetBinContent(
                self.pt_weights.FindBin(pt))
        else:
            weight = event.tau_weight
        pt_hist.Fill(event.tau_Et, weight)
        mu_hist.Fill(event.mu, 1.0)


class NtupleTester(AbstractTester):

    """Concrete subclass of AbstractTester that deals with Ntuple files.

    Reweighs the signal events with mu-dependent weights both in the
    mu and the pT histogram.
    Background events get pT-dependent weights only in the pT histogram.
    For the mu histogram, all background event weights are 1.0.
    """

    # Set abstract class attributes of the super class.
    tree_name = "MiniTree"
    cycle_name = "NtupleCycle"

    def __init__(self, filenames, nprong, weights_name):
        """Create a weights tester for Ntuple files.

        weights_name is the path to the ROOT file containing the weights
        histograms.
        """
        AbstractTester.__init__(self, filenames, nprong)
        self.weights_file = common.PyRootFile(weights_name)
        self.pt_weights = self.weights_file.Get("{}prong/pt_weights"
                                                .format(self.nprong))
        self.mu_weights = self.weights_file.Get("{}prong/mu_weights"
                                                .format(self.nprong))
        # Heuristic test if pt_weights takes pT in GeV or MeV.
        last_bin = self.pt_weights.GetNbinsX()+1
        pt_scale = self.pt_weights.GetBinLowEdge(last_bin)
        if pt_scale > 1e3:
            raise TypeError("pt_weights seems to expect pT in MeV, but "
                            "we want to pass in GeV.")

    def match_prong(self, event):
        """Return True if the current event matches the prong we want."""
        if self.nprong == 2:
            return 2 <= event.NumTrack <= 3
        else:
            return event.NumTrack == self.nprong

    def process_sig_event(self, pt_hist, mu_hist, event):
        """Fill the passed pT and mu histograms with the passed event."""
        weight = self.mu_weights.GetBinContent(
            self.mu_weights.FindBin(event.mu))
        pt_hist.Fill(event.Et, weight)
        mu_hist.Fill(event.mu, weight)

    def process_bkg_event(self, pt_hist, mu_hist, event):
        """Fill the passed pT and mu histograms with the passed event."""
        # Convert pT from MeV to GeV.
        pt = event.Et / 1000.0
        weight = self.pt_weights.GetBinContent(self.pt_weights.FindBin(pt))
        pt_hist.Fill(event.Et, weight)
        mu_hist.Fill(event.mu, 1.0)


def get_tester_class(filename):
    """Return an adequate implementation of `AbstractTester`.

    filename : a file name used to determine which class to return.
    """
    # pylint: disable=invalid-name
    for Tester in TTNTTester, NtupleTester:
        if Tester.does_filename_match(filename):
            return Tester
    raise ValueError("Don't know how to handle this file: "+filename)


def get_parser():
    """Return an argparse.ArgumentParser instance."""
    parser = argparse.ArgumentParser()
    parser.add_argument("files", nargs="+", help="Input file")
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument("--weights", "-w",
                       help="Path to file containing weights histograms.")
    group.add_argument("--fromtree", "-t", action="store_true",
                       help="Read weights from a branch \"tau_weights\" "
                       "inside the TTree of the ROOT files. This does "
                       "not work for Ntuple files.")
    return parser


def main(argv):
    """Main function. You should pass `sys.argv[1:]` as argument."""
    args = get_parser().parse_args(argv)
    all_canvases = []    # Contains all canvases.
    all_histograms = []  # Contains tuples (pt_hist, mu_hist).
    for prong in 1, 3:
        Tester = get_tester_class(args.files[0])
        if Tester is NtupleTester:
            tester = NtupleTester(args.files, prong, args.weights)
        elif args.fromtree:
            tester = TTNTTester(args.files, prong)
        else:
            tester = TTNTTester(args.files, prong, args.weights)
        canvases, histograms = tester.show_results()
        all_canvases.extend(canvases)
        all_histograms.append(histograms)
    for canvas in all_canvases:
        canvas.SaveAs(".png")


if __name__ == "__main__":
    main(sys.argv[1:])
