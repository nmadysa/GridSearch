#! /usr/bin/env python2

"""Script that plots the signal efficiency w.r.t. the applied score cut."""

from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import os
import sys
import argparse

import ROOT  # pylint: disable=import-error
ROOT.gROOT.SetBatch(False)
ROOT.PyConfig.IgnoreCommandLineOptions = True

from GridSearch import plotting
from GridSearch import common


class EfficiencyPlotter(object):

    """Class that plots signal efficiency w.r.t. applied score cut.

    Usage:
    >>> plotter = EfficiencyPlotter(
    ...     "EfficiencyEvaluationCycle.mc.Signal.root", 1)
    >>> canvas = plotter.plot_graph("BDT")
    >>> canvas.SaveAs(".eps")
    """

    #~ def __init__(self, infile, nprong, pt_range=(0, -1)):
    def __init__(self, infile, nprong):
        """Initialize an object of this class.

        infile   : Path to an EfficiencyEvaluation.mc.Signal.root file.
        nprong   : Prongness of the file. Must be in `{1, 2, 3}`.
        """
        #~ pt_range : Interval of pT to consider. Given as a tuple
                   #~ `(pt_min, pt_max)` in GeV.
                   #~ If `pt_max == -1`, it refers to the last bin.
        #~ """
        if nprong not in {1, 2, 3}:
            raise common.NProngError(nprong)
        elif not common.PyRootFile.is_signal_file(infile):
            raise ValueError("Not a signal file: {!r}".format(self.infile))
        self.infile = common.PyRootFile(infile)
        self.nprong = nprong
        #~ # Convert pt_range to bin indices before saving it.
        #~ self.pt_range = self.__pt_to_bins(pt_range)

    def get_score_histogram(self, method):
        """Return a score histogram for this object's pT range."""
        #~ h_pt_score = self.infile.Get("h_pT_{}Score".format(method))
        #~ ptmin, ptmax = self.pt_range
        #~ h_score = h_pt_score.ProjectionY("_py", ptmin, ptmax)
        h_score = self.infile.Get("h_{}Score".format(method))
        return h_score

    def get_total_signal(self):
        """Return number of truth taus for this object's prong and pT."""
        h_prong_pt_mu = self.infile.Get("h_truthPt")
        prong_bin = 2 if self.nprong == 1 else 4
        #~ ptmin, ptmax = self.pt_range
        #~ return h_prong_pt_mu.Integral(prong_bin, prong_bin,
                                      #~ ptmin, ptmax, 0, -1)
        return h_prong_pt_mu.Integral(prong_bin, prong_bin, 0, -1, 0, -1)

    def itercuts(self, method):
        """Return iterator over score cuts and respective signal efficiency."""
        h_score = self.get_score_histogram(method)
        n_truth = self.get_total_signal()
        n_passed = 0
        for i_bin in reversed(xrange(h_score.GetNbinsX()+2)):
            n_passed += h_score.GetBinContent(i_bin)
            cut = h_score.GetBinLowEdge(i_bin)
            efficiency = n_passed / n_truth
            yield cut, efficiency

    def plot_graph(self, method):
        """Create a graph, plot it, and return the canvas."""
        canvas = self._create_canvas(method)
        points = list(self.itercuts(method))
        graph = self._create_graph(points)
        graph.SetTitle(method)
        graph.SetMaximum(1.0)
        graph.Draw("LA")
        graph.GetXaxis().SetRangeUser(0, 1.0)
        graph.GetXaxis().SetTitle("Score Cut")
        graph.GetYaxis().SetTitle("Signal efficiency (Reco+ID)")
        plotting.create_energy_text(x_y=(0.2, 0.26), size=0.045)
        prong_text = {
            1: "1-prong",
            2: "multi-prong",
            3: "3-prong",
            }[self.nprong]
        plotting.create_text(prong_text, (0.2, 0.2), size=0.045)
        plotting.create_legend([graph], (0.6, 0.75, 0.9, 0.9))
        return canvas

    # --- Protected methods ---

    def _create_canvas(self, method):
        """Return a well-named TCanvas object."""
        prong_name = {1: "1p", 2: "mp", 3: "3p"}[self.nprong]
        #~ ptmin, ptmax = self.pt_range
        #~ canvas_name = "effvscut_{method}_{prong_name}_{ptmin}{ptmax}"
        canvas_name = "effvscut_{}_{}".format(method, prong_name)
        canvas = ROOT.TCanvas(canvas_name, canvas_name, 720, 700)
        canvas.SetGrid(True, True)
        return canvas

    @staticmethod
    def _create_graph(points):
        """Create a ROOT.TGraph object from a sequence of points."""
        graph = ROOT.TGraph(len(points))
        ROOT.SetOwnership(graph, False)
        for i, (x, y) in enumerate(points):  # pylint: disable=invalid-name
            graph.SetPoint(i, x, y)
        return graph

    #~ def __pt_to_bins(self, pt_range):
        #~ """Return bin numbers corresponding to the given pT range."""
        #~ ptmin, ptmax = pt_range
        #~ h_prong_pt_mu = self.infile.Get("h_truthPt")
        #~ pt_axis = h_prong_pt_mu.GetYaxis()
        #~ # Look up bin indices by pT.
        #~ lowbin = pt_axis.FindFixBin(ptmin)
        #~ # Substract a small amount from ptmax so that if it equals
        #~ # exactly a bin border, it will belong to the *lower* of the
        #~ # two bins.
        #~ upbin = -1 if ptmax < 0 else pt_axis.FindFixBin(ptmax-1e-6)
        #~ return lowbin, upbin


def get_parser():
    """Return an argparse.ArgumentParser instance."""
    parser = argparse.ArgumentParser(
        description="Plot signal efficiency over score cut.")
    parser.add_argument("file", type=str, metavar="FILE",
                        help="Input file (signal efficiency evaluation "
                        "output)")
    #~ parser.add_argument("--ptmin", type=float, default=0,
                        #~ help="Minimum pT to consider.")
    #~ parser.add_argument("--ptmax", type=float, default=-1,
                        #~ help="Maximum pT to consider.")
    parser.add_argument("--nprong", "-n", type=int, required=True,
                        help="Prongness of the file. 1, 2, or 3.")
    parser.add_argument("--method", "-m", type=str, required=True,
                        action="append",
                        help="Method to evaluate. (May be used more "
                        "than once)")
    parser.add_argument("--outdir", "-o", default=os.curdir,
                        help="output directory")
    return parser


def main(argv):
    """Main function. You should pass `sys.argv[1:]` as argument."""
    args = get_parser().parse_args(argv)
    plotting.set_atlas_style()
    #~ pt_range = args.ptmin, args.ptmax
    #~ plotter = EfficiencyPlotter(args.file, args.nprong, pt_range)
    plotter = EfficiencyPlotter(args.file, args.nprong)
    for method in args.method:
        canvas = plotter.plot_graph(method)
        outfile = os.path.join(args.outdir, canvas.GetName()+".eps")
        canvas.SaveAs(outfile)


if __name__ == '__main__':
    main(sys.argv[1:])
