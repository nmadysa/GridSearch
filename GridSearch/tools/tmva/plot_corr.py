#! /usr/bin/env python2

"""Script to plot the correlations of variables as determined by TMVA.

Read, format, and draw a correlation plot for signal and background
from a TMVA.root file.
"""

from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import os
import sys
import argparse

import ROOT
ROOT.gROOT.SetBatch(True)  # pylint: disable=no-member
ROOT.PyConfig.IgnoreCommandLineOptions = True

from GridSearch import common
from GridSearch.common import idvariables

def get_parser():
    """Return an argparse.ArgumentParser instance."""
    parser = argparse.ArgumentParser()
    parser.add_argument("infile", metavar="<TMVA output>", type=str,
                        help="The input TMVA.root file.")
    parser.add_argument("--nprong", "-n", choices=[1, 3], default=None,
                        metavar="<n>", help="Add an optional suffix to "
                        "the output file names.")
    parser.add_argument("--outdir", "-o", default=os.curdir, metavar="DIR",
                        help="output directory")
    return parser


def main(argv):

    """Main function. You should pass `sys.argv[1:]` as argument."""

    args = get_parser().parse_args(argv)
    infile = common.PyRootFile(args.infile)
    for hist_name_suffix in common.SigBkgPair("S", "B"):
        # Create canvas.
        canvas_name = "Correlation_{kind}{prong}".format(
            kind={"S": "Signal", "B": "Background"}[hist_name_suffix],
            prong={None: "", 1: "_1p", 3: "_3p"}[args.nprong],
            )
        canvas = ROOT.TCanvas(canvas_name, canvas_name, 400, 400)  # pylint: disable=no-member
        canvas.cd()
        # Find and draw histogram.
        hist = infile.Get("CorrelationMatrix" + hist_name_suffix)
        for axis in hist.GetXaxis(), hist.GetYaxis():
            for i_bin in range(1, axis.GetNbins()+1):
                # Translate TMVA's var names into nice formulas.
                ttnt_name = axis.GetBinLabel(i_bin)
                ntuple_name = idvariables.get_ntuple_name(ttnt_name)
                nice_name = idvariables.get_nice_name(ntuple_name)
                axis.SetBinLabel(i_bin, nice_name)
        hist.GetXaxis().LabelsOption("v")
        hist.Draw("colz text")  # colz: colors+scale
                                # text: label bins with their contents
        # Adjust margins and save.
        canvas.SetLeftMargin(0.15)
        canvas.SetBottomMargin(0.15)
        canvas.SetRightMargin(0.12)
        canvas.SaveAs(os.path.join(args.outdir, canvas_name+".eps"))


if __name__ == "__main__":
    main(sys.argv[1:])




