#! /usr/bin/env python2

"""Put different TMVA classifiers into a single ROOT file.

This takes several TMVA output files and names of classifiers within them
and copies each classifier into a new destination file.
The process is not complete and likely to be buggy.
"""

from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import sys
import argparse

import ROOT  # pylint: disable=import-error
ROOT.gROOT.SetBatch(True)
ROOT.PyConfig.IgnoreCommandLineOptions = True

from GridSearch.common.pyrootfile import PyRootFile, force_create


class CopyAction(object):

    """Defers copying of the MVA method until the `copy` method is called.

    Usage:
    >>> action = CopyAction("infile.root", "BDT", "infileBDT")
    >>> outfile = ROOT.TFile("outfile.root", "CREATE")
    >>> action.copy(outfile)
    """

    def __init__(self, filename, old_name, new_name):
        """Create a new instance.

        filename : Path to a ROOT file to be read.
        old_name : Name of the MVA method in `filename`.
        new_name : Name of `old_name`'s clone.
        """
        self.old_name = old_name
        self.new_name = new_name
        self.rootfile = PyRootFile(filename, "READ")
        self.method_dir = self.__get_method_dir()
        self.method = self.method_dir.Get(self.old_name)

    def __get_method_dir(self):
        """Find the Method_* directory that contains `self.old_name`."""
        for method_dir_key in self.rootfile.GetListOfKeys():
            dir_name = method_dir_key.GetName()
            if not dir_name.startswith("Method_"):
                continue
            method_dir = self.rootfile.Get(dir_name)
            for method_key in method_dir.GetListOfKeys():
                method_name = method_key.GetName()
                if method_name == self.old_name:
                    return method_dir
        raise KeyError("No MVA method {!r} in file {!r}"
                       .format(self.old_name, self.rootfile))

    def copy(self, target_file):
        """Copy the MVA method into the opened ROOT file `target_file`."""
        # Retrieve target Method_* directory.
        target_method_dir = self._ensure_dir(target_file,
                                             self.method_dir.GetName())
        target_method = target_method_dir.mkdir(self.new_name)
        if not target_method:
            raise KeyError("Target method {!r} already exists"
                           .format(self.new_name))
        self._copy_contents(self.method, target_method)

    @staticmethod
    def _ensure_dir(parent, name):
        """Ensure that the TDirectory `parent` contains a folder named `name`.

        Returns `parent.Get(name)` and always `cd`s into it.
        """
        if parent.cd(name):
            return ROOT.gDirectory
        else:
            result = parent.mkdir(name)
            result.cd()
            return result

    @staticmethod
    def _copy_contents(source, dest):
        """Copy all objects from TDirectory `source` to `dest`."""
        dest.cd()
        for key in source.GetListOfKeys():
            obj = source.Get(key.GetName())
            if key.GetName() in {"TrainingPath", "WeightsFileName"}:
                newname = key.GetName()
            elif key.GetName().startswith("MVA_"):
                prefix, _, suffix = key.GetName().split("_", 2)
                newname = "_".join([prefix, dest.GetName(), suffix])
            else:
                newname = ""
            clone = obj.Clone(newname)
            clone.Write()


def get_parser():
    """Return an argparse.ArgumentParser instance."""
    parser = argparse.ArgumentParser(
        description="Rename a branch in ROOT files")
    parser.add_argument("--yes", "-y", action="store_true",
                        help="Overwrite output file without asking.")
    parser.add_argument("--no", "-n", action="store_true",
                        help="Never overwrite output file. This takes "
                        "precedence over --yes.")
    parser.add_argument("--input", "-i", type=str, nargs=3, action="append",
                        metavar=("<file>", "<old name>", "<new name>"),
                        help="Specifies an MVA method that should be copied.")
    parser.add_argument("rootfile", metavar="<output file>", type=str,
                        help="Output ROOT file.")
    return parser


def main(argv):
    """Main function. You should pass `sys.argv[1:]` as argument."""
    args = get_parser().parse_args(argv)
    actions = [CopyAction(filename, old_name, new_name)
               for (filename, old_name, new_name) in args.input]
    try:
        mode = "abort" if args.no else "force" if args.yes else "interactive"
        outfile = force_create(args.rootfile, mode)
    except IOError:
        sys.exit(1)
    for action in actions:
        action.copy(outfile)


if __name__ == "__main__":
    main(sys.argv[1:])
