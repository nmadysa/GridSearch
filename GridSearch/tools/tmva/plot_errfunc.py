#! /usr/bin/env python2

"""Take a TMVA MLP and plot its error function over its weights.

This script takes a TMVAClassification_<MLPName>.weights.xml file
and varies the weights in a specified direction.
It then plots the variation in the network's error function over
this applied change.

This script depends on FANN for the implementation.
"""

from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import os
import sys
import abc
import numpy
import argparse
import itertools
import lxml.etree
from matplotlib import pyplot
from collections import namedtuple

from fann2 import libfann

import ROOT  # pylint: disable=import-error
ROOT.gROOT.SetBatch(False)
ROOT.PyConfig.IgnoreCommandLineOptions = True

from GridSearch import common


class XMLReader(object):

    """Base class for classes that read MLPs from TMVA weights files."""

    # pylint: disable=too-few-public-methods

    __metaclass__ = abc.ABCMeta

    def __init__(self, weights_xml_path):
        """Create an instance."""
        self.path = weights_xml_path
        # Get root element.
        self.root = lxml.etree.parse(weights_xml_path).getroot()
        if not self.root.tag == "MethodSetup":
            raise ValueError("Not a TMVA weights file: {!s}"
                             .format(weights_xml_path))
        method, name = self.root.attrib["Method"].split("::")
        if not method == "MLP":
            raise ValueError("Expected an MLP, found {} instead"
                             .format(method))
        self.name = name


class DatasetCreator(XMLReader):

    """This class creates a FANN dataset from a ROOT TTree."""

    cache_filename = "fann_dataset.dat"

    def __init__(self, weights_xml_path):
        """Create an instance."""
        super(DatasetCreator, self).__init__(weights_xml_path)
        self.variables = self.get_variables()
        self.do_autocache = False

    def cache_dataset(self, dataset):
        """Save dataset in the file "fann_data.dat"."""
        dataset.save_train(self.cache_filename)

    def load_cache(self):
        """Read dataset cached in the file "fann_data.dat"."""
        dataset = libfann.training_data()
        success = dataset.read_train_from_file(self.cache_filename)
        if not success:
            raise IOError(self.cache_filename)
        return dataset

    def get_dataset_from_array(self, events, classes):
        """Return normalized dataset object from numpy arrays.

        events  : array of shape `(n_events, n_vars)` containing
                  the input values for each tau candidate.
        classes : array of shape `(n_events,)` containing `1` if
                  the corresponding tau candidate is a signal tau
                  and `0` otherwise.
        """
        # Normalize variables so that all values are forced into
        # the interval [-1; 1].
        minimums = numpy.array([var.min for var in self.variables])
        maximums = numpy.array([var.max for var in self.variables])
        events = 2 * (events - minimums) / (maximums - minimums) - 1
        # Reshape `classes` from a 1D-array to a 2D-array with 1 column.
        if len(classes.shape) == 1:
            classes = classes.reshape(-1, 1)
        dataset = libfann.training_data()
        dataset.set_train_data(events, classes)
        if self.do_autocache:
            self.cache_dataset(dataset)
        return dataset

    def get_dataset_from_tree(self, tree):
        """Return normalized dataset object from a ROOT.TTree."""
        events, classes = self.get_arrays_from_tree(tree)
        return self.get_dataset_from_array(events, classes)

    def get_arrays_from_tree(self, tree):
        """Return ROOT.TTree information as numpy arrays."""
        # Prepare arrays.
        n_events = tree.GetEntries()
        n_vars = len(self.variables)
        events = numpy.zeros(shape=(n_events, n_vars))
        classes = numpy.zeros(n_events)
        # Iterate over tree.
        getval = tree.__getattr__
        for i_evt, event in enumerate(iter(tree)):
            for i_var, var in enumerate(self.variables):
                events[i_evt, i_var] = getval(var.name)
            classes[i_evt] = (1 if event.classID == 0 else 0)
        return events, classes

    def get_variables(self):
        """Return list of input variables for this network."""
        Variable = namedtuple("Variable", ["name", "min", "max"])  # pylint: disable=invalid-name
        variables_node = self.root.find("Variables")
        num_vars = int(variables_node.attrib["NVar"])
        variables = []
        for var_node in variables_node.iter("Variable"):
            i = int(var_node.attrib["VarIndex"])
            assert i == len(variables)
            variables.append(Variable(
                name=var_node.attrib["Label"],
                min=float(var_node.attrib["Min"]),
                max=float(var_node.attrib["Max"]),
                ))
        assert num_vars == len(variables)
        return variables


class NetworkCreator(XMLReader):

    """Class that creates a FANN neural net from a TMVA weights file."""

    def __init__(self, weights_xml_path):
        """Create an instance."""
        super(NetworkCreator, self).__init__(weights_xml_path)
        self.layers = self.build_network()

    def create_fann(self):
        """Create a FANN neural net from the read XML file."""
        net = libfann.neural_net()
        net.create_standard_array(self.get_layer_structure())
        net.set_weight_array(self.get_synapses())
        return net

    def get_layer_structure(self):
        """Read layer structure of the MLP with bias neurons excluded."""
        # Substract 1 if the current layer is not the output layer.
        neurons_per_layer = []
        for i, layer in enumerate(self.layers):
            is_last = (i+1 == len(self.layers))
            if is_last:
                neurons_per_layer.append(layer.num_neurons)
            else:
                neurons_per_layer.append(layer.num_neurons-1)
        return neurons_per_layer

    def get_synapses(self):
        """Return list of synapses for this network."""
        synapses = []
        for layer in self.layers:
            synapses.extend(layer.get_synapses())
        return synapses

    def build_network(self):
        """Return a list of NetworkLayer objects from the XML file.."""
        # Build layers.
        layout_node = self.root.find("Weights").find("Layout")
        num_layers = int(layout_node.attrib["NLayers"])
        layers = []
        for layer_node in layout_node.iter("Layer"):
            # Check the layer's index.
            i = int(layer_node.attrib["Index"])
            assert i == len(layers)
            # Add this layer.
            layer = (NetworkCreator.NetworkLayer(layer_node, layers[-1])
                     if layers else
                     NetworkCreator.NetworkLayer(layer_node))
            layers.append(layer)
        # Sanity check.
        assert num_layers == len(layers)
        # double-link the layers.
        for i, layer in enumerate(layers[:-1]):
            layer.next_layer = layers[i+1]
        return layers

    class NetworkLayer(object):

        """Class representing a layer in a neural net.

        This class is used to read the synapses of the neural net.
        """

        def __init__(self, node, prev_layer=None):
            """Create a NetworkLayer.

            node       : XML node associated with this layer.
            prev_layer : NetworkLayer before this one. Pass `None`
                         when creating the input layer.
            """
            self.node = node
            self.num_neurons = int(node.attrib["NNeurons"])
            if prev_layer is None:
                self.i_start = 0
            else:
                self.i_start = prev_layer.i_start + prev_layer.num_neurons
            self.next_layer = None

        def get_synapses(self):
            """Return synapses from this layer to the next one.

            Note that `self.next_layer` must be set before calling this
            method.
            """
            synapses = []
            for i, neuron_node in enumerate(self.node.iter("Neuron"),
                                            self.i_start):
                synapses.extend(self.get_neuron_synapses(i, neuron_node))
            return synapses

        def get_neuron_synapses(self, i_neuron, node):
            """Get outgoing synapses of a neuron.

            i_neuron : Index of this neuron.
            node     : XML node associated with this neuron.
            """
            Synapse = namedtuple("Synapse", ["ifrom", "ito", "weight"])  # pylint: disable=invalid-name
            # Special case for the output neuron.
            if node.text is None:
                assert int(node.attrib["NSynapses"]) == 0
                return []
            # Get synapses from XML.
            weights = [float(weight) for weight in node.text.split()]
            assert len(weights) == int(node.attrib["NSynapses"])
            synapses = []
            for i_to, weight in enumerate(weights, self.next_layer.i_start):
                synapses.append(Synapse(i_neuron, i_to, weight))
            return synapses


class AbstractPlotter(object):

    """Abstract base class containing common logic for plotting."""

    def __init__(self, net, dataset):
        """Constructor.

        net     : The neural net whose error function is to be plotted.
        dataset : The dataset used to compute the error function.
        """
        self.net = net
        self.dataset = dataset
        self._original_state = net.get_connection_array()
        self.get_error = self.get_mse_error

    def get_dim(self):
        """Return number of dimensions of the weight space."""
        return len(self._original_state)

    def get_unit_vector(self, index_or_vector):
        """Return a unit vector in the weight space.

        Argument may be an integer synapse index or a vector.
        index  : zero-based index of the axis along which to plot.
        vector : 1D-array of length `self.get_dim()` specifying the
                 direction along which to plot.
        """
        if isinstance(index_or_vector, int):
            index = index_or_vector
            direction = numpy.zeros(self.get_dim())
            direction[index] = 1.
        else:
            vector = numpy.asarray(index_or_vector)
            assert vector.shape == (self.get_dim(),)
            direction = vector / numpy.linalg.norm(vector)
        return direction

    def get_weight_vector(self):
        """Return the net's original point in weight space."""
        return numpy.array([weight for (_, _, weight) in self._original_state])

    def apply_weight_vector(self, weights):
        """Set the net's weights to the ones specified in a vector."""
        new_state = [(ifrom, ito, new_weight)
                     for (ifrom, ito, _), new_weight in
                     zip(self._original_state, weights)]
        self.net.set_weight_array(new_state)

    def reset_weights(self):
        """Restore the net's original synapse weights."""
        self.net.set_weight_array(self._original_state)

    def get_mse_error(self):
        """Return mean square error of the net with its current weights."""
        return self.net.test_data(self.dataset)

    def get_ce_error(self):
        """Return cross-entropy error of the net with its current weights."""
        targets = numpy.array(self.dataset.get_output())
        outputs = (self.net.run(evt)[0] for evt in self.dataset.get_input())
        error = -sum(
            target*numpy.log(output)  + (1-target)*numpy.log(1-output)
            for target, output in itertools.izip(targets, outputs)
            )
        return error / len(targets)


class Plotter1D(AbstractPlotter):

    """Class used to create 1D plots of the error function of a neural net."""

    def __init__(self, net, dataset):
        """Constructor. See AbstractPlotter.__init__ for more details."""
        super(Plotter1D, self).__init__(net, dataset)
        self.precision = 201

    def plot(self, *args, **kwargs):
        """Plot an error function curve and return its figure.

        The arguments are passed to `self.create_curve`.
        """
        xlist, ylist = self.create_curve(*args, **kwargs)
        figure = pyplot.figure()
        canvas = figure.add_subplot(111)
        canvas.set_xlabel("Distance")
        canvas.set_ylabel("Error Function")
        canvas.plot(xlist, ylist, "b-")
        canvas.set_xlim(xlist.min(), xlist.max())
        return figure

    def create_curve(self, i_weight, extent=4):
        """Plot the error function along a given axis.

        i_weight : Zero-based index of the axis along which to plot.
        extent   : How far to deviate from the network's point
                   in the weight space.
        """
        weights = self.get_weight_vector()
        anchor = weights[i_weight]
        xlist = numpy.linspace(anchor-extent, anchor+extent,
                               self.precision)
        ylist = []
        try:
            for weight in xlist:
                weights[i_weight] = weight
                self.apply_weight_vector(weights)
                ylist.append(self.get_error())
        finally:
            self.reset_weights()
        return xlist, ylist


class Plotter2D(AbstractPlotter):

    """Class used to create 2D contour plots of an MLP's error function."""

    def __init__(self, net, dataset):
        """Constructor. See AbstractPlotter.__init__ for more details."""
        super(Plotter2D, self).__init__(net, dataset)
        self.precision = 101
        self.plot_function = self.plot_pcolor

    def plot(self, *args, **kwargs):
        """Plot an error function curve and return its figure.

        The arguments are passed to `self.create_curve`.
        """
        x_weight, y_weight = args[:2]
        xlist, ylist, zarr = self.create_curve(*args, **kwargs)
        figure = pyplot.figure()
        canvas = figure.add_subplot(111)
        canvas.set_xlabel("Weight {}".format(x_weight))
        canvas.set_ylabel("Weight {}".format(y_weight))
        cmap = pyplot.get_cmap("coolwarm")
        graph = self.plot_function(canvas, xlist, ylist, zarr, cmap)
        canvas.set_xlim(xlist.min(), xlist.max())
        canvas.set_ylim(ylist.min(), ylist.max())
        colorbar = figure.colorbar(graph, ax=canvas)
        colorbar.set_label("Mean square error")
        return figure

    @staticmethod
    def plot_contourf(canvas, xlist, ylist, zarr, colormap):
        """Call `matplotlib.pyplot.contourf`."""
        return canvas.contourf(xlist, ylist, zarr, 100, cmap=colormap)

    @staticmethod
    def plot_pcolor(canvas, xlist, ylist, zarr, colormap):
        """Call `matplotlib.pyplot.contourf`."""
        return canvas.pcolor(xlist, ylist, zarr, cmap=colormap)

    def create_curve(self, x_weight, y_weight, extent=4):
        """Plot the error function along a given axis.

        x_weight,
        y_weight  : Zero-based indices of the axes along which to plot.
        extent    : How far to deviate from the network's point in the
                    weight space.
        """
        weights = self.get_weight_vector()
        x_anchor = weights[x_weight]
        y_anchor = weights[y_weight]
        xlist = numpy.linspace(x_anchor-extent, x_anchor+extent,
                               self.precision)
        ylist = numpy.linspace(y_anchor-extent, y_anchor+extent,
                               self.precision)
        zarr = numpy.zeros(shape=(len(xlist), len(ylist)))
        try:
            for i_x, x_val in enumerate(xlist):
                for i_y, y_val in enumerate(ylist):
                    weights[x_weight] = x_val
                    weights[y_weight] = y_val
                    self.apply_weight_vector(weights)
                    zarr[i_x, i_y] = self.get_error()
        finally:
            self.reset_weights()
        return xlist, ylist, zarr


def profile(func):
    """Function decorator that prints a function's execution time."""
    from time import time
    import functools
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        """Wrapper docstring"""
        before = time()
        result = func(*args, **kwargs)
        after = time()
        print("Function {} executed in {:.2f}s."
              .format(func.__name__, after-before), file=sys.stderr)
        return result
    return wrapper


def get_plot_vars(args):
    # pylint: disable=invalid-name
    """Interpret the command-line args and return variables for plotting.

    args : Namespace object created by argparse.ArgumentParser.parse_args.

    Returns:
    Plotter   : Class of the correct plotter. Instance to be constructed
                via `Plotter(net, dataset)`.
    plot_args : Arguments to be passed to `Plotter.plot`.
    """
    if args.plot_1d:
        Plotter = Plotter1D
        i_weight, = args.plot_1d
        plot_args = (i_weight, args.range)
    elif args.plot_2d:
        Plotter = Plotter2D
        x_weight, y_weight = args.plot_2d
        plot_args = (x_weight, y_weight, args.range)
    else:
        raise TypeError(repr(args))
    return Plotter, plot_args


def get_parser():
    """Return an argparse.ArgumentParser instance."""
    parser = argparse.ArgumentParser(
        description="Plot network error over weights")
    # Typical options.
    parser.add_argument("indir", metavar="DIRECTORY", default=os.curdir,
                        nargs="?", type=str,
                        help="directory containing TMVA.root and the "
                        "'weights' folder")
    parser.add_argument("--name", "-n", metavar="NAME", type=str,
                        default="MLP", help="Name of the MLP")
    parser.add_argument("--range", "-r", metavar="RANGE", type=float,
                        default=20, help="Plot +-RANGE around the "
                        "weight's value.")
    parser.add_argument("--precision", "-p", metavar="PRECISION", type=int,
                        default=101, help="How many points to compute in "
                        "the given range.")
    parser.add_argument("--out", "-o", type=str, default=None,
                        help="Output file. If not passed, show the plot.")
    # What to plot.
    plot_group = parser.add_mutually_exclusive_group(required=True)
    plot_group.add_argument("--plot-1d", metavar="I", type=int, nargs=1,
                            help="Create a 1D plot. The argument is the index "
                            "of a synapse weight.")
    plot_group.add_argument("--plot-2d", metavar="I", type=int, nargs=2,
                            help="Create a 2D plot. The arguments are the "
                            "two indices of synapse weights.")
    # Special flags.
    parser.add_argument("--contour", action="store_true",
                        help="For --plot-2d: Create a contour plot.")
    parser.add_argument("--num-weights", "-N", action="store_true",
                        help="Print number of weights and exit.")
    parser.add_argument("--no-cache", "-C", action="store_true",
                        help="Don't pickle the events read from the "
                        "TMVA.root tree.")
    parser.add_argument("--time", "-T", action="store_true",
                        help="Print execution time to stderr.")
    return parser


def main(argv):
    """Main function. You should pass `sys.argv[1:]` as argument."""
    args = get_parser().parse_args(argv)
    # Get neural net.
    weights_file = os.path.join(
        args.indir, "weights",
        "TMVAClassification_{}.weights.xml".format(args.name))
    net = NetworkCreator(weights_file).create_fann()
    if args.num_weights:
        print(len(net.get_connection_array()))
        return
    # Get dataset.
    creator = DatasetCreator(weights_file)
    creator.do_autocache = not args.no_cache
    try:
        dataset = creator.load_cache()
    except IOError:
        rootfile = common.PyRootFile(os.path.join(args.indir, "TMVA.root"))
        dataset = creator.get_dataset_from_tree(rootfile.TestTree)
    # Create the plot.
    Plotter, plot_args = get_plot_vars(args)  # pylint: disable=invalid-name
    plotter = Plotter(net, dataset)
    plotter.precision = args.precision
    if args.plot_2d and args.contour:
        plotter.plot_function = plotter.plot_contourf
    if args.time:
        plotter.plot = profile(plotter.plot)
    figure = plotter.plot(*plot_args)  # pylint: disable=star-args
    if args.out is None:
        pyplot.show()
    else:
        figure.savefig(args.out)


if __name__ == "__main__":
    main(sys.argv[1:])
