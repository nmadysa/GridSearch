#! /usr/bin/env python2

"""Script to plot the distribution of variables used by TMVA.

Read, format, and draw histograms of variables for signal and background
from a TMVA.root file.
"""

from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import re
import os
import sys
import ast
import argparse

from ConfigParser import RawConfigParser

import ROOT
ROOT.gROOT.SetBatch(True)  # pylint: disable=no-member
ROOT.PyConfig.IgnoreCommandLineOptions = True

from GridSearch import common
from GridSearch import plotting
from GridSearch.common import idvariables


SELF_PATH = os.path.abspath(os.path.dirname(__file__))
RES_PATH = os.path.normpath(os.path.join(SELF_PATH, "..", "..", "res"))
CONFIG_FILE = os.path.join(RES_PATH, "tmva_var_plot_options.ini")


def _draw_hists(trees, varname, nbins, range_, nprong):
    """Draw signal and background histograms of a variable using `TTree.Draw`.

    trees   : A `common.SigBkgPair` of TTree instances to read.
    varname : Name of the variable to draw.
    nbins   : Number of bins to use for the histogram
    range_  : A tuple of floats `(low, high)` denoting the range of the
              histograms. If `None`, let ROOT figure out good values.
    nprong  : Either 1 or 3.
    """
    if not nprong in (1, 3):
        raise common.NProngError(nprong)
    if range_ is not None:
        low, high = range_
        arguments = "{},{},{}".format(nbins, low, high)
    else:
        arguments = str(nbins)
    # Get histograms.
    hists = []
    for kind, draw_style in [("sig", "hist"), ("bkg", "p E0 same")]:
        histname = "{}__{}".format(varname, kind)
        n_drawn = getattr(trees, kind).Draw(
            "{}>>{}({})".format(varname, histname, arguments),
            "tau_numTrack=={}".format(nprong),
            draw_style,
            )
        if n_drawn == -1:
            raise ValueError("ROOT.TTree.Draw failed")
        # pylint: disable=no-member
        hist = ROOT.gPad.GetPrimitive(histname)
        assert hist
        ROOT.SetOwnership(hist, False)
        hists.append(hist)
    # pylint: disable=star-args
    return common.SigBkgPair(*hists)


def _format_varname(ttnt_name):
    """Return nice formatting of `varname`.

    Raises KeyError on failure.
    """
    ntuple_name = idvariables.get_ntuple_name(ttnt_name)
    return idvariables.get_nice_name(ntuple_name)


class AmbiguousNameException(Exception):
    """`VariableOptions` doesn't know which INI section to use."""
    pass


class VariableOptions(object):

    """Reads an INI file and returns plotting options for variables.

    This method also maintains a dict of default values for each option
    which are provided whenever an option is missing.

    The header of a section in the INI file may be a regular expression.
    When the correct section for a variable cannot be determined (e.g.
    because several match), an AmbiguousNameException is raised.
    """


    def __init__(self):
        """Create a new instance."""
        self.defaults = dict(
            nprong="None",
            logx="False",
            logy="False",
            nbins="100",
            range="None",
            legend_pos="0.7, 0.75, 0.9, 0.85",
            label_pos="0.2, 0.75",
            )
        self._plot_options = {}

    def read(self, filename):
        """Read options from an INI file and save them as dicts.

        Note that this always clears the previously-saved options.
        """
        self._plot_options.clear()
        parser = RawConfigParser()
        parser.read(filename)
        for section in parser.sections():
            # Treat every section header as a regex.
            varname_regex = re.compile(section)
            options = dict(parser.items(section))
            self._verify_options(options)
            self._plot_options[varname_regex] = options

    def _verify_options(self, options):
        """Make sure that only known options are passed.

        An option is known if it is present in the `defaults` dict.
        If any option is unknown to this method, a TypeError is raised.
        """
        unknown = set(options) - set(self.defaults)
        if unknown:
            raise TypeError("Unknown plotting option(s): " +
                            ", ".join(unknown))

    def get(self, varname, nprong=None):
        """Get options for a varname.

        This iterates over all INI sections and returns the options
        provided by that section whose header (as a regex) matches
        `varname`.
        If more than one header matches, an AmbiguousNameException is
        raised unless prongness deduction can disambiguate the choice.
        
        Prongness deduction only works if there are exactly two candidate
        sections for a variable and the argument `nprong` is passed to
        this method and either "1" or "3".
        In that case, all sections are selected that have a corresponding
        key "nprong" with an equal value.
        If both candidate sections match, an AmbiguousNameException is
        raised anyway.

        All options missing in the INI file are filled in with this
        object's `defaults`.
        """
        matching = [options for pattern, options in self._plot_options.items()
                    if pattern.match(varname)]
        # With exactly two sections and a passed `nprong` argument, the
        # "nprong" key might disambiguate the sections.
        if len(matching) == 2 and nprong != None:
            matching = [options for options in matching
                        if options["nprong"] == nprong]
        # Make sure the matching options are unambiguous.
        if len(matching) > 1:
            raise AmbiguousNameException(varname)
        elif len(matching) == 1:
            result = self.defaults.copy()
            result.update(matching[0])
            return result
        else:
            return self.defaults.copy()


class Outputter(object):

    # pylint: disable=no-self-use

    """Class that takes a TMVA.root file and produces nice plots.

    Usage:
    >>> outputter = Outputter("TMVA.root", nprong=3)
    >>> for canvas in outputter.itercanvases():
    ...     canvas.SaveAs(".eps")
    """

    def __init__(self, filenames, nprong, inipath=None):
        """Instantiate the class.

        filenames : A tuple `(signal, background)` of paths to TTNT files.
        inipath   : Path to a plotting config file.
        nprong    : 1 or 3.
        """
        # Validate arguments.
        if not nprong in (1, 3):
            raise common.NProngError(nprong)
        self.files = common.SigBkgPair(common.PyRootFile(filenames.sig),
                                       common.PyRootFile(filenames.bkg))
        self.trees = common.SigBkgPair(self.files.sig.Get("tau"),
                                       self.files.bkg.Get("tau"))
        self.nprong = nprong
        # Optional attributes.
        self.is_atlas = False
        # Get options and adapt `nprong` default to our prongness.
        self.options = VariableOptions()
        self.options.defaults["nprong"] = str(self.nprong)
        if inipath is not None:
            self.options.read(inipath)

    def itercanvases(self, id_variables=None):
        """Returns an iterator over all nice canvases.

        variables : A `common.IDVariables` of variables to plot.
                    If not passed, iterate over all variables in the tree.

        Note that if `variables` is passed, all its elements are iterated
        over, even if the config file notes that they shouldn't.
        """
        # Get right list of variables.
        if id_variables is None:
            variables = self.get_default_vars()
        else:
            variables = getattr(id_variables.for_ttnt,
                                {1: "sp", 3: "mp"}[self.nprong])
            # Extract names from `(name, type)` tuples.
            # pylint: disable=star-args
            variables, _ = zip(*variables)
        for varname in variables:
            yield self._create_canvas(varname)

    def get_default_vars(self):
        """Return an iterator over default variables.

        A variable qualifies for the default list if:
            - it is in both input trees
            - the config file either doesn't have an nprong option for
              this variable OR the nprong option matches this Outputter's
              nprong attribute.
        """
        for branch in self.trees.sig.GetListOfBranches():
            name = branch.GetName()
            # Must exist in BOTH trees.
            if not self.trees.bkg.GetBranch(name):
                continue
            # Must have same prongness as this object.
            options = self.options.get(name, str(self.nprong))
            var_prongness = int(options["nprong"])
            if var_prongness == self.nprong:
                yield name

    # Internal methods.

    def _create_canvas(self, varname):
        """Create a nice canvas for the passed variable name."""
        # Create canvas.
        canvas_name = "{}_{}p".format(varname, self.nprong)
        # pylint: disable=no-member
        canvas = ROOT.TCanvas(canvas_name)
        # Read variable options.
        options = {}
        for key, value in self.options.get(varname, str(self.nprong)).items():
            try:
                options[key] = ast.literal_eval(value)
            except ValueError as exc:
                raise ValueError("{!s}: {!r}".format(exc.message, value))
        # Draw histograms.
        hists = _draw_hists(self.trees, varname, options["nbins"],
                            options["range"], self.nprong)
        self._style_signal(hists.sig)
        self._style_background(hists.bkg)
        # Final adjustments.
        self._style_canvas(canvas, options["logx"], options["logy"],
                           options["label_pos"])
        self._style_final(canvas, hists, options["legend_pos"])
        return canvas

    def _style_canvas(self, canvas, is_logx, is_logy, label_pos):
        """Format canvas in a nice way."""
        canvas.cd()
        if is_logx:
            canvas.SetLogx(1)
        if is_logy:
            canvas.SetLogy(1)
        prong_text = "{}-prong".format(self.nprong)
        if self.is_atlas:
            plotting.create_atlas_label(x_y=(label_pos[0], label_pos[1]+0.10))
        plotting.create_text(prong_text, (label_pos[0], label_pos[1]+0.05))
        plotting.create_energy_text(label_pos)

    def _style_signal(self, hist):
        # pylint: disable=no-member
        """Format the signal histogram."""
        hist.SetTitle("Signal sample")
        hist.Scale(1.0 / hist.Integral())
        hist.SetMarkerStyle(0)
        hist.SetMarkerSize(0)
        hist.SetLineColor(ROOT.kBlack)
        hist.SetLineWidth(1)
        hist.SetFillColor(ROOT.kRed)
        hist.SetFillStyle(3004) # Hatched, lines go from bottom-left
                                # to top-right.

    def _style_background(self, hist):
        """Format the background histogram."""
        hist.SetTitle("Background sample")
        hist.Scale(1.0 / hist.Integral())
        # Set error to small but non-zero value to only show X errors.
        for i in range(1, hist.GetNbinsX()+1):
            hist.SetBinError(i, 1.e-6)

    def _style_final(self, canvas, hists, legend_pos):
        """Make adjustments which can only be done after drawing."""
        canvas.cd()
        a_hist = hists[0]
        varname, _, _ = a_hist.GetName().partition("__")
        # Manipulate X-axis title.
        x_axis = a_hist.GetXaxis()
        x_axis.SetTitle(_format_varname(varname))
        # Manipulate Y-axis title.
        y_axis = a_hist.GetYaxis()
        y_axis.SetTitle("Fraction of Candidates")
        y_axis.SetRangeUser(0.0, 1.1*max(h.GetMaximum() for h in hists))
        # Build legend.
        plotting.create_legend(hists, legend_pos, entry_option="PFL")


def get_parser():
    """Return an argparse.ArgumentParser instance."""
    parser = argparse.ArgumentParser()
    parser.add_argument("infiles", type=str, metavar="FILE", nargs=2,
                        help="TTNT files used for training.")
    parser.add_argument("--config", "-C", default=CONFIG_FILE, metavar="FILE",
                        help="An optional INI file describing how to bin "
                        "and plot variables.")
    parser.add_argument("--variables", "-V", default=None, metavar="FILE",
                        help="An optional INI file listing the variables "
                        "to plot for 1- and 3-prong.")
    parser.add_argument("--atlas", "-A", action="store_true",
                        help='Add the label "ATLAS work in progress" to '
                        'all plots.')
    parser.add_argument("--outdir", "-o", default=os.curdir, metavar="DIR",
                        help="output directory")
    return parser


def main(argv):
    """Main function. You should pass `sys.argv[1:]` as argument."""
    args = get_parser().parse_args(argv)
    plotting.set_atlas_style()
    id_variables = common.IDVariables(args.variables)
    # Get outputters.
    outputters = []
    for nprong in 1, 3:
        if common.PyRootFile.is_signal_file(args.infiles[0]):
            infiles = common.SigBkgPair(args.infiles[0], args.infiles[1])
        else:
            infiles = common.SigBkgPair(args.infiles[1], args.infiles[0])
        outputter = Outputter(infiles, nprong, args.config)
        outputters.append(outputter)
        # Optional settings to all outputters.
        outputter.is_atlas = args.atlas
    # Otherwise, iterate over outputters and canvases.
    for outputter in outputters:
        for canvas in outputter.itercanvases(id_variables):
            outname = os.path.join(args.outdir,
                                   "{}.eps".format(canvas.GetName()))
            canvas.SaveAs(outname)


if __name__ == "__main__":
    main(sys.argv[1:])




