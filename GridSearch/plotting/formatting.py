#! /usr/bin/env python2

"""Module for various convenience functions regarding plotting."""

from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import array

import ROOT  # pylint: disable=import-error
ROOT.gROOT.SetBatch(True)
ROOT.PyConfig.IgnoreCommandLineOptions = True


# --- Creating objects ---

def create_canvas_by_client_rect(name, title, width, height, _cache=[]):
    # pylint: disable=dangerous-default-value
    """Create a canvas of specific size by giving its client rectangle.

    The width/height parameters on ROOT.TCanvas specify the dimensions
    of the canvas's window.
    The width/height parameters on `create_canvas_by_client_rect` specify
    the dimensions of the window's usable graphics area.
    """
    # Find corrective addends.
    if _cache:
        diff_x, diff_y = _cache  # pylint: disable=unbalanced-tuple-unpacking
    else:
        canvas = ROOT.TCanvas(name, title, width, height)
        diff_x = width - canvas.GetWw()
        diff_y = height - canvas.GetWh()
        canvas.Close()
        _cache[:] = [diff_x, diff_y]
    # Create window naively.
    canvas = ROOT.TCanvas(name, title, width+diff_x, height+diff_y)
    return canvas


def create_graph(xlist, ylist, draw_options="", do_draw=True):
    """Create a ROOT.TGraph object from two lists of coordinates."""
    if len(xlist) != len(ylist):
        raise ValueError("Lists of unequal length: {} vs. {}"
                         .format(len(xlist), len(ylist)))
    # Create the graph
    graph = ROOT.TGraph(len(xlist), array.array("f", xlist),
                        array.array("f", ylist))
    ROOT.SetOwnership(graph, False)
    if do_draw:
        graph.Draw(draw_options)
    return graph


def create_hline(x_axis, y_coord, do_draw=True):
    """Create a ROOT.TLine object parallel to an x axis."""
    xlow = x_axis.GetBinLowEdge(x_axis.GetFirst())
    xup = x_axis.GetBinUpEdge(x_axis.GetLast())
    line = ROOT.TLine(xlow, y_coord, xup, y_coord)
    ROOT.SetOwnership(line, False)
    if do_draw:
        line.Draw()
    return line


def create_vline(y_axis, x_coord, do_draw=True):
    """Create a ROOT.TLine object parallel to a y axis."""
    ylow = y_axis.GetBinLowEdge(y_axis.GetFirst())
    yup = y_axis.GetBinUpEdge(y_axis.GetLast())
    line = ROOT.TLine(x_coord, ylow, x_coord, yup)
    ROOT.SetOwnership(line, False)
    if do_draw:
        line.Draw()
    return line


def create_legend(entries, rect, entry_option="", do_draw=True):
    """Create a legend for the current canvas.

    entries      : Iterable of objects to be added to the legend.
                   Their title is written and a representation of them is
                   drawn left of it.
    rect         : The rectangle of where to draw the legend. A 4-tuple
                   of floats (left, bottom, right, up) in the canvas
                   coordinate system.
    entry_option : Common options to be used to draw the representations
                   of all entries. If you want different entries to have
                   different styles, pass an empty sequence for `entries`
                   and add them yourself.
    do_draw      : Draw the legend object before returning.
    Returns : The ROOT.TLegend object.
    """
    # Create, set ownership to False to prevent premature deletion.
    left, bottom, right, top = rect
    legend = ROOT.TLegend(left, bottom, right, top, "", "NDC")
    ROOT.SetOwnership(legend, False)
    # Set desired style.
    legend.SetFillStyle(0)
    legend.SetBorderSize(0)
    # Add objects. Show their marker next to the title.
    for entry in entries:
        legend.AddEntry(entry, "", entry_option)
    if do_draw:
        legend.Draw()
    return legend


def create_lumi_text(luminosity, x_y, size=0.04, break_line=False,
                     do_draw=True):
    """Create a standard label that denotes the used luminosity.

    luminosity : Luminosity in inverse femtobarn (fb**-1).
    x_y        : Position in NDC.
    size       : Font size.
    break_line : If True, insert a line break between luminosity and
                 center-of-mass energy. This means that, in fact, two
                 `ROOT.TLatex` object will be created. Default is False.
    do_draw    : If True, draw the text before returning from this
                 function. Default is True.
    Returns : A list `[tlatex_object]` of the `ROOT.TLatex` objects
              created. Depending on `break_line`, the length of this
              list is either 1 or 2.
    """
    x, y = x_y  # pylint: disable=invalid-name
    lumi_text = "#scale[0.7]{{#int}}dt L = {:.1f} fb^{{-1}}".format(luminosity)
    energy_text = "#sqrt{s} = 8 TeV"
    text_objects = []
    if break_line:
        # First line.
        text_objects.append(create_text(lumi_text, (x, y), size=size,
                                        do_draw=False))
        # Second line.
        y -= 1.25*size  # pylint: disable=invalid-name
        text_objects.append(create_text(energy_text, (x, y), size=size,
                                        do_draw=False))
    else:
        # Join text and print in one line.
        joined_text = ", ".join([lumi_text, energy_text])
        text_objects.append(create_text(joined_text, (x, y), size=size,
                                        do_draw=False))
    if do_draw:
        for text in text_objects:
            text.Draw('sames')
    return text_objects


def create_energy_text(x_y, size=0.04, do_draw=True):
    """Create a standard label of the used center-of-mass energy.

    x_y        : Position in NDC.
    size       : Font size.
    do_draw    : If True, draw the text before returning from this
                 function. Default is True.
    Returns : The `ROOT.TLatex` object created.
    """
    text = "#sqrt{s} = 8 TeV"
    return create_text(text, x_y, size=size, do_draw=do_draw)


def create_atlas_label(description="work in progress", x_y=(0.6, 0.87),
                       offset=(0.12, 0), size=0.05, do_draw=True):
    """Create an ATLAS watermark label in the current canvas.

    description : Text to be added behind the word "ATLAS".
    x_y         : Coordinates (left, bottom) of the text in NDC.
    offset      : Tuple of coordinate differences between "ATLAS" and
                  `description`.
    size        : Font size.
    do_draw     : If True, draw the label before returning
    Returns : tuple `(atlas, description)` of the two created ROOT.TLatex
              objects.
    """
    x, y = x_y  # pylint: disable=invalid-name
    off_x, off_y = offset
    atlas_label = create_text("ATLAS", x_y, size=size, font=72,
                              do_draw=False)
    description_label = create_text(description, (x+off_x, y+off_y),
                                    size=size, font=42, do_draw=False)
    if do_draw:
        atlas_label.Draw("sames")
        description_label.Draw("sames")
    return atlas_label, description_label


def create_text(text, x_y, **options):
    """Create a ROOT.TLatex object.

    text    : Text to draw.
    x_y     : Coordinates where to draw the text. A tuple (left, bottom).
    Keyword-only arguments:
    angle   : Rotation of the text
    size    : Font size
    font    : Font style (integer)
    color   : Text color
    ndc     : Determines if `x_y` are given in NDC (normalized device
              coordinates). Default is True.
    do_draw : Determines if the text will be drawn before returning from
              this call. Default is True.
    Returns : The ROOT.TLatex object that has been created.
    """
    # Sanitize input.
    _validate_kwargs(options, good_keys={"angle", "size", "font", "color",
                                         "ndc", "do_draw"})
    # Create object, avoid premature deletion.
    x, y = x_y  # pylint: disable=invalid-name
    text_object = ROOT.TLatex(x, y, text)
    ROOT.SetOwnership(text_object, False)
    # Set options.
    if "size" in options:
        text_object.SetTextSize(options["size"])
    if "angle" in options:
        text_object.SetTextAngle(options["angle"])
    if "font" in options:
        text_object.SetTextFont(options["font"])
    if "color" in options:
        text_object.SetTextColor(options["color"])
    if options.get("ndc", True):
        text_object.SetNDC()
    if options.get("do_draw", True):
        text_object.Draw("sames")
    return text_object


# --- Formatting objects ---

def style_object(graphic_object, **kwargs):
    """Change various style attributes on a graphic object.

    May be passed any number of the following keyword arguments:
    - linecolor, linestyle, linewidth
    - markercolor, markerstyle, markersize
    - fillcolor, fillstyle
    If any of these attributes is passed and not None, it will be fowarded
    to `graphic_object`'s proper method.
    E.g. `linecolor` will be passed to `graphic_object.SetLineColor`.

    Returns graphic_object.
    """
    # These are the methods that we know how to handle.
    setter_names = {
        "SetLineColor", "SetLineStyle", "SetLineWidth",
        "SetMarkerColor", "SetMarkerStyle", "SetMarkerSize",
        "SetFillColor", "SetFillStyle",
        }
    setter_names = {name.replace("Set", "", 1).lower(): name
                    for name in setter_names}
    known_kwargs = setter_names.keys()
    _validate_kwargs(kwargs, good_keys=known_kwargs)
    # Handle arguments in a loop.
    for argument, value in kwargs.items():
        # For each argument we get, look up the proper method and call it.
        setter_name = setter_names[argument]
        set_attribute = getattr(graphic_object, setter_name)
        set_attribute(value)
    return graphic_object


def copy_style(from_, to_):
    """Copy style attributes from one graphic object to another."""
    attributes = {"LineColor", "LineStyle", "LineWidth",
                  "MarkerColor", "MarkerStyle", "MarkerSize",
                  "FillColor", "FillStyle"}
    for attribute in attributes:
        try:
            getter = getattr(from_, "Get"+attribute)
            setter = getattr(to_, "Set"+attribute)
            setter(getter())
        except AttributeError:
            pass

def scale_axes(graphic_object, scale):
    """Scale font size etc. of the axes on a graphic object.

    graphic_object : An object with methods `GetXaxis` and `GetYaxis`.
    scale          : Scaling factor.
    """
    x_axis, y_axis = graphic_object.GetXaxis(), graphic_object.GetYaxis()
    gs = ROOT.gStyle  # pylint: disable=invalid-name
    for axis, axis_name, label_offset_factor in [(x_axis, "X", 1.5),
                                                 (y_axis, "Y", 0.7)]:
        axis.SetLabelSize(gs.GetLabelSize(axis_name) * scale)
        axis.SetTitleSize(gs.GetTitleSize(axis_name) * scale)
        axis.SetTitleOffset(gs.GetTitleOffset(axis_name) *
                            label_offset_factor / scale)
        axis.SetLabelOffset(gs.GetLabelOffset(axis_name) * scale)
        axis.SetTickLength(gs.GetTickLength(axis_name) * scale)


# --- Helper functions ---

def _validate_kwargs(kwargs, good_keys):
    """Raise TypeError if the mapping `kwargs` has keys not in `good_keys`."""
    # Find keys in kwargs that are not in good_keys.
    bad_keys = set(kwargs.keys()) - set(good_keys)
    # If the set of bad keys is non-empty, raise TypeErrror.
    if bad_keys:
        raise TypeError("invalid keyword argument(s): " +
                        ", ".join(bad_keys))
