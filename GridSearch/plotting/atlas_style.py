#! /usr/bin/env python2

"""Python implementation of Atlas style.

This module exports a function called set_atlas_style which creates a
ROOT.TStyle object and forces its usage.
"""

from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import ROOT  # pylint: disable=import-error
ROOT.gROOT.SetBatch(True)
ROOT.PyConfig.IgnoreCommandLineOptions = True


class _AtlasStyle(ROOT.TStyle):

    """Subclass of ROOT.TStyle that conforms with ATLAS guidelines."""

    # pylint: disable = no-member, too-few-public-methods

    def __init__(self, name, title=""):

        """Create an instance of this TStyle subclass."""

        ROOT.TStyle.__init__(self, name, title)

        # Use plain black on white colors:
        self.SetFrameBorderMode(ROOT.kWhite)
        self.SetFrameFillColor(ROOT.kWhite)
        self.SetCanvasBorderMode(ROOT.kWhite)
        self.SetCanvasColor(ROOT.kWhite)
        self.SetPadBorderMode(ROOT.kWhite)
        self.SetPadColor(ROOT.kWhite)
        self.SetStatColor(ROOT.kWhite)

        # set the paper & margin sizes
        self.SetPaperSize(20, 26)

        # set margin sizes
        self.SetPadTopMargin(0.05)
        self.SetPadRightMargin(0.05)
        self.SetPadBottomMargin(0.16)
        self.SetPadLeftMargin(0.16)

        # set title offsets (for axis label)
        self.SetTitleXOffset(1.4)
        self.SetTitleYOffset(1.4)

        # use large fonts
        font = 42 # Helvetica
        self.SetTextFont(font)
        self.SetLabelFont(font, "x")
        self.SetTitleFont(font, "x")
        self.SetLabelFont(font, "y")
        self.SetTitleFont(font, "y")
        self.SetLabelFont(font, "z")
        self.SetTitleFont(font, "z")

        tsize = 0.05
        self.SetTextSize(tsize)
        self.SetLabelSize(tsize, "x")
        self.SetTitleSize(tsize, "x")
        self.SetLabelSize(tsize, "y")
        self.SetTitleSize(tsize, "y")
        self.SetLabelSize(tsize, "z")
        self.SetTitleSize(tsize, "z")

        # use bold lines and markers
        self.SetMarkerStyle(20)
        self.SetMarkerSize(1.2)
        self.SetHistLineWidth(2)
        self.SetLineStyleString(2, "[12 12]") # postscript dashes

        # get rid of X error bars
        # self.SetErrorX(0.001)
        # get rid of error bar caps
        self.SetEndErrorSize(0.)

        # do not display any of the standard histogram decorations
        self.SetOptTitle(0)
        # self.SetOptStat(1111)
        self.SetOptStat(0)
        # self.SetOptFit(1111)
        self.SetOptFit(0)

        # put tick marks on top and RHS of plots
        self.SetPadTickX(1)
        self.SetPadTickY(1)


def set_atlas_style():
    """Forces ATLAS style to be used for all plots."""
    style = _AtlasStyle("AtlasStyle")
    ROOT.gROOT.SetStyle(style.GetName())  # pylint: disable=no-member
    ROOT.gROOT.ForceStyle()
