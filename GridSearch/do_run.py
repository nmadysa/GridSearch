#! /usr/bin/env python2

"""Exports a class that runs over a structure created by init_run.

When used as a script, instantiates and runs an object of this class.
"""

from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import os
import sys
import glob
import argparse
import functools
import subprocess
from collections import deque
from multiprocessing import Process

import GridSearch.init_run
import GridSearch.do_run
import GridSearch.training
import GridSearch.determine_cuts
import GridSearch.plot_roc
import GridSearch.plot_scores
import GridSearch.plot_sig_eff
import GridSearch.performance
from GridSearch import common


class RunData(object):

    # pylint: disable=too-few-public-methods

    """Encapsulation of all data to be passed around during the run."""

    def __init__(self, basedir, nprong):
        """Create a new instance.

        basedir : path to an ANN folder.
        nprong  : Either 1 or 3.
        """
        if nprong not in (1, 3):
            raise common.NProngError(nprong)
        self.basedir = basedir
        self.nprong = nprong
        self.prongdir = os.path.join(basedir, "{}prong".format(nprong))
        self.actor_name = "do_run.py in ({})".format(self.prongdir)

    def __repr__(self):
        return "{}({}, {})".format(type(self).__name__,
                                   self.basedir, self.nprong)


# Utility functions

def _enters_prong_folder(func):
    """Function decorator to add a call to preserve_cwd to a function.

    This takes a function which only takes a `RunData` instance as parameter
    and wraps a calls to it as follows:
    ```
    with preserve_cwd(run_data.prongdir):
        func(run_data)
    ```
    """
    @functools.wraps(func)
    def wrapper(run_data):  # pylint: disable=missing-docstring
        with common.preserve_cwd(run_data.prongdir):
            return func(run_data)
    return wrapper


def _locate(name, dirs):
    """Return the first file "name" that exists in one of the `dirs`.

    This raises IOError if none of the listed directories contains the
    file.
    """
    for dir_ in dirs:
        path = os.path.join(dir_, name)
        if os.path.exists(path):
            return path
    raise IOError(2, "No such file or directory: {!r}".format(name))


def _tmva_output_exists(path):
    """Return True if it is likely TMVA has run successfully before.

    This method checks whether `path` contains a folder "weights" which
    contains weights files for both the BDT and the MLP.
    If it does, it is likely that TMVA needs not run again.
    """
    template = os.path.join(path, "weights",
                            "TMVAClassification_{}.weights.xml")
    names = (template.format(method) for method in ["BDT", "MLP"])
    return all(os.path.exists(name) for name in names)


# Functions for execution of scripts and programs.

def __logging_wrapper(actor, logdir, logname, callback):
    """Print to stdout, open a log file, then call `callback(logfile)`."""
    print("{}: {} is running ...".format(actor, logname))
    sys.stdout.flush()
    try:
        logpath = os.path.join(logdir, logname)+".log"
        with open(logpath, "w") as logfile:
            callback(logfile)
    except:
        print("{}: {} failed!".format(actor, logname))
        raise
    else:
        print("{}: {} is done!".format(actor, logname))


def _exec(actor, logname, args, logdir="logs"):
    """Start a new process and log its output.

    Pass `args` to `subprocess.check_call` and redirect its stdout and
    stderr to a log file named "{logpath}/{logname}.log".

    actor  : Name of the caller which is printed to stdout.
    args   : Command line passed to `subprocess`.
    name   : Log file name (without suffix).
    logdir : Path to the directory where the log file is saved.
    """
    def callback(logfile):
        """Take the passed log file for logging the subprocess output."""
        subprocess.check_call(args, stdout=logfile, stderr=logfile)
    __logging_wrapper(actor, logdir, logname, callback)


def _exec_module(actor, module, args, logdir="logs"):
    """Like `_exec`, but calls an imported module's main function instead.

    module : A module object with a function `main`.
    args   : Arguments passed to `module.main`.
    """
    def callback(logfile):
        """Take the passed log file for redirecting Python output."""
        with common.redirect_stdout(logfile):
            with common.redirect_stderr(logfile):
                module.main(args)
    __logging_wrapper(actor, logdir, module.__name__, callback)


def _exec_sframe(actor, xml_file, logdir="logs"):
    """Like `_exec`, but executes an SFrame cycle."""
    _exec(actor, xml_file, ["sframe_main", xml_file], logdir)


# Functions that perform the run.

def run_all(run_data):
    """Execute programs in the correct order to train and evaluate an ANN.

    This calls all run_* functions of this module with the passed
    `run_data` in the order that is necessary to evaluate an ANN.
    """
    run_training(run_data)
    run_optimisation(run_data)
    determine_cuts(run_data)
    run_evaluation(run_data)
    make_plots(run_data)
    run_performance(run_data)


@_enters_prong_folder
def run_training(run_data):
    """Train BDT and MLP via TMVA, save results in folder "weights".

    The function tests if the methods have been trained already and exits
    if that's the case.
    """
    if _tmva_output_exists(os.curdir):
        return
    ttnt_files = glob.glob(os.path.join(os.pardir, "ttnt", "*.root"))
    methods_file = _locate("methods.ini", [os.curdir, os.pardir])
    # Do not use `_exec_module` because Python is unable to redirect
    # TMVA's output.
    _exec(run_data.actor_name, logname="training", args=[
        "python", "-m", "GridSearch.training",
        "--weights-dir", os.path.join(os.pardir, "event_weights"),
        "--methods", methods_file,
        "--variables", os.path.join(os.pardir, "variables.ini"),
        "--nprong", str(run_data.nprong),
        ] + ttnt_files)


@_enters_prong_folder
def run_optimisation(run_data):
    """Create histograms in folder "optimisation"."""
    _exec_sframe(run_data.actor_name, "RunEfficiencyOptimisationCycle.xml")


@_enters_prong_folder
def determine_cuts(run_data):
    """Create cut-vs-pt curves in subfolder "cuts"."""
    # Tuple-unpack the files list to assert the expected number of files.
    (optfile,) = glob.glob(os.path.join("optimisation", "*.root"))
    _exec_module(run_data.actor_name, GridSearch.determine_cuts, [
        "--nprong", str(run_data.nprong),
        "--outdir", "cuts",
        optfile,
        ])


@_enters_prong_folder
def run_evaluation(run_data):
    """Create efficiency-flattened histograms in "evaluation"."""
    _exec_sframe(run_data.actor_name, "RunEfficiencyEvaluationCycle.xml")


@_enters_prong_folder
def make_plots(run_data):
    """Create plots in subfolder "plots"."""
    # Tuple-unpack the files list to assert the expected number of files.
    files = (_, _) = glob.glob(os.path.join("evaluation", "*.root"))
    # Plot score histograms
    _exec_module(run_data.actor_name, GridSearch.plot_scores, [
        "--nprong", ("2" if run_data.nprong == 3 else "1"),
        "--outdir", "plots",
        ] + files)
    # Plot ROC curves for defaultBDT/MLP and BDT/MLP.
    for reference_method in ["defaultBDT", "BDT"]:
        _exec_module(run_data.actor_name, GridSearch.plot_roc, [
            "--nprong", ("2" if run_data.nprong == 3 else "1"),
            "--out", "plots",
            "--by-folder", os.pardir, reference_method, "MLP",
            ])


@_enters_prong_folder
def run_performance(run_data):
    """Create plots in subfolder "plots"."""
    # Tuple-unpack the files list to assert the expected number of files.
    files = (_, _) = glob.glob(os.path.join("evaluation", "*.root"))
    # Get performance.
    _exec_module(run_data.actor_name, GridSearch.performance, [
        "--nprong", ("2" if run_data.nprong == 3 else "1"),
        "--method", "MLP",
        "--out", "performance",
        ] + files)


# Main part.

def get_parser():
    """Return an argparse.ArgumentParser instance."""
    parser = argparse.ArgumentParser(
        description="Train, test, and evaluate an artificial neural "
        "network. This script expects that all files and folders be "
        "laid out in a specific folder structure. Use the module "
        "init_run to create such a structure.")
    parser.add_argument("basedirs", metavar="ANN_DIR", nargs="+",
                        type=str, help="ANN folder in which to run "
                        "the scripts.")
    parser.add_argument("--nprong", "-n", metavar="N", default=None,
                        type=int, help="1 or 3. The prong to train and "
                        "test on. If not passed, train and test on "
                        "both prongs.")
    parser.add_argument("--jobs", "-j", metavar="N", type=int, default=2,
                        help="How many train+test runs to execute in "
                        "parallel. The default is 2, not 1!")
    return parser


def main(argv):
    """Main function. You should pass `sys.argv[1:]` as argument."""
    args = get_parser().parse_args(argv)
    workers = []
    queue = deque()
    for basedir in args.basedirs:
        for nprong in [1, 3] if args.nprong is None else [args.nprong]:
            queue.append(RunData(basedir, nprong))
    while len(queue) > 0:
        # Fill up the list of workers.
        while len(workers) < 4 and len(queue) > 0:
            worker = Process(target=run_all, args=[queue.popleft()])
            worker.daemon = True
            worker.start()
            workers.append(worker)
        # Wait for some time.
        workers[0].join(5)
        # Remove all finished workers.
        workers = [worker for worker in workers if worker.is_alive()]
    # The queue is empty, wait for all remaining processes to finish.
    for worker in workers:
        worker.join()


if __name__ == "__main__":
    main(sys.argv[1:])

